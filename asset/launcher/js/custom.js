
$(window).on("load scroll", function (e) {
    var scroll = $(window).scrollTop();

    if (scroll >= 1) {
        $("body").addClass("scrolling has-scroll");
    } else {
        $("body").removeClass("scrolling");
    }
});

function readURLbg(input, show_image) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("." + show_image).css(
                "background-image",
                "url(" + e.target.result + ")"
            );

            $("." + show_image)
                .css("background-repeat", "no-repeat")
                .css("background-position", "no-repeat")
                .css("background-size", "cover");
            /*  .css("height", "100%"); */

            $(".file_txt").attr("src", e.target.result);
        };

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}



$(document).ready(function () {

    $("body").removeClass("nav-opened");
    $('<div class="page-blocker"></div>').appendTo('body');

    $('.navbar-toggle').click(function () {
        $("html").toggleClass("nav-opened");
    });

    $(".page-blocker").click(function () {
        $("html").toggleClass("nav-opened");
        setTimeout($.proxy(function () {
            $('html').removeClass("closing");
        }, this), 700);
    });

    /*------------[Start] Menu Mobile ------------*/

    $(function () {
        $(".nav-main .dropdown a").click(function () {
            $('html').addClass("level-1-opened");
            $(this).parent().addClass("showing")
        });

        $(".nav-main .nav-close").click(function () {
            $('html').removeClass("level-1-opened");
            setTimeout($.proxy(function () {
                $(".nav-main .dropdown").removeClass("showing");
            }, this), 700);
        });
    });

    /*------------[Start] Tooltip ------------*/

    $('.card-share .item').tooltip({
        placement: 'right'
    });


    /*-----------------------[Start] Back to top-----------------------*/
    $('<div class="item-totop totop"></div>').appendTo('body');

    $('.totop').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });


    /*------------[Start] change color of SVG image using CSS ------------*/

    $('img.svg-js').each(function () {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Remove any invalid XML tags
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and apply on SVG
            $.each(attributes, function () {
                $svg.attr(this.name, this.value);
            });

            // Replace IMG with SVG
            $img.replaceWith($svg);
        }, 'xml');
    });

});


$(window).on("load", function () {
    $("html").addClass("page-loaded");

    $('.preload').fadeOut();

    /*---------------jquery.youtubecoverphoto.js -----------------*/


    $('.card-video.lg').click(function () {
        $(this).addClass('playing');
    });

    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if (isMobile.any()) {
        $("html").addClass("device");
    } else {
        $("html").addClass("pc");
    }

});
