<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| Your site name,
|
*/
$config['site_name'] = 'luncher';
$config['version'] = '3.1.0';
