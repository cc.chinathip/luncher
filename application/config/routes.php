<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route_path = APPPATH . 'routes/';
require_once $route_path . 'routes_landing.php';

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;

$route['administrator/login'] = 'auth/backend/auth/login';
$route['administrator/register'] = 'auth/backend/auth/register';
$route['administrator/forgot-password'] = 'auth/backend/auth/forgot_password';

$route['page/chart/pie_chart'] = 'page/chart/pie_chart';
$route['page/(:any)'] = 'page/detail/$1';
$route['blog/index'] = 'blog/index';
$route['blog/(:any)'] = 'blog/detail/$1';
$route['administrator/web-page'] = 'page/backend/page/admin';


$route[ADMIN_NAMESPACE_URL.'/manage-form/(:any)'] = 'form/backend/$1';
$route[ADMIN_NAMESPACE_URL.'/manage-form/(:any)/(:any)'] = 'form/backend/$1/$2';
$route[ADMIN_NAMESPACE_URL.'/manage-form/(:any)/(:any)/(:any)'] = 'form/backend/$1/$2/$3';
$route[ADMIN_NAMESPACE_URL.'/manage-form/(:any)/(:any)/(:any)/(:any)'] = 'form/backend/$1/$2/$3/$4';

$route[ADMIN_NAMESPACE_URL.'/(:any)'] = '$1/backend/$1';
$route[ADMIN_NAMESPACE_URL.'/(:any)/(:any)'] = '$1/backend/$1/$2';
$route[ADMIN_NAMESPACE_URL.'/(:any)/(:any)/(:any)'] = '$1/backend/$1/$2/$3';
$route[ADMIN_NAMESPACE_URL.'/(:any)/(:any)/(:any)/(:any)'] = '$1/backend/$1/$2/$3/$3';




$route['api/user/(:any)'] = 'api/user/$1';
$route['api/group/(:any)'] = 'api/group/$1';

$route['api/(:any)'] = '$1/api/$1';
$route['api/(:any)/(:any)'] = '$1/api/$1/$2';
$route['api/(:any)/(:any)/(:any)'] = '$1/api/$1/$2/$3';
$route['api/(:any)/(:any)/(:any)/(:any)'] = '$1/api/$1/$2/$3/$3';

$route['default_controller'] = 'frontend/home'; //front end web

// front end

$route['LanguageSwitcher/switchLang/(:any)'] = 'frontend/LanguageSwitcher/switchLang/$1';

$route['auth'] = 'frontend/auth';
$route['auth/register_confirm'] = 'frontend/auth/register_confirm';
$route['auth/login_confirm'] = 'frontend/auth/login_confirm';
$route['logout'] = 'frontend/auth/logout';


$route['account.html'] = 'frontend/member/account';
$route['account/account_update'] = 'frontend/member/account/account_update';
$route['account/profile_upload'] = 'frontend/member/account/profile_upload';

$route['address.html'] = 'frontend/member/address';
$route['address/get_amphoe/(:any)'] = 'frontend/member/address/get_amphoe/$1';
$route['address/get_tambon/(:any)'] = 'frontend/member/address/get_tambon/$1';
$route['address/get_zipcode/(:any)'] = 'frontend/member/address/get_zipcode/$1';
$route['address/get_data/(:any)'] = 'frontend/member/address/get_data/$1';
$route['address/address_add'] = 'frontend/member/address/address_add/';
$route['address/address_edit'] = 'frontend/member/address/address_edit/';
$route['address/set_main/(:any)'] = 'frontend/member/address/set_main/$1';
$route['address/delete_address/(:any)'] = 'frontend/member/address/delete_address/$1';


$route['notification.html'] = 'frontend/member/account/notification';
$route['account/payment-methods.html'] = 'frontend/member/account/payment';
$route['account/payment_add'] = 'frontend/member/account/payment_add';
$route['account/delete_payment/(:any)'] = 'frontend/member/account/delete_payment/$1';
$route['account/set_mai_payment/(:any)'] = 'frontend/member/account/set_mai_payment/$1';



$route['favorite'] = 'frontend/member/account/favorite';
$route['my-project'] = 'frontend/member/account/my_project';
$route['my_project_update'] = 'frontend/member/account/my_project_update';
$route['sponsor'] = 'frontend/member/account/sponsor';


/* project  */
$route['project-create'] = 'frontend/project/project';
$route['project-edit-(:num)'] = 'frontend/project/project/project_edit/$1';

$route['project-list-backup'] = 'frontend/project/project/list';
$route['project-list'] = 'frontend/project/project/list';
$route['project-detail/(:any)'] = 'frontend/project/project/detail/$1';
$route['payment-project-(:num)'] = 'frontend/project/project/payment_project/$1';

$route['project/in_favourite/(:any)'] = 'frontend/project/project/in_favourite/$1';
$route['project/un_favourite/(:any)'] = 'frontend/project/project/un_favourite/$1';
$route['project/in_like/(:any)'] = 'frontend/project/project/in_like/$1';
$route['project/un_like/(:any)'] = 'frontend/project/project/un_like/$1';
$route['project/comment/(:any)'] = 'frontend/project/project/comment/$1';
$route['project/save_comment/(:any)'] = 'frontend/project/project/save_comment/$1';
$route['project/comment/(:any)/(:any)/(:any)'] = 'frontend/project/project/comment/$1/$1/$1';

$route['donate/project'] = 'frontend/donate/project';
$route['donate/payment'] = 'frontend/donate/payment';

$route['basic-info-(:num)'] = 'frontend/project/project/basic_info/$1';
$route['rewards'] = 'frontend/project/project/rewards';
$route['rewards-add'] = 'frontend/project/project/rewards_add';
$route['rewards-edit-(:num)'] = 'frontend/project/project/rewards_edit/$1';
$route['story-(:num)'] = 'frontend/project/project/story/$1';


$route['news-detail-(:num)'] = 'frontend/project/project/news_detail/$1';

$route['terms.html'] = 'frontend/content/terms';
$route['guide.html'] = 'frontend/content/guide';
$route['trust.html'] = 'frontend/content/trust';
$route['policy.html'] = 'frontend/content/policy';
$route['prohibited.html'] = 'frontend/content/prohibited';

$route['profile'] = 'frontend/profile';
$route['search'] = 'frontend/search';
$route['upload/image'] = 'frontend/upload/image';