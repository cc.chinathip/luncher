<?php

if(!function_exists('set_day_letf')) {
	function set_day_letf($date_end='') {
		$date_now = date_create(date('Y-m-d'));
		$date_end = date_create($date_end);
		$diff_det = date_diff($date_now,$date_end);
		return $diff_det->format("%a");
	}
}

if(!function_exists('count_day')) {
	function count_day($date_start='') {
		$date_now = date_create(date('Y-m-d'));
		$date_start = date_create($date_start);
		$diff_det = date_diff($date_now,$date_start);
		return $diff_det->format("%a");
	}
}

if(!function_exists('check_favourite')) {
	function check_favourite($pid,$mid) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

		$favourite = $ci->model_db->db_select(
			'favourite',false,
			['favourite_id'],['favourite_project_id'=>$pid,'favourite_created_by'=>$mid]
		);

		if (isset($favourite) && !empty($favourite)) {
			return 'active un_favourite';
		}else{
			return 'in_favourite';
		}
	}
}

if(!function_exists('check_like')) {
	function check_like($pid,$mid) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

		$like = $ci->model_db->db_select(
			'like',false,
			['like_id'],['like_project_id'=>$pid,'like_created_by'=>$mid]
		);

		if (isset($like) && !empty($like)) {
			return 'active un_like';
		}else{
			return 'in_like';
		}
	}
}


if(!function_exists('lang_id')) {
	function lang_id() {
		$ci =& get_instance();
		return (($ci->session->userdata('site_lang') == 'thai') ? 1 : 2);
	}
}

if(!function_exists('project_home_by_category')) {
	function project_home_by_category($category_id) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

		$join_member = [
	        [
	            'member',
	            'member_id',
	            'project',
	            'project_created_by',
	            'INNER'
	        ]
	    ];

	    $where_project = [
	    	'project_category_id' => $category_id,
	      	'project_date_start <=' => date('Y-m-d'),
	      	'project_date_end >=' => date('Y-m-d')
	    ];

	    $project = $ci->model_db->db_select('project',true,[],$where_project,'project_date_end ASC',$join_member,[3,0]);

		
		if (isset($project) && !empty($project)) {
			return $project;
		}else{
			return [];
		}
	}
}

if(!function_exists('menu_category')) {
	function menu_category() {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

		$lang_id = (($ci->session->userdata('site_lang') == 'thai') ? 1 : 2);

	   	$join_category = [
          	[
              'project_category_description',
              'project_category_description_project_category_id',
              'project_category',
              'project_category_id',
              'INNER'
          	]
      	];

	      $where_category = [
	        'project_category_status' => 1,
	        'project_category_description_language_id' => $lang_id
	      ];

	      $project_category = $ci->model_db->db_select('project_category',true,[],$where_category,'project_category_id ASC',$join_category,['all',0]);

		if (isset($project_category) && !empty($project_category)) {
			return $project_category;
		}else{
			return [];
		}
	}
}

if(!function_exists('province')) {
	function province() {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

	    $province = $ci->model_db->db_select('province',false,[],[],'',[],['all',0]);

		if (isset($province) && !empty($province)) {
			return $province;
		}else{
			return [];
		}
	}
}


if(!function_exists('menu_subcategory')) {
	function menu_subcategory($category) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

		$lang_id = (($ci->session->userdata('site_lang') == 'thai') ? 1 : 2);

	   	$join_category = [
          	[
              'project_subcategory_description',
              'project_subcategory_description_project_subcategory_id',
              'project_subcategory',
              'project_subcategory_id',
              'INNER'
          	]
      	];

	      $where_category = [
	      	'project_subcategory_project_category_id' => $category,
	        'project_subcategory_status' => 1,
	        'project_subcategory_description_language_id' => $lang_id
	      ];

	      $project_subcategory = $ci->model_db->db_select('project_subcategory',true,[],$where_category,'project_subcategory_id ASC',$join_category,['all',0]);

		if (isset($project_subcategory) && !empty($project_subcategory)) {
			return $project_subcategory;
		}else{
			return [];
		}
	}
}

if(!function_exists('account_donate_count')) {
	function account_donate_count($mid) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

	    $donate_count = $ci->model_db->db_countrows('donate',['donate_created_by'=>$mid]);

		if (isset($donate_count) && !empty($donate_count)) {
			return $donate_count;
		}else{
			return 0;
		}
	}
}

if(!function_exists('account_project_count')) {
	function account_project_count($mid) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

	    $project_count = $ci->model_db->db_countrows('project',['project_created_by'=>$mid]);

		if (isset($project_count) && !empty($project_count)) {
			return $project_count;
		}else{
			return 0;
		}
	}
}

if(!function_exists('donate_count')) {
	function donate_count($project_id) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

	    $donate_count = $ci->model_db->db_countrows('donate',['donate_project_id'=>$project_id]);

		if (isset($donate_count) && !empty($donate_count)) {
			return $donate_count;
		}else{
			return 0;
		}
	}
}

if(!function_exists('donate_total')) {
	function donate_total($project_id,$project_target) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_db');

		
	    $donate_total = $ci->model_db->db_sum('donate','donate_amount',['donate_project_id'=>$project_id]);
		if (isset($donate_total) && $donate_total[0]['donate_amount'] != NULL) {

			if ($project_target == NULL) {
				$per_cen = 0;
			}else{
				$per_cen = (intval($donate_total[0]['donate_amount']) / intval($project_target)) * 100;
			}

			return [
				'total' => $donate_total[0]['donate_amount'],
				'per_cen' => $per_cen
			];
		}else{
			return ['total' => 0,'per_cen' => 0];
		}
	}
}