<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['project_subcategory'] = 'Project Subcategory';
$lang['project_subcategory_id'] = 'Id';
$lang['project_subcategory_project_category_id'] = 'Category Id';
$lang['project_subcategory_name'] = 'Name';
$lang['project_subcategory_status'] = 'Status';
$lang['project_subcategory_created_at'] = 'Created At';
$lang['project_subcategory_created_by'] = 'Created By';
