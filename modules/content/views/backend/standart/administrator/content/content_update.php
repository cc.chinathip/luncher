

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Content        <small>Edit Content</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/content'); ?>">Content</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Content</h3>
                            <h5 class="widget-user-desc">Edit Content</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/content/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_content', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_content', 
                            'method'  => 'POST'
                            ]); ?>

                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8">
                                  <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                      <?php foreach (db_get_all_data('language') as $key => $row): ?>
                                      <li class="<?= ($key == 0) ? 'active' : '' ?>"><a href="#tab_<?= $row->language_id ?>" data-toggle="tab" aria-expanded="true"><?= $row->language_name ?></a></li>
                                      <?php endforeach; ?>  
                                    </ul>
                                    <div class="tab-content">
                                      <?php foreach (db_get_all_data('language') as $key => $row): ?>
                                      <div class="tab-pane <?= ($key == 0) ? 'active' : '' ?>" id="tab_<?= $row->language_id ?>">
                                       
                                        <div class="form-group ">
                                            <label for="content_description_name_<?= $row->language_id ?>">Name 
                                            <i class="required">*</i>
                                            </label>
                                            <input type="text" class="form-control" name="content_description_name[<?= $row->language_id ?>]" id="content_description_name_<?= $row->language_id ?>" placeholder="Name" value="<?= set_value('content_name',$content_description[$key]->content_description_name); ?>">
                                            <small class="info help-block">
                                            <b>Input Content Name</b> Max Length : 250.</small>
                                        </div>
                                                                 
                                        <div class="form-group ">
                                            <label for="content_description_detail_<?= $row->language_id ?>">Detail 
                                            </label>
                                            <textarea class="content_description_detail" name="content_description_detail[<?= $row->language_id ?>]" id="content_description_detail_<?= $row->language_id ?>" rows="5" cols="80"><?= $content_description[$key]->content_description_detail ?></textarea>
                                            <small class="info help-block">
                                            </small>
                                        </div>
                                      </div>
                                      <?php endforeach; ?>  
                                       <div class="form-group ">
                                          <label for="content_status">Status 
                                          <i class="required">*</i>
                                          </label>
                                              <select  class="form-control chosen chosen-select-deselect" name="content_status" id="content_status" data-placeholder="Select Status" >
                                                  <option value=""></option>
                                                  <?php foreach (db_get_all_data('status') as $row): ?>
                                                  <option <?=  $row->status_id ==  $content->content_status ? 'selected' : ''; ?> value="<?= $row->status_id ?>"><?= $row->status_title; ?></option>
                                                  <?php endforeach; ?>  
                                              </select>
                                              <small class="info help-block">
                                              </small>
                                        </div>
                                      <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                  </div>
                                 
                                </div>
                            </div>
                                                
                            <div class="message"></div>
                            <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<!-- Page script -->
<script>
    $(document).ready(function(){

      $('.content_description_detail').each(function(e){
          CKEDITOR.replace( this.id);
      });
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/content';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
        
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].updateElement();
        };
        
        var form_content = $('#form_content');
        var data_post = form_content.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_content.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#content_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>