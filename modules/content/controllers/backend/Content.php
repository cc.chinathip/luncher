<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Content Controller
*| --------------------------------------------------------------------------
*| Content site
*|
*/
class Content extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_content');
		$this->load->model('content_description/model_content_description');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Contents
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('content_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['contents'] = $this->model_content->get($filter, $field, $this->limit_page, $offset);
		$this->data['content_counts'] = $this->model_content->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/content/index/',
			'total_rows'   => $this->model_content->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Content List');
		$this->render('backend/standart/administrator/content/content_list', $this->data);
	}
	
	/**
	* Add new contents
	*
	*/
	public function add()
	{
		$this->is_allowed('content_add');

		$this->template->title('Content New');
		$this->render('backend/standart/administrator/content/content_add', $this->data);
	}

	/**
	* Add New Contents
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('content_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		// $this->form_validation->set_rules('content_name', 'Name', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('content_status', 'Status', 'trim|required');

		if ($this->form_validation->run()) {
			
			$language = db_get_all_data('language');

			$save_data = [
				'content_name' => $this->input->post('content_description_name')[$language[0]->language_id],
				'content_status' => $this->input->post('content_status'),
				'content_created_by' => get_user_data('id'),
				'content_created_at' => date('Y-m-d H:i:s'),
			];

			
			$save_content = $this->model_content->store($save_data);
            
            foreach ($language as $row){
				$save_data = [
					'content_description_content_id' => $save_content,
					'content_description_language_id' => $row->language_id,
					'content_description_name' => $this->input->post('content_description_name')[$row->language_id],
					'content_description_detail' => $this->input->post('content_description_detail')[$row->language_id],
					'content_description_status' => $this->input->post('content_status'),
					'content_description_created_by' => get_user_data('id'),
					'content_description_created_at' => date('Y-m-d H:i:s')
				];

				$save_content_description = $this->model_content_description->store($save_data);
			}
			

            $save_data_logs_system = [
				'logs_system_table_name' => 'content',
				'logs_system_data_id' => $save_content,
				'logs_system_title' => 'add data',
				'logs_system_desc' => 'user add data to content',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_content->logs_insert($save_data_logs_system);

			if ($save_content) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_content;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/content/edit/' . $save_content, 'Edit Content'),
						anchor('administrator/content', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/content/edit/' . $save_content, 'Edit Content')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/content');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/content');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Contents
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('content_update');

		$this->data['content'] = $this->model_content->find($id);
		$this->data['content_description'] = $this->model_content_description->get_where(['content_description_content_id'=>$id]);
		$this->template->title('Content Update');
		$this->render('backend/standart/administrator/content/content_update', $this->data);
	}

	/**
	* Update Contents
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('content_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		// $this->form_validation->set_rules('content_name', 'Name', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('content_status', 'Status', 'trim|required');
		
		if ($this->form_validation->run()) {
			
			$language = db_get_all_data('language');

			$save_data = [
				'content_name' => $this->input->post('content_description_name')[$language[0]->language_id],
				'content_status' => $this->input->post('content_status'),
						'content_updated_by' => get_user_data('id'),
			'content_updated_at' => date('Y-m-d H:i:s'),
			];

			
			$save_content = $this->model_content->change($id, $save_data);

			foreach ($language as $row){
				$save_data = [
					'content_description_name' => $this->input->post('content_description_name')[$row->language_id],
					'content_description_detail' => $this->input->post('content_description_detail')[$row->language_id],
					'content_description_status' => $this->input->post('content_status'),
					'content_description_updated_by' => get_user_data('id'),
					'content_description_updated_at' => date('Y-m-d H:i:s')
				];

				$save_content_description = $this->model_content_description->db_update(
					$save_data,
					['content_description_content_id'=>$id,'content_description_language_id'=>$row->language_id]
				);
			}

			$save_data_logs_system = [
				'logs_system_table_name' => 'content',
				'logs_system_data_id' => $id,
				'logs_system_title' => 'edit data',
				'logs_system_desc' => 'user edit data to content',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_content->logs_insert($save_data_logs_system);

			if ($save_content) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/content', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/content');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/content');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Contents
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('content_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_deleted($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_deleted($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'content'), 'success');
        } else {
            set_message(cclang('error_delete', 'content'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Contents
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('content_view');

		$this->data['content'] = $this->model_content->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Content Detail');
		$this->render('backend/standart/administrator/content/content_view', $this->data);
	}
	
	/**
	* delete Contents
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$content = $this->model_content->find($id);

		
		
		return $this->model_content->remove($id);
	}

	private function _deleted($id)
	{
		$content = $this->model_content->find($id);

		$save_data = [
			'content_deleted' => 1,
			'content_updated_by' => get_user_data('id'),
			'content_updated_at' => date('Y-m-d H:i:s')
		];
		
		$save_data_delete = $this->model_content->change($id, $save_data);

		$save_data_logs_system = [
			'logs_system_table_name' => 'content',
			'logs_system_data_id' => $id,
			'logs_system_title' => 'delete data',
			'logs_system_desc' => 'user delete data',
			'logs_system_created_by' => get_user_data('id'),
		];

		$save_logs_system = $this->model_content->logs_insert($save_data_logs_system);

		return $save_data_delete;
	}

	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('content_export');

		$this->model_content->export('content', 'content');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('content_export');

		$this->model_content->pdf('content', 'content');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('content_export');

		$table = $title = 'content';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_content->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file content.php */
/* Location: ./application/controllers/administrator/Content.php */