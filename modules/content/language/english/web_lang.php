<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['content'] = 'Content';
$lang['content_id'] = 'Id';
$lang['content_name'] = 'Name';
$lang['content_status'] = 'Status';
$lang['content_created_by'] = 'Created By';
$lang['content_created_at'] = 'Created At';
