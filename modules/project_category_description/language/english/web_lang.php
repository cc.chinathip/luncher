<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['project_category_description'] = 'Project Category Description';
$lang['project_category_description_id'] = 'Project Category Description Id';
$lang['project_category_description_project_category_id'] = 'Project Category Description Project Category Id';
$lang['project_category_description_language_id'] = 'Project Category Description Language Id';
$lang['project_category_description_name'] = 'Project Category Description Name';
$lang['project_category_description_detail'] = 'Project Category Description Detail';
$lang['project_category_description_status'] = 'Project Category Description Status';
$lang['project_category_description_created_by'] = 'Project Category Description Created By';
$lang['project_category_description_created_at'] = 'Project Category Description Created At';
