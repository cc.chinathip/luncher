<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Project Subcategory Description Controller
*| --------------------------------------------------------------------------
*| Project Subcategory Description site
*|
*/
class Project_subcategory_description extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_project_subcategory_description');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Project Subcategory Descriptions
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('project_subcategory_description_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['project_subcategory_descriptions'] = $this->model_project_subcategory_description->get($filter, $field, $this->limit_page, $offset);
		$this->data['project_subcategory_description_counts'] = $this->model_project_subcategory_description->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/project_subcategory_description/index/',
			'total_rows'   => $this->model_project_subcategory_description->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Project Subcategory Description List');
		$this->render('backend/standart/administrator/project_subcategory_description/project_subcategory_description_list', $this->data);
	}
	
	/**
	* Add new project_subcategory_descriptions
	*
	*/
	public function add()
	{
		$this->is_allowed('project_subcategory_description_add');

		$this->template->title('Project Subcategory Description New');
		$this->render('backend/standart/administrator/project_subcategory_description/project_subcategory_description_add', $this->data);
	}

	/**
	* Add New Project Subcategory Descriptions
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('project_subcategory_description_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('project_subcategory_description_project_subcategory_id', 'Project Subcategory Description Project Subcategory Id', 'trim|required');
		$this->form_validation->set_rules('project_subcategory_description_language_id', 'Project Subcategory Description Language Id', 'trim|required');
		$this->form_validation->set_rules('project_subcategory_description_name', 'Project Subcategory Description Name', 'trim|required|max_length[250]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'project_subcategory_description_project_subcategory_id' => $this->input->post('project_subcategory_description_project_subcategory_id'),
				'project_subcategory_description_language_id' => $this->input->post('project_subcategory_description_language_id'),
				'project_subcategory_description_name' => $this->input->post('project_subcategory_description_name'),
				'project_subcategory_description_status' => $this->input->post('project_subcategory_description_status'),
						'project_subcategory_description_created_by' => get_user_data('id'),
			'project_subcategory_description_created_at' => date('Y-m-d H:i:s'),
			];

			
			$save_project_subcategory_description = $this->model_project_subcategory_description->store($save_data);
            
            $save_data_logs_system = [
				'logs_system_table_name' => 'project_subcategory_description',
				'logs_system_data_id' => $save_project_subcategory_description,
				'logs_system_title' => 'add data',
				'logs_system_desc' => 'user add data to project_subcategory_description',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_project_subcategory_description->logs_insert($save_data_logs_system);

			if ($save_project_subcategory_description) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_project_subcategory_description;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/project_subcategory_description/edit/' . $save_project_subcategory_description, 'Edit Project Subcategory Description'),
						anchor('administrator/project_subcategory_description', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/project_subcategory_description/edit/' . $save_project_subcategory_description, 'Edit Project Subcategory Description')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/project_subcategory_description');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/project_subcategory_description');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Project Subcategory Descriptions
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('project_subcategory_description_update');

		$this->data['project_subcategory_description'] = $this->model_project_subcategory_description->find($id);

		$this->template->title('Project Subcategory Description Update');
		$this->render('backend/standart/administrator/project_subcategory_description/project_subcategory_description_update', $this->data);
	}

	/**
	* Update Project Subcategory Descriptions
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('project_subcategory_description_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('project_subcategory_description_project_subcategory_id', 'Project Subcategory Description Project Subcategory Id', 'trim|required');
		$this->form_validation->set_rules('project_subcategory_description_language_id', 'Project Subcategory Description Language Id', 'trim|required');
		$this->form_validation->set_rules('project_subcategory_description_name', 'Project Subcategory Description Name', 'trim|required|max_length[250]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'project_subcategory_description_project_subcategory_id' => $this->input->post('project_subcategory_description_project_subcategory_id'),
				'project_subcategory_description_language_id' => $this->input->post('project_subcategory_description_language_id'),
				'project_subcategory_description_name' => $this->input->post('project_subcategory_description_name'),
				'project_subcategory_description_status' => $this->input->post('project_subcategory_description_status'),
						'project_subcategory_description_updated_by' => get_user_data('id'),
			'project_subcategory_description_updated_at' => date('Y-m-d H:i:s'),
			];

			
			$save_project_subcategory_description = $this->model_project_subcategory_description->change($id, $save_data);

			$save_data_logs_system = [
				'logs_system_table_name' => 'project_subcategory_description',
				'logs_system_data_id' => $id,
				'logs_system_title' => 'edit data',
				'logs_system_desc' => 'user edit data to project_subcategory_description',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_project_subcategory_description->logs_insert($save_data_logs_system);

			if ($save_project_subcategory_description) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/project_subcategory_description', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/project_subcategory_description');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/project_subcategory_description');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Project Subcategory Descriptions
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('project_subcategory_description_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_deleted($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_deleted($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'project_subcategory_description'), 'success');
        } else {
            set_message(cclang('error_delete', 'project_subcategory_description'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Project Subcategory Descriptions
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('project_subcategory_description_view');

		$this->data['project_subcategory_description'] = $this->model_project_subcategory_description->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Project Subcategory Description Detail');
		$this->render('backend/standart/administrator/project_subcategory_description/project_subcategory_description_view', $this->data);
	}
	
	/**
	* delete Project Subcategory Descriptions
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$project_subcategory_description = $this->model_project_subcategory_description->find($id);

		
		
		return $this->model_project_subcategory_description->remove($id);
	}

	private function _deleted($id)
	{
		$project_subcategory_description = $this->model_project_subcategory_description->find($id);

		$save_data = [
			'project_subcategory_description_deleted' => 1,
			'project_subcategory_description_updated_by' => get_user_data('id'),
			'project_subcategory_description_updated_at' => date('Y-m-d H:i:s')
		];
		
		$save_data_delete = $this->model_project_subcategory_description->change($id, $save_data);

		$save_data_logs_system = [
			'logs_system_table_name' => 'project_subcategory_description',
			'logs_system_data_id' => $id,
			'logs_system_title' => 'delete data',
			'logs_system_desc' => 'user delete data',
			'logs_system_created_by' => get_user_data('id'),
		];

		$save_logs_system = $this->model_project_subcategory_description->logs_insert($save_data_logs_system);

		return $save_data_delete;
	}

	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('project_subcategory_description_export');

		$this->model_project_subcategory_description->export('project_subcategory_description', 'project_subcategory_description');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('project_subcategory_description_export');

		$this->model_project_subcategory_description->pdf('project_subcategory_description', 'project_subcategory_description');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('project_subcategory_description_export');

		$table = $title = 'project_subcategory_description';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_project_subcategory_description->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file project_subcategory_description.php */
/* Location: ./application/controllers/administrator/Project Subcategory Description.php */