<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_project_subcategory_description extends MY_Model {

    private $primary_key    = 'project_subcategory_description_id';
    private $table_name     = 'project_subcategory_description';
    private $field_search   = ['project_subcategory_description_project_subcategory_id', 'project_subcategory_description_language_id', 'project_subcategory_description_name', 'project_subcategory_description_status', 'project_subcategory_description_created_by', 'project_subcategory_description_updated_by'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "project_subcategory_description.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "project_subcategory_description.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "project_subcategory_description.".$field . " LIKE '%" . $q . "%' )";
        }

        if($where != NULL){
            $where .= ' AND project_subcategory_description.project_subcategory_description_deleted = 0';
        }else{
            $where = ' project_subcategory_description.project_subcategory_description_deleted = 0';
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "project_subcategory_description.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "project_subcategory_description.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "project_subcategory_description.".$field . " LIKE '%" . $q . "%' )";
        }

        if($where != NULL){
            $where .= ' AND project_subcategory_description.project_subcategory_description_deleted = 0';
        }else{
            $where = ' project_subcategory_description.project_subcategory_description_deleted = 0';
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('project_subcategory_description.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function get_where($where=[])
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->result();
    }
    
    public function logs_insert($data){
        $this->db->insert('logs_system', $data);
        $this->db->insert_id();
    }

    public function db_update($data,$where=[]){
        $this->db->where($where);
        $this->db->update($this->table_name, $data);
    }

    public function join_avaiable() {
        $this->db->join('status', 'status.status_id = project_subcategory_description.project_subcategory_description_status', 'LEFT');
        $this->db->join('aauth_users', 'aauth_users.id = project_subcategory_description.project_subcategory_description_created_by', 'LEFT');
        
        $this->db->select('project_subcategory_description.*,status.status_title as status_status_title,aauth_users.email as aauth_users_email');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_project_subcategory_description.php */
/* Location: ./application/models/Model_project_subcategory_description.php */