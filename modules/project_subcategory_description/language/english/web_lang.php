<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['project_subcategory_description'] = 'Project Subcategory Description';
$lang['project_subcategory_description_id'] = 'Project Subcategory Description Id';
$lang['project_subcategory_description_project_subcategory_id'] = 'Project Subcategory Description Project Subcategory Id';
$lang['project_subcategory_description_language_id'] = 'Project Subcategory Description Language Id';
$lang['project_subcategory_description_name'] = 'Project Subcategory Description Name';
$lang['project_subcategory_description_status'] = 'Project Subcategory Description Status';
$lang['project_subcategory_description_created_by'] = 'Project Subcategory Description Created By';
$lang['project_subcategory_description_updated_by'] = 'Project Subcategory Description Updated By';
