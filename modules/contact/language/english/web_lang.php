<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['contact'] = 'Contact';
$lang['contact_id'] = 'Contact Id';
$lang['contact_project_id'] = 'Contact Project Id';
$lang['contact_status'] = 'Contact Status';
$lang['contact_created_at'] = 'Contact Created At';
$lang['contact_created_by'] = 'Contact Created By';
$lang['contact_updated_at'] = 'Contact Updated At';
$lang['contact_updated_by'] = 'Contact Updated By';
