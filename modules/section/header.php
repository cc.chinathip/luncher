<!DOCTYPE html>
<html lang="th">

<head> 
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes"/>

<title>Launcher</title>   
 
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap&subset=thai" rel="stylesheet"> 

<link href="<?= base_url('asset/launcher/fonts/fonts.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/animate.css') ?>" rel="stylesheet">  
<link href="<?= base_url('asset/launcher/css/swiper.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/jquery.fancybox.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/bootstrap-datepicker.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/global.css') ?>" rel="stylesheet">
 
</head>
 
<body>
<div class="preload"></div>
<header class="header">
	<div class="navbar navbar-topbar header-slideout">
		<div class="container">
			<ul class="nav nav-general">
				<li><button class="btn btn-light btn-search" data-toggle="modal" data-target="#searchModal"></button></li>
				<li><a class="btn" href="project-create.html">Start a Project</a></li>
				<li class="nav-login"><a class="btn btn-light" href="login.html"><span class="icons icon-key"></span> LOGIN</a></li>
				<li class="nav-register"><a class="btn btn-light" href="login.html"><span class="icons icon-lock"></span> REGISTER</a></li>
				<li class="nav-lang dropdown">
					<a data-toggle="dropdown" href="#"><span class="icons icon-global"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">TH</a></li>
						<li><a href="#">EN</a></li>
					</ul>
				</li>
			</ul><!--nav-general-->
		</div><!--container-->
	</div><!--navbar-topbar-->

	<div class="navbar navbar-device d-block d-lg-none header-slideout"> <!--show on screen < 991px (Tablet & Mobile)-->
		<div class="container">
			<div class="row">
				<div class="col-6">
					<div class="navbar-brand"><a href="index.html"><img src="img/logo.png" alt=""></a></div>
				</div>
				<div class="col-6">
					<button class="btn btn-icon navbar-toggle" type="button"> 
			            <span class="group">
					      <span></span>
					      <span></span>
					      <span></span>
					    </span>
			        </button>
				</div>
			</div><!--row-->
		</div><!--container-->
	</div><!--navbar-device-->

	<div class="navbar navbar-main">
		<div class="container">
			<div class="navbar-brand d-none d-lg-inline-block"><a href="index.html"><img src="img/logo.png" alt=""></a></div>

			<ul class="nav nav-main">
				<li class="nav-search d-block d-lg-none">
					<div class="search-general in-navbar">
						<button class="btn btn-light btn-search"></button>
						<input type="text" class="form-control" name=""> 
					</div>
				</li>

				<li class="nav-item dropdown">
					<a data-toggle="dropdown" href="#">Comics & Illustration</a>
					<ul class="dropdown-menu">
						<li class="nav-close"><a><span class="arrow"></span> Comics & Illustration</a></li>
						<li><a href="project.html">Comic Books</a></li>
						<li><a href="project.html">Graphic Novels</a></li>
						<li><a href="project.html">Webcomics</a></li>
						<li><a href="project.html">Anthologies</a></li>
					</ul><!--dropdown-menu-->
				</li>
				<li class="nav-item dropdown">
					<a data-toggle="dropdown" href="#">Design & Tech</a>
					<ul class="dropdown-menu">
						<li class="nav-close"><a><span class="arrow"></span> Design & Tech</a></li>
						<li><a href="project.html">Lorem ipsum dolor </a></li>
						<li><a href="project.html">Consectetur adipiscing</a></li>
						<li><a href="project.html">Mauris in magna</a></li>
					</ul><!--dropdown-menu-->
				</li> 
				<li class="nav-item"><a href="#">Food & Craft</a></li>
				<li class="nav-item"><a href="#">Games</a></li>
				<li class="nav-item"><a href="#">Music</a></li>
				<li class="nav-item"><a href="#">Mobile App</a></li>
				<li class="nav-item"><a href="#">Donation Project</a></li>
			</ul> 
		</div><!--container-->
	</div><!--navbar-general-->
</header>
