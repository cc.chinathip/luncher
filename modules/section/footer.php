<div class="footer">
		<div class="footer-primary">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<h3 class="footer-title d-block d-md-none">หมวดหมู่</h3>
						<ul class="footer-links category">
							<li><a href="project.html">Comics & Illustration</a></li>
							<li><a href="project.html">Design & Tech</a></li>
							<li><a href="project.html">Food & Craft</a></li>
							<li><a href="project.html">Games</a></li>
							<li><a href="project.html">Music</a></li>
							<li><a href="project.html">Mobile App</a></li>
							<li><a href="project.html">Donation Project </a></li>
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3 col-6">
						<h3 class="footer-title">เกี่ยวกับเรา</h3>
						<ul class="footer-links">
							<li><a href="help.html">ศูนย์ความช่วยเหลือ</a></li>
							<li><a href="#">เงื่อนไขและความเป็นส่วนตัว</a></li>
							<li><a href="#">แผนผังเว็บไซต์</a></li> 
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3 col-6">
						<h3 class="footer-title">กฏระเบียบการใช้บริการ</h3>
						<ul class="footer-links">
							<li><a href="#">คู่มือการสร้างแคมเปญ</a></li>
							<li><a href="#">Trust & Safety</a></li>
							<li><a href="#">Cookie & Policy</a></li>
							<li><a href="#">Prohibited item สิ่งของต้องห้าม</a></li> 
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3">
						<div class="footer-followus">
							<div class="inner">
								<div class="followus">
									<a class="icons icon-facebook" href="#" target="_blank"></a>
									<a class="icons icon-instagram"  href="#" target="_blank"></a>
									<a class="icons icon-twitter"  href="#" target="_blank"></a>
									<a class="icons icon-youtube"  href="#" target="_blank"></a>
								</div>

								<div class="dropdown currency">
									<a data-toggle="dropdown" href="#">Currency Exchange</a>
									<div class="dropdown-menu">
										<p class="text-center mb-0">DATA</p>
									</div><!--dropdown-menu-->
								</div><!--dropdown-->
							</div><!--inner-->
						</div><!--footer-followus-->
					</div><!--col-md-3-->
				</div><!--row-->
			</div><!--container-->
		</div><!--footer-primary-->

		<div class="footer-secondary">
			© 2020 All rights reserved. <span class="nowrap">Powerde by <a href="https://gramickhouse.com/" target="_blank">Gramick House</a></span>
		</div><!--footer-secondary-->
	</div><!--footer-->
	<!--====================== Modal ====================-->

<div class="modal modal-search fade" id="searchModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<button class="btn btn-close" data-dismiss="modal"></button>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="input-block search">
    					<input type="text" name="" class="form-control" placeholder="ค้นหาโปรเจคที่คุณสนใจ...">
    					<span class="icons icon-search"></span>
    				</div>
    			</div><!--col-md-12-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">หมวดหมู่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">พื้นที่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    		</div><!--row-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->

<script src="<?= base_url('asset/launcher/js/jquery-1.12.4.min.js') ?>"></script>
<script src="<?= base_url('asset/launcher/js/popper.min.js') ?>"></script>
<script src="<?= base_url('asset/launcher/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('asset/launcher/js/wow.js') ?>"></script>   
<script src="<?= base_url('asset/launcher/js/swiper.js') ?>"></script>     
<script src="<?= base_url('asset/launcher/js/jquery.fancybox.js') ?>"></script>   
<script src="<?= base_url('asset/launcher/js/ie11CustomProperties.js') ?>"></script>
<script src="<?= base_url('asset/launcher/js/jquery.customSelect.js') ?>"></script>  
<script src='<?= base_url('asset/launcher/js/circle-progress.js') ?>'></script> 
<script src='<?= base_url('asset/launcher/js/jquery.appear.js') ?>'></script>   
<script src='<?= base_url('asset/launcher/js/bootstrap-datepicker.js') ?>'></script>  
<script src='<?= base_url('asset/launcher/js/jquery.validate.js') ?>'></script>   
<script src="<?= base_url('asset/launcher/js/custom.js') ?>"></script>  

<script type="text/javascript">
	var swiperBanner = new Swiper('.swiper-banner', {
      speed: 1000,
      loop: true,
      observer: true,
      effect: 'slide',
      observeParents: true,
      autoplay: {
        delay: 7000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination.banner',
        clickable: true,
      },
      navigation: {
          nextEl: '.swiper-button-next.banner',
          prevEl: '.swiper-button-prev.banner',
      },
      
    });

    var swiperProject = new Swiper('.swiper-project', {
      speed: 1000,
      loop: false,
      observer: true,
      spaceBetween: 20,
      effect: 'slide',
      observeParents: true,
      pagination: {
        el: '.swiper-pagination.project',
        clickable: true,
      },
      navigation: {
          nextEl: '.swiper-button-next.project',
          prevEl: '.swiper-button-prev.project',
      },
      
    });

    //---

    var swiperNews = new Swiper('.swiper-news', {
    	slidesPerView: 4,
	    spaceBetween:30,
	    slidesPerGroup: 4,
	    speed: 800,
	    loop: false,
	    observer: true,
	    effect: 'slide',
	    observeParents: true,
	    pagination: {
	        el: '.swiper-pagination.news',
	        clickable: true,
	    },
	    navigation: {
	        nextEl: '.swiper-button-next.news',
	        prevEl: '.swiper-button-prev.news',
	    }, 
	    breakpoints: {
		    1440: {
		        slidesPerView: 4,  
		        slidesPerGroup: 4,
		        spaceBetween: 20,
		        speed: 700,
		    },
		    992: { 
	          	slidesPerView: 3,  
		        slidesPerGroup: 3,
		        spaceBetween: 20,
		        speed: 700,
		    },
		    767: {
		        slidesPerView: 1, 
		        slidesPerGroup: 1,
	          	spaceBetween: 10, 
		    }  
	    }
    });


    $('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 

    $(window).on('load resize', function(){
    	var window_size = $(window).width();
    	var p_size = 120;
    	var p_thickness = 15;

	  	if (window_size > 1281) {
	     	var p_size = 200;
    		var p_thickness = 30;
  
	  	}else if (window_size > 1025) {
	    	var p_size = 160;
    		var p_thickness = 20;	
	 	} 

	 	$('.progress-circle').each(function(){
	    	$(this).appear(function() {
		      $(this).find(".circle").circleProgress({
			    //value: .84,
			    fill: {color: '#21D695'},
			    emptyFill: 'rgba(244, 244, 244, .4)',
			    startAngle: -Math.PI / 4 * 2,
			    thickness:p_thickness,
			    size:p_size,
			  }).on('circle-animation-progress', function(event, progress, stepValue) {
			    $(this).find('strong').text(stepValue.toFixed(2).substr(2));
			  });
			}); 
		});
	});

   
  
</script>
 
</body>
</html>