<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['bank'] = 'Bank';
$lang['bank_id'] = 'Id';
$lang['bank_name'] = 'Name';
$lang['bank_status'] = 'Status';
$lang['bank_created_at'] = 'Created At';
$lang['bank_created_by'] = 'Created By';
