<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_content_description extends MY_Model {

    private $primary_key    = 'content_description_id';
    private $table_name     = 'content_description';
    private $field_search   = ['content_description_content_id', 'content_description_language_id', 'content_description_name', 'content_description_detail', 'content_description_status', 'content_description_deleted', 'content_description_created_by', 'content_description_updated_by', 'content_description_created_at', 'content_description_updated_at'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "content_description.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "content_description.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "content_description.".$field . " LIKE '%" . $q . "%' )";
        }

        if($where != NULL){
            $where .= ' AND content_description.content_description_deleted = 0';
        }else{
            $where = ' content_description.content_description_deleted = 0';
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "content_description.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "content_description.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "content_description.".$field . " LIKE '%" . $q . "%' )";
        }

        if($where != NULL){
            $where .= ' AND content_description.content_description_deleted = 0';
        }else{
            $where = ' content_description.content_description_deleted = 0';
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('content_description.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function get_where($where=[])
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->result();
    }
    
    public function logs_insert($data){
        $this->db->insert('logs_system', $data);
        $this->db->insert_id();
    }

    public function db_update($data,$where=[]){
        $this->db->where($where);
        $this->db->update($this->table_name, $data);
    }

    public function join_avaiable() {
        
        $this->db->select('content_description.*');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_content_description.php */
/* Location: ./application/models/Model_content_description.php */