<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Content Description Controller
*| --------------------------------------------------------------------------
*| Content Description site
*|
*/
class Content_description extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_content_description');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Content Descriptions
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('content_description_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['content_descriptions'] = $this->model_content_description->get($filter, $field, $this->limit_page, $offset);
		$this->data['content_description_counts'] = $this->model_content_description->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/content_description/index/',
			'total_rows'   => $this->model_content_description->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Content Description List');
		$this->render('backend/standart/administrator/content_description/content_description_list', $this->data);
	}
	
	/**
	* Add new content_descriptions
	*
	*/
	public function add()
	{
		$this->is_allowed('content_description_add');

		$this->template->title('Content Description New');
		$this->render('backend/standart/administrator/content_description/content_description_add', $this->data);
	}

	/**
	* Add New Content Descriptions
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('content_description_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('content_description_content_id', 'Content Description Content Id', 'trim|required');
		$this->form_validation->set_rules('content_description_language_id', 'Content Description Language Id', 'trim|required');
		$this->form_validation->set_rules('content_description_name', 'Content Description Name', 'trim|required');
		$this->form_validation->set_rules('content_description_status', 'Content Description Status', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'content_description_content_id' => $this->input->post('content_description_content_id'),
				'content_description_language_id' => $this->input->post('content_description_language_id'),
				'content_description_name' => $this->input->post('content_description_name'),
				'content_description_detail' => $this->input->post('content_description_detail'),
				'content_description_status' => $this->input->post('content_description_status'),
				'content_description_deleted' => $this->input->post('content_description_deleted'),
				'content_description_created_by' => $this->input->post('content_description_created_by'),
				'content_description_updated_by' => $this->input->post('content_description_updated_by'),
				'content_description_created_at' => $this->input->post('content_description_created_at'),
				'content_description_updated_at' => $this->input->post('content_description_updated_at'),
						'content_description_created_by' => get_user_data('id'),
			'content_description_created_at' => date('Y-m-d H:i:s'),
			];

			
			$save_content_description = $this->model_content_description->store($save_data);
            
            $save_data_logs_system = [
				'logs_system_table_name' => 'content_description',
				'logs_system_data_id' => $save_content_description,
				'logs_system_title' => 'add data',
				'logs_system_desc' => 'user add data to content_description',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_content_description->logs_insert($save_data_logs_system);

			if ($save_content_description) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_content_description;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/content_description/edit/' . $save_content_description, 'Edit Content Description'),
						anchor('administrator/content_description', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/content_description/edit/' . $save_content_description, 'Edit Content Description')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/content_description');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/content_description');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Content Descriptions
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('content_description_update');

		$this->data['content_description'] = $this->model_content_description->find($id);

		$this->template->title('Content Description Update');
		$this->render('backend/standart/administrator/content_description/content_description_update', $this->data);
	}

	/**
	* Update Content Descriptions
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('content_description_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('content_description_content_id', 'Content Description Content Id', 'trim|required');
		$this->form_validation->set_rules('content_description_language_id', 'Content Description Language Id', 'trim|required');
		$this->form_validation->set_rules('content_description_name', 'Content Description Name', 'trim|required');
		$this->form_validation->set_rules('content_description_status', 'Content Description Status', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'content_description_content_id' => $this->input->post('content_description_content_id'),
				'content_description_language_id' => $this->input->post('content_description_language_id'),
				'content_description_name' => $this->input->post('content_description_name'),
				'content_description_detail' => $this->input->post('content_description_detail'),
				'content_description_status' => $this->input->post('content_description_status'),
				'content_description_deleted' => $this->input->post('content_description_deleted'),
				'content_description_created_by' => $this->input->post('content_description_created_by'),
				'content_description_updated_by' => $this->input->post('content_description_updated_by'),
				'content_description_created_at' => $this->input->post('content_description_created_at'),
				'content_description_updated_at' => $this->input->post('content_description_updated_at'),
						'content_description_updated_by' => get_user_data('id'),
			'content_description_updated_at' => date('Y-m-d H:i:s'),
			];

			
			$save_content_description = $this->model_content_description->change($id, $save_data);

			$save_data_logs_system = [
				'logs_system_table_name' => 'content_description',
				'logs_system_data_id' => $id,
				'logs_system_title' => 'edit data',
				'logs_system_desc' => 'user edit data to content_description',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_content_description->logs_insert($save_data_logs_system);

			if ($save_content_description) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/content_description', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/content_description');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/content_description');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Content Descriptions
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('content_description_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_deleted($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_deleted($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'content_description'), 'success');
        } else {
            set_message(cclang('error_delete', 'content_description'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Content Descriptions
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('content_description_view');

		$this->data['content_description'] = $this->model_content_description->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Content Description Detail');
		$this->render('backend/standart/administrator/content_description/content_description_view', $this->data);
	}
	
	/**
	* delete Content Descriptions
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$content_description = $this->model_content_description->find($id);

		
		
		return $this->model_content_description->remove($id);
	}

	private function _deleted($id)
	{
		$content_description = $this->model_content_description->find($id);

		$save_data = [
			'content_description_deleted' => 1,
			'content_description_updated_by' => get_user_data('id'),
			'content_description_updated_at' => date('Y-m-d H:i:s')
		];
		
		$save_data_delete = $this->model_content_description->change($id, $save_data);

		$save_data_logs_system = [
			'logs_system_table_name' => 'content_description',
			'logs_system_data_id' => $id,
			'logs_system_title' => 'delete data',
			'logs_system_desc' => 'user delete data',
			'logs_system_created_by' => get_user_data('id'),
		];

		$save_logs_system = $this->model_content_description->logs_insert($save_data_logs_system);

		return $save_data_delete;
	}

	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('content_description_export');

		$this->model_content_description->export('content_description', 'content_description');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('content_description_export');

		$this->model_content_description->pdf('content_description', 'content_description');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('content_description_export');

		$table = $title = 'content_description';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_content_description->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file content_description.php */
/* Location: ./application/controllers/administrator/Content Description.php */