<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['content_description'] = 'Content Description';
$lang['content_description_id'] = 'Content Description Id';
$lang['content_description_content_id'] = 'Content Description Content Id';
$lang['content_description_language_id'] = 'Content Description Language Id';
$lang['content_description_name'] = 'Content Description Name';
$lang['content_description_detail'] = 'Content Description Detail';
$lang['content_description_status'] = 'Content Description Status';
$lang['content_description_deleted'] = 'Content Description Deleted';
$lang['content_description_created_by'] = 'Content Description Created By';
$lang['content_description_updated_by'] = 'Content Description Updated By';
$lang['content_description_created_at'] = 'Content Description Created At';
$lang['content_description_updated_at'] = 'Content Description Updated At';
