<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Logs System Controller
*| --------------------------------------------------------------------------
*| Logs System site
*|
*/
class Logs_system extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_logs_system');
		$this->load->model('model_logs_system');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Logs Systems
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('logs_system_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['logs_systems'] = $this->model_logs_system->get($filter, $field, $this->limit_page, $offset);
		$this->data['logs_system_counts'] = $this->model_logs_system->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/logs_system/index/',
			'total_rows'   => $this->model_logs_system->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Logs System List');
		$this->render('backend/standart/administrator/logs_system/logs_system_list', $this->data);
	}
	
	/**
	* Add new logs_systems
	*
	*/
	public function add()
	{
		$this->is_allowed('logs_system_add');

		$this->template->title('Logs System New');
		$this->render('backend/standart/administrator/logs_system/logs_system_add', $this->data);
	}

	/**
	* Add New Logs Systems
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('logs_system_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('logs_system_table_name', 'Logs System Table Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('logs_system_data_id', 'Logs System Data Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('logs_system_title', 'Logs System Title', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('logs_system_desc', 'Logs System Desc', 'trim|required');
		$this->form_validation->set_rules('logs_system_created_by', 'Logs System Created By', 'trim|required|max_length[10]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'logs_system_table_name' => $this->input->post('logs_system_table_name'),
				'logs_system_data_id' => $this->input->post('logs_system_data_id'),
				'logs_system_title' => $this->input->post('logs_system_title'),
				'logs_system_desc' => $this->input->post('logs_system_desc'),
				'logs_system_created_by' => $this->input->post('logs_system_created_by'),
				'logs_system_created_at' => date('Y-m-d H:i:s'),
						'logs_system_created_by' => get_user_data('id'),
			'logs_system_created_at' => date('Y-m-d H:i:s'),
			];

			
			$save_logs_system = $this->model_logs_system->store($save_data);
            
            $save_data_logs_system = [
				'logs_system_table_name' => 'logs_system',
				'logs_system_data_id' => $save_logs_system,
				'logs_system_title' => 'add data',
				'logs_system_desc' => 'user add data to logs_system',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_logs_system->store($save_data_logs_system);

			if ($save_logs_system) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_logs_system;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/logs_system/edit/' . $save_logs_system, 'Edit Logs System'),
						anchor('administrator/logs_system', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/logs_system/edit/' . $save_logs_system, 'Edit Logs System')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/logs_system');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/logs_system');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Logs Systems
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('logs_system_update');

		$this->data['logs_system'] = $this->model_logs_system->find($id);

		$this->template->title('Logs System Update');
		$this->render('backend/standart/administrator/logs_system/logs_system_update', $this->data);
	}

	/**
	* Update Logs Systems
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('logs_system_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('logs_system_table_name', 'Logs System Table Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('logs_system_data_id', 'Logs System Data Id', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('logs_system_title', 'Logs System Title', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('logs_system_desc', 'Logs System Desc', 'trim|required');
		$this->form_validation->set_rules('logs_system_created_by', 'Logs System Created By', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'logs_system_table_name' => $this->input->post('logs_system_table_name'),
				'logs_system_data_id' => $this->input->post('logs_system_data_id'),
				'logs_system_title' => $this->input->post('logs_system_title'),
				'logs_system_desc' => $this->input->post('logs_system_desc'),
				'logs_system_created_by' => $this->input->post('logs_system_created_by'),
				'logs_system_created_at' => date('Y-m-d H:i:s'),
						'logs_system_updated_by' => get_user_data('id'),
			'logs_system_updated_at' => date('Y-m-d H:i:s'),
			];

			
			$save_logs_system = $this->model_logs_system->change($id, $save_data);

			$save_data_logs_system = [
				'logs_system_table_name' => 'logs_system',
				'logs_system_data_id' => $id,
				'logs_system_title' => 'edit data',
				'logs_system_desc' => 'user edit data to logs_system',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_logs_system->store($save_data_logs_system);

			if ($save_logs_system) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/logs_system', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/logs_system');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/logs_system');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Logs Systems
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('logs_system_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'logs_system'), 'success');
        } else {
            set_message(cclang('error_delete', 'logs_system'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Logs Systems
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('logs_system_view');

		$this->data['logs_system'] = $this->model_logs_system->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Logs System Detail');
		$this->render('backend/standart/administrator/logs_system/logs_system_view', $this->data);
	}
	
	/**
	* delete Logs Systems
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$logs_system = $this->model_logs_system->find($id);

		
		
		return $this->model_logs_system->remove($id);
	}

	private function _deleted($id)
	{
		$logs_system = $this->model_logs_system->find($id);

		$save_data = [
			'logs_system_deleted' => 1,
			'logs_system_updated_by' => get_user_data('id'),
			'logs_system_updated_at' => date('Y-m-d H:i:s')
		];
		
		$save_data_delete = $this->model_logs_system->change($id, $save_data);

		$save_data_logs_system = [
			'logs_system_table_name' => 'logs_system',
			'logs_system_data_id' => $id,
			'logs_system_title' => 'delete data',
			'logs_system_desc' => 'user delete data',
			'logs_system_created_by' => get_user_data('id'),
		];

		$save_logs_system = $this->model_logs_system->store($save_data_logs_system);

		return $save_data_delete;
	}
	
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('logs_system_export');

		$this->model_logs_system->export('logs_system', 'logs_system');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('logs_system_export');

		$this->model_logs_system->pdf('logs_system', 'logs_system');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('logs_system_export');

		$table = $title = 'logs_system';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_logs_system->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file logs_system.php */
/* Location: ./application/controllers/administrator/Logs System.php */