<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['logs_system'] = 'Logs System';
$lang['logs_system_id'] = 'Logs System Id';
$lang['logs_system_table_name'] = 'Logs System Table Name';
$lang['logs_system_data_id'] = 'Logs System Data Id';
$lang['logs_system_title'] = 'Logs System Title';
$lang['logs_system_desc'] = 'Logs System Desc';
$lang['logs_system_created_by'] = 'Logs System Created By';
$lang['logs_system_created_at'] = 'Logs System Created At';
