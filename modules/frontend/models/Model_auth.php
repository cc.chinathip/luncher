<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_auth extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function login($username='',$password='')
	{
		$this->db->select('count(member_id) as have_me');
	    $this->db->from('member');
	    $this->db->where([
	    	'member_username' => $username,
	    	'member_status' => 1
	    ]);

	    $result = $this->db->get()->result();

	    if ($result[0]->have_me > 0) {
	    	return $this->verify_password($username,$password);
	    }else{
	    	return 'member_not_found';
	    }
	    
	}

	public function check_email($email='',$mid)
	{
		$this->db->select('count(member_id) as have_me');
	    $this->db->from('member');
	    $this->db->where([
	    	'member_email' => $email,
	    	'member_id !=' => $mid,
	    	'member_status' => 1
	    ]);

	    $result = $this->db->get()->result();

	    if ($result[0]->have_me > 0) {
	    	return true;
	    }

	    return false;
	    
	}

	public function check_email_forgot($email='')
	{
		$this->db->select('count(member_id) as have_me');
	    $this->db->from('member');
	    $this->db->where([
	    	'member_email' => $email,
	    	'member_status' => 1
	    ]);

	    $result = $this->db->get()->result();

	    if ($result[0]->have_me > 0) {
	    	return true;
	    }

	    return false;
	    
	}

	public function check_email_regis($email='')
	{
		$this->db->select('count(member_id) as have_me');
	    $this->db->from('member');
	    $this->db->where([
	    	'member_email' => $email,
	    	'member_status' => 1
	    ]);

	    $result = $this->db->get()->result();

	    if ($result[0]->have_me > 0) {
	    	return true;
	    }

	    return false;
	    
	}

	public function check_username_regis($username='')
	{
		$this->db->select('count(member_id) as have_me');
	    $this->db->from('member');
	    $this->db->where([
	    	'member_username' => $username,
	    	'member_status' => 1
	    ]);

	    $result = $this->db->get()->result();

	    if ($result[0]->have_me > 0) {
	    	return true;
	    }

	    return false;
	    
	}

	public function check_pass($password='',$mid)
	{
		$this->db->select('*');
	    $this->db->from('member');
	    $this->db->where([
	    	'member_id' => $mid,
	    	'member_status' => 1
	    ]);

	    $result = $this->db->get()->result()[0];


	    if (password_verify($password,$result->member_password)) {
	        return true;
	    }
	    
	    return false;
	}

	public function store($member_id)
	{
		$this->db->select('store_id');
	    $this->db->from('store');
	    $this->db->where([
	    	'store_member_id' => $member_id,
	    	'store_status' => 1
	    ]);

	    $result = $this->db->get()->result()[0];

	    return $result;
	   
	}

	private function verify_password($username,$password)
	{
		$this->db->select('*');
	    $this->db->from('member');
	    $this->db->where([
	    	'member_username' => $username,
	    	'member_status' => 1
	    ]);

	    $result = $this->db->get()->result()[0];


	    if (password_verify($password,$result->member_password)) {
	        return $result;
	    } else {
	        return 'password_not_match';
	    }
	}
}
