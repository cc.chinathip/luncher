<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_db extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function db_insert($table,$data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
		// return true;
	}

	public function db_update($table,$data,$where=[]){
		$this->db->where($where);
    	$this->db->update($table, $data);
		return true;
	}

	public function db_delete($table,$where=[]){
		$this->db->where($where);
		$this->db->delete($table);
		return true;
	}
	public function db_countrows($table,$where_select=[],$deleted=true,$offset=0)
	{
	    $this->db->select($table.'_id as total');
	    $this->db->from($table);

	    $where = [];
	    if ($deleted) {
	    	$where = [ 
		    	$table.'_deleted' => 0
		    ];
	    }

	    if (!empty($where_select)) {
			$where = array_merge($where,$where_select);
		}

	  	$this->db->where($where);
	  	$this->db->limit(100000000, $offset);
		
		return $this->db->count_all_results();
	}

	public function db_countrows_project_fillter($table,$where_select=[],$deleted=true,$offset=0,$fillter_search=[],$text_search=[])
	{
	    $this->db->select($table.'_id as total');
	    $this->db->from($table);

	    $where = [];
	    if ($deleted) {
	    	$where = [ 
		    	$table.'_deleted' => 0
		    ];
	    }

	    if (!empty($where_select)) {
			$where = array_merge($where,$where_select);
		}

	  	$this->db->where($where);
	  	if (!empty($fillter_search[1])) {
		  	$this->db->where_in($fillter_search[0],$fillter_search[1]);
		}

		if (!empty($text_search)) {
			foreach($text_search[1] as $key => $field) {
			    if($key == 0) {
			        $this->db->like($field, $text_search[0]);
			    } else {
			        $this->db->or_like($field, $text_search[0]);
			    }
			}
		}
		
	  	$this->db->limit(100000000, $offset);
		
		return $this->db->count_all_results();
	}

	public function db_sum($table,$field,$where_select=[],$deleted=true)
	{

		 $this->db
		    ->select_sum($field)
		    ->from($table);

	    $where = [];
	    if ($deleted) {
	    	$where = [ 
		    	$table.'_deleted' => 0
		    ];
	    }

	    if (!empty($where_select)) {
			$where = array_merge($where,$where_select);
		}

	  	$this->db->where($where);
		
		return $this->db->get()->result_array();
	}
	// para
	// [1] table type string
	// [2] delete : true & false
	// [3] field : select type array
	// [4] where : type array
	// [5] order by : type string
	// [6] limit & offset : array[0] = limit, array[1] = offset
	// [7] searc : array[0] = value search, array[1] = field search
	public function db_select($table,$deleted=true,$field=[],$where_select=[],$order_by='',$join=[],$limit=[10,0],$text_search=[])
	{
	    $this->db->select($field);
	    $this->db->from($table);

	    // var_dump($join);
	    if (!empty($join)) {
	    	foreach ($join as $v) {
	    		$this->db_join($v[0],$v[1],$v[2],$v[3],$v[4]);
	    	}
	    }

	    $where = [];
	    if ($deleted) {
	    	$where = [ 
		    	$table.'_deleted' => 0
		    ];
	    }

	    if (!empty($where_select)) {
			$where = array_merge($where,$where_select);
		}
		
		$this->db->where($where);

		if (!empty($text_search)) {
			foreach($text_search[1] as $key => $field) {
			    if($key == 0) {
			        $this->db->like($field, $text_search[0]);
			    } else {
			        $this->db->or_like($field, $text_search[0]);
			    }
			}
		}

		if ($limit[0] != 'all') {
			$this->db->limit($limit[0],$limit[1]);
		}
		
		if (!empty($order_by)) {
			$this->db->order_by($order_by);
		}else{
			$this->db->order_by($table.'_id ASC');
		}
		
	    $result = $this->db->get()->result();
	    return $result;
	}

	public function db_select_project_fillter($table,$deleted=true,$field=[],$where_select=[],$order_by='',$join=[],$limit=[10,0],$fillter_search=[],$text_search=[])
	{
	    $this->db->select($field);
	    $this->db->from($table);

	    // var_dump($join);
	    if (!empty($join)) {
	    	foreach ($join as $v) {
	    		$this->db_join($v[0],$v[1],$v[2],$v[3],$v[4]);
	    	}
	    }

	    $where = [];
	    if ($deleted) {
	    	$where = [ 
		    	$table.'_deleted' => 0
		    ];
	    }

	    if (!empty($where_select)) {
			$where = array_merge($where,$where_select);
		}
		
		$this->db->where($where);

		if (!empty($fillter_search[1])) {
			$this->db->where_in($fillter_search[0],$fillter_search[1]);
		}

		if (!empty($text_search)) {
			foreach($text_search[1] as $key => $field) {
			    if($key == 0) {
			        $this->db->like($field, $text_search[0]);
			    } else {
			        $this->db->or_like($field, $text_search[0]);
			    }
			}
		}

		if ($limit[0] != 'all') {
			$this->db->limit($limit[0],$limit[1]);
		}
		
		if (!empty($order_by)) {
			$this->db->order_by($order_by);
		}else{
			$this->db->order_by($table.'_id ASC');
		}
		
	    $result = $this->db->get()->result();
	    return $result;
	}

	public function db_join($table_from,$field_form,$table_to,$field_to,$join_type)
	{
		$this->db->join($table_from, $table_from.'.'.$field_form.' = '.$table_to.'.'.$field_to, $join_type);
		return $this;
	}

	public function join_lang($table,$field)
	{
		$this->db->join('languages', 'languages.language_id = '.$table.'.'.$field, 'INNER');
		return $this;
	}

}
