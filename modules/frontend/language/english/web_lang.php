<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['member'] = 'Member';
$lang['member_id'] = 'Id';
$lang['member_avatar'] = 'Avatar';
$lang['member_first_name'] = 'First Name';
$lang['member_last_name'] = 'Last Name';
$lang['member_email'] = 'Email';
$lang['member_phone_number'] = 'Phone Number';
$lang['member_status'] = 'Status';
$lang['member_last_login'] = 'Last Login';
$lang['member_created_at'] = 'Created At';
