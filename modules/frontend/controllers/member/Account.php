<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Account extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');
        $this->load->library('pagination');

        if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
	}


    public function index() 
    {   
        $data['menu_left'] = 'account';
        $data['account'] = $this->model_db->db_select('member',true,[],['member_id'=>$this->session->userdata('laun_id')])[0];
        $data['country'] = $this->model_db->db_select('country',false,[],[],'',[],['all',0]);
        $this->template->build('frontend/member/account',$data);
    }

    public function account_update() 
    {
        $this->form_validation->set_rules('member_username', 'username', 'trim|required');

        if ($this->form_validation->run()) {
           
            $data_account = [
                'member_username' => $this->input->post('member_username'),
                'member_first_name' => $this->input->post('member_first_name'),
                'member_last_name' => $this->input->post('member_last_name'),
                'member_gender' => $this->input->post('member_gender'),
                'member_phone_number' => $this->input->post('member_phone_number'),
                'member_country' => $this->input->post('member_country'),
                'member_date_open_project' => date("Y-m-d", strtotime($this->input->post('member_date_open_project'))),
                'member_biography' => $this->input->post('member_biography'),
                'member_email' => $this->input->post('member_email'),
                'member_updated_by' => 0,
                'member_updated_at' => date('Y-m-d H:i:s'),
            ];

            // date_format(date_create(strtotime($this->input->post('member_date_open_project'))),"Y-m-d")

            $this->model_db->db_update('member',$data_account,['member_id'=>$this->session->userdata('laun_id')]);

            $data['status'] = true;
               
        } else {
            $data['status'] = false;
            $data['message'] = validation_errors();
        }

        echo json_encode($data);
    }

    public function profile_upload()
    {
        $path_upload = 'uploads/member/';

        if (!is_dir($path_upload)) {
            mkdir($path_upload, 0777, TRUE);
        }

        $config['upload_path'] = $path_upload; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '1000000';

        $this->load->library('upload',$config); 

        if(!empty($_FILES['profile_image']['name'])){
            
            $_FILES['file']['name'] = $_FILES['profile_image']['name'];
            $_FILES['file']['type'] = $_FILES['profile_image']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['profile_image']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['profile_image']['error'];
            $_FILES['file']['size'] = $_FILES['profile_image']['size'];
  
            $config['file_name'] = md5($this->session->userdata('laun_id'));

            $this->upload->initialize($config);

            if($this->upload->do_upload('file')){
                $upload_data = $this->upload->data();
                $filename = $upload_data['file_name'];

                $data_store = [
                    'member_avatar' => $filename
                ];

                $data_save = $this->model_db->db_update('member',$data_store,
                    [
                        'member_id'=>$this->session->userdata('laun_id'),
                    ]
                );

                $member_data = array(
                    'laun_avatar'     => BASE_URL.'uploads/member/'.$filename,
                );
                $this->session->set_userdata($member_data);


                $resp['status'] = true;
                echo json_encode($resp);

            }

        }
    }

    public function notification() 
    {   
        $data['menu_left'] = 'notification';
        $this->template->build('frontend/member/notification',$data);
    }

    public function payment() 
    {   
        $data['menu_left'] = 'payment';

        $join = [
            [
                'bank',
                'bank_id',
                'member_bank',
                'member_bank_bank_id',
                'INNER'
            ]
        ];
  
        $where = [
            'member_bank_status' => 1,
            'member_bank_created_by' => $this->session->userdata('laun_id'),
        ];
  
        $data['member_bank'] = $this->model_db->db_select('member_bank',true,[],$where,'member_bank_id ASC',$join);
        $data['bank'] = $this->model_db->db_select('bank',true,[],['bank_status'=>1],'bank_name ASC',[]);

        $this->template->build('frontend/member/payment',$data);
    }

    public function payment_add() 
    {   
        $this->form_validation->set_rules('member_bank_fullname', 'fullname', 'trim|required');

        if ($this->form_validation->run()) {

            $data_member_bank = [
                'member_bank_fullname' => $this->input->post('member_bank_fullname'),
                'member_bank_card_id' => $this->input->post('member_bank_card_id'),
                'member_bank_account_name' => $this->input->post('member_bank_account_name'),
                'member_bank_number' => $this->input->post('member_bank_number'),
                'member_bank_bank_id' => $this->input->post('member_bank_bank_id'),
                'member_bank_main' => 0,
                'member_bank_status' => 1,
                'member_bank_created_by' => $this->session->userdata('laun_id'),
                'member_bank_created_at' => date('Y-m-d H:i:s'),
            ];

            $this->model_db->db_insert('member_bank',$data_member_bank);

            $data['status'] = true;
               
        } else {
            $data['status'] = false;
            $data['message'] = validation_errors();
        }

        echo json_encode($data);
    }

    public function delete_payment($id) 
    {
        $data_member_bank = [
            'member_bank_deleted' => 1,
        ];
        $this->model_db->db_update('member_bank',$data_member_bank,['member_bank_id'=>$id]);

        $data['status'] = true;

        echo json_encode($data);
    }

    public function set_mai_payment($id) 
    {
        $data_bank = [
            'member_bank_main' => 0,
        ];
        $this->model_db->db_update('member_bank',$data_bank,['member_bank_created_by'=>$this->session->userdata('laun_id')]);

        $data_bank = [
            'member_bank_main' => 1,
        ];
        $this->model_db->db_update('member_bank',$data_bank,['member_bank_id'=>$id]);

        $data['status'] = true;

        echo json_encode($data);
    }

    public function favorite() 
    {   
        $data['menu_left'] = 'favorite';

        $join = [
            [
                'project',
                'project_id',
                'favourite',
                'favourite_project_id',
                'INNER'
            ],
            [ 
                'member',
                'member_id',
                'project',
                'project_created_by',
                'INNER',
            ]
        ];
  
        $where = [
          'favourite_created_by' => $this->session->userdata('laun_id'),
          /* 'favourite_created_by' => 2, */
        ];
  
        $data['project_favor'] = $this->model_db->db_select('favourite',true,[],$where,'favourite_id ASC',$join);


        $this->template->build('frontend/member/favorite',$data);
    }

    public function my_project() 
    {   
        
        $limit = 10;
        if (isset($_GET)) {
            if (!empty($_GET['per_page'])) {
              $offset = $_GET['per_page'];
            }else{
              $offset = 0;
            }
        }


        $data['menu_left'] = 'my_project';

        $join = [
            [
                'member',
                'member_id',
                'project',
                'project_created_by',
                'INNER'
            ]
        ];
  
        $where = [
          'project_created_by' => $this->session->userdata('laun_id'),
        ];
  
        $data['project_list'] = $this->model_db->db_select('project',true,[],$where,'project_id ASC',$join,[$limit,$offset]);

        $data['total_rows'] = $this->model_db->db_countrows('project',$where,true);
        $data['offset'] = $offset;
        $config = [
            'base_url'     => BASE_URL.'my-project',
            'total_rows'   => $data['total_rows'],
            'per_page'     => $limit,
            'page_query_string'  => TRUE,
            'reuse_query_string'  => TRUE,
        ];

        $this->pagination->initialize($config);
      
        $data['pagination'] = $this->pagination->create_links();

        $this->template->build('frontend/member/my_project',$data);
    }

    public function my_project_update() 
    {   
        $this->form_validation->set_rules('project_id', 'project_id', 'trim|required');

        if ($this->form_validation->run()) {

            $data_project_update = [
                'project_update_project_id' => $this->input->post('project_id'),
                'project_update_detail' => $this->input->post('project_update_detail'),
                'project_update_created_by' => $this->session->userdata('laun_id'),
                'project_update_created_at' => date('Y-m-d H:i:s'),
            ];

            $this->model_db->db_insert('project_update',$data_project_update);

            $data['status'] = true;
               
        } else {
            $data['status'] = false;
            $data['message'] = validation_errors();
        }

        echo json_encode($data);
    }

    public function sponsor() 
    {   
        
        $limit = 10;
        if (isset($_GET)) {
            if (!empty($_GET['per_page'])) {
              $offset = $_GET['per_page'];
            }else{
              $offset = 0;
            }
        }


        $data['menu_left'] = 'sponsor';

        $join = [
            [
                'member',
                'member_id',
                'donate',
                'donate_created_by',
                'INNER'
            ],
            [
                'project',
                'project_id',
                'donate',
                'donate_project_id',
                'INNER'
            ]
        ];
  
        $where = [
          'donate_created_by' => $this->session->userdata('laun_id'),
        ];
  
        $data['donate_list'] = $this->model_db->db_select('donate',true,[],$where,'donate_id ASC',$join,[$limit,$offset]);

        $data['total_rows'] = $this->model_db->db_countrows('donate',$where,true);
        $data['offset'] = $offset;
        $config = [
            'base_url'     => BASE_URL.'sponsor',
            'total_rows'   => $data['total_rows'],
            'per_page'     => $limit,
            'page_query_string'  => TRUE,
            'reuse_query_string'  => TRUE,
        ];

        $this->pagination->initialize($config);
      
        $data['pagination'] = $this->pagination->create_links();

        $this->template->build('frontend/member/sponsor',$data);
    }

}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */