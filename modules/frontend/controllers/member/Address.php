<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Address extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');

        if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
	}


    public function index() 
    {   
        $data['menu_left'] = 'address';

        $join_address = [
            [
                'province',
                'province_id',
                'member_address',
                'member_address_province',
                'INNER'
            ],
            [
                'amphoe',
                'amphoe_id',
                'member_address',
                'member_address_amphoe',
                'INNER'
            ],
            [
                'tambon',
                'tambon_id',
                'member_address',
                'member_address_tambon',
                'INNER'
            ]
        ];

        $data['member_address'] = $this->model_db->db_select('member_address',true,[],['member_address_status'=>1,'member_address_member_id'=>$this->session->userdata('laun_id')],'member_address_id ASC',$join_address,[100,0]);
        // var_dump($data['member_address']);
        $data['province'] = $this->model_db->db_select('province',false,[],[],'province_name_th ASC',[],['all','all']);
        $this->template->build('frontend/member/address',$data);
    }

    public function get_data($id='')
    {
        $data['member_address'] = $this->model_db->db_select('member_address',true,[],['member_address_status'=>1,'member_address_id'=>$id],'member_address_id ASC',[],[100,0])[0];

        $data['province'] = $this->model_db->db_select('province',false,[],[],'province_name_th ASC',[],['all','all']);
        $data['amphoe'] = $this->model_db->db_select('amphoe',false,[],['amphoe_province_id'=>$data['member_address']->member_address_province],'amphoe_name_th ASC',[],[100,0]);
        $data['tambon'] = $this->model_db->db_select('tambon',false,[],['tambon_amphoe_id'=>$data['member_address']->member_address_amphoe],'tambon_name_th ASC',[],[100,0]);

        echo json_encode(['status'=>true,'data'=>$data]);
    }

    public function set_main($id) 
    {
        $data_address = [
            'member_address_main' => 0,
        ];
        $this->model_db->db_update('member_address',$data_address,['member_address_member_id'=>$this->session->userdata('laun_id')]);

        $data_address = [
            'member_address_main' => 1,
        ];
        $this->model_db->db_update('member_address',$data_address,['member_address_id'=>$id]);

        $data['status'] = true;

        echo json_encode($data);
    }

    public function delete_address($id) 
    {
        $data_address = [
            'member_address_deleted' => 1,
        ];
        $this->model_db->db_update('member_address',$data_address,['member_address_id'=>$id]);

        $data['status'] = true;

        echo json_encode($data);
    }

    public function address_add() 
    {   
        $this->form_validation->set_rules('member_address_fullname', 'fullname', 'trim|required');

        if ($this->form_validation->run()) {

            if (isset($_POST['member_address_main'])) {

                $data_address = [
                    'member_address_main' => 0,
                ];
                $this->model_db->db_update('member_address',$data_address,['member_address_member_id'=>$this->session->userdata('laun_id')]);
            }
           
            $data_address = [
                'member_address_fullname' => $this->input->post('member_address_fullname'),
                'member_address_phone_number' => $this->input->post('member_address_phone_number'),
                'member_address_province' => $this->input->post('member_address_province'),
                'member_address_amphoe' => $this->input->post('member_address_amphoe'),
                'member_address_tambon' => $this->input->post('member_address_tambon'),
                'member_address_postcode' => $this->input->post('member_address_postcode'),
                'member_address_address' => $this->input->post('member_address_address'),
                'member_address_main' => (isset($_POST['member_address_main'])) ? $_POST['member_address_main'] : 0,
                'member_address_member_id' => $this->session->userdata('laun_id'),
                'member_address_status' => 1,
                'member_address_created_by' => $this->session->userdata('laun_id'),
                'member_address_created_at' => date('Y-m-d H:i:s'),
            ];

            $this->model_db->db_insert('member_address',$data_address);

            $data['status'] = true;
               
        } else {
            $data['status'] = false;
            $data['message'] = validation_errors();
        }

        echo json_encode($data);
    }

    public function address_edit() 
    {   
        $this->form_validation->set_rules('member_address_edit_fullname', 'fullname', 'trim|required');

        if ($this->form_validation->run()) {

            if (isset($_POST['member_address_edit_main'])) {

                $data_address = [
                    'member_address_main' => 0,
                ];

                $this->model_db->db_update('member_address',$data_address,['member_address_member_id'=>$this->session->userdata('laun_id')]);
            }
           
            $data_address = [
                'member_address_fullname' => $this->input->post('member_address_edit_fullname'),
                'member_address_phone_number' => $this->input->post('member_address_edit_phone_number'),
                'member_address_province' => $this->input->post('member_address_edit_province'),
                'member_address_amphoe' => $this->input->post('member_address_edit_amphoe'),
                'member_address_tambon' => $this->input->post('member_address_edit_tambon'),
                'member_address_postcode' => $this->input->post('member_address_edit_postcode'),
                'member_address_address' => $this->input->post('member_address_edit_address'),
                'member_address_main' => (isset($_POST['member_address_edit_main'])) ? $_POST['member_address_edit_main'] : 0,
                'member_address_updated_by' => $this->session->userdata('laun_id'),
                'member_address_updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->model_db->db_update('member_address',$data_address,['member_address_id'=>$this->input->post('member_address_edit_id')]);

            $data['status'] = true;
               
        } else {
            $data['status'] = false;
            $data['message'] = validation_errors();
        }

        echo json_encode($data);
    }

    public function get_amphoe($province_id){

        $data['amphoe'] = $this->model_db->db_select('amphoe',false,[],['amphoe_province_id'=>$province_id],'amphoe_name_th ASC',[],[100,0]);

        echo json_encode(['status'=>true,'data'=>$data]);
    }

    public function get_tambon($amphoe_id){

        $data['tambon'] = $this->model_db->db_select('tambon',false,[],['tambon_amphoe_id'=>$amphoe_id],'tambon_name_th ASC',[],[100,0]);

        echo json_encode(['status'=>true,'data'=>$data]);
    }

    public function get_zipcode($tambon_id){

        $data['zipcode'] = $this->model_db->db_select('tambon',false,['tambon_zipcode'],['tambon_id'=>$tambon_id],'tambon_name_th ASC',[],[1,0])[0];

        echo json_encode(['status'=>true,'data'=>$data]);
    }

}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */