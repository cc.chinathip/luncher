<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Profile extends Front
{
	
  	public function __construct()
  	{
  		parent::__construct();
          $this->load->model('model_db');

          $this->load->library('pagination');
  	}

    public function index(){   
      $user_id = $_GET['uid'];

      $join_member = [
          [
              'member',
              'member_id',
              'project',
              'project_created_by',
              'INNER'
          ]
      ];

      $where_project = [
        'project_created_by' => $user_id,
        'project_date_start <=' => date('Y-m-d'),
        'project_date_end >=' => date('Y-m-d')
      ];

      $data['project_list'] = $this->model_db->db_select('project',true,[],$where_project,'project_date_end ASC',$join_member,[$limit,$offset]);

      $this->template->build('frontend/profile',$data);

    }
   
}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */