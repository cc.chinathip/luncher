<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Home extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');
        $this->load->model('model_auth');
	}


    public function index() 
    {   

        $join_category = [
            [
                'project_category_description',
                'project_category_description_project_category_id',
                'project_category',
                'project_category_id',
                'INNER'
            ]
        ];

        $where_category = [
          'project_category_status' => 1,
          'project_category_description_language_id' => lang_id()
        ];

        $data['project_category'] = $this->model_db->db_select('project_category',true,[],$where_category,'project_category_id ASC',$join_category);

        $data['project_category_rand'] = $this->model_db->db_select('project_category',true,[],$where_category,'rand()',$join_category,[4,0]);

        $join_member = [
            [
                'member',
                'member_id',
                'project',
                'project_created_by',
                'INNER'
            ]
        ];

          $where_project = [
            'project_date_start <=' => date('Y-m-d'),
            'project_date_end >=' => date('Y-m-d')
          ];

          $data['project_recom'] = $this->model_db->db_select('project',true,[],$where_project,'rand()',$join_member,[7,0]);

          $data['project_news'] = $this->model_db->db_select('project_news',false,[],[],'',[],[7,0]);

          // var_dump($data['project_recom']);

        $this->template->build('frontend/home',$data);
    }

}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */