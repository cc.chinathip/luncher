<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Project extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');

        $this->load->library('pagination');
	}

  public function news_detail($news_id){   

   
    $data['news_detail'] = $this->model_db->db_select('project_news',true,[],['project_news_id'=>$news_id])[0];
    $data['project_news'] = $this->model_db->db_select('project_news',false,[],[],'',[],[7,0]);

    $this->template->build('frontend/project/news-detail',$data);

  }

    function current_url()
    {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $linkParts = explode('&subcategory=', $url);
        $link = $linkParts[0];
        return $link;
    }
 
    public function list(){   
      $limit = 12;
      $data['fillter'] = [];
      if (isset($_GET)) {
        if (!empty($_GET['per_page'])) {
          $offset = $limit + $_GET['per_page'];
        }else{
          $offset = 4;
        }

        if (isset($_GET['subcategory']) && !empty($_GET['subcategory'])) {
          $data['fillter'] = explode(',', $_GET['subcategory']);
        }
      }

      $data['current_url'] = $this->current_url();



      // select project
      $join_member = [
          [
              'member',
              'member_id',
              'project',
              'project_created_by',
              'INNER'
          ]
      ];

      $where_project = [
        // 'project_status' => 1,
        'project_category_id' => $_GET['category'],
        'project_date_start <=' => date('Y-m-d'),
        'project_date_end >=' => date('Y-m-d')
      ];

      if (isset($_GET) && !empty($_GET['subcategory'])) {
          $data['project_slide'] = $this->model_db->db_select_project_fillter('project',true,[],$where_project,'project_date_end ASC',$join_member,[4,0],['project_subcategory_id',$data['fillter']]);
          $data['project_list'] = $this->model_db->db_select_project_fillter('project',true,[],$where_project,'project_date_end ASC',$join_member,[$limit,$offset],['project_subcategory_id',$data['fillter']]);
          $data['total_rows'] = $this->model_db->db_countrows_project_fillter('project',$where_project,true,$offset,['project_subcategory_id',$data['fillter']]);
      }else{
          $data['project_slide'] = $this->model_db->db_select('project',true,[],$where_project,'project_date_end ASC',$join_member,[4,0]);
          $data['project_list'] = $this->model_db->db_select('project',true,[],$where_project,'project_date_end ASC',$join_member,[$limit,$offset]);
          $data['total_rows'] = $this->model_db->db_countrows('project',$where_project,true,$offset);
      }
      

      
     
      $config = [
        'base_url'     => BASE_URL.'project-list',
        'total_rows'   => $data['total_rows'],
        'per_page'     => $limit,
        'page_query_string'  => TRUE,
        'reuse_query_string'  => TRUE,
      ];

      $this->pagination->initialize($config);
      
      $data['pagination'] = $this->pagination->create_links();

      // end select project

      // select project_category
      $join_category = [
          [
              'project_category_description',
              'project_category_description_project_category_id',
              'project_category',
              'project_category_id',
              'INNER'
          ]
      ];

      $where_category = [
        'project_category_id' => $_GET['category'],
        'project_category_status' => 1,
        'project_category_description_language_id' => lang_id()
      ];

      $data['project_category'] = $this->model_db->db_select('project_category',true,[],$where_category,'project_category_id ASC',$join_category);

      // end select project_category

      // select project_subcategory
      $join_subcategory = [
          [
              'project_subcategory_description',
              'project_subcategory_description_project_subcategory_id',
              'project_subcategory',
              'project_subcategory_id',
              'INNER'
          ]
      ];

      $where_subcategory = [
        'project_subcategory_project_category_id' => $_GET['category'],
        'project_subcategory_status' => 1,
        'project_subcategory_description_language_id' => lang_id()
      ];

      $data['project_subcategory'] = $this->model_db->db_select('project_subcategory',true,[],$where_subcategory,'project_subcategory_id ASC',$join_subcategory);
      // end select project_subcategory
      

      $this->template->build('frontend/project/project_list',$data);

    }

    public function detail($id){   

      $join_member = [
          [
              'member',
              'member_id',
              'project',
              'project_created_by',
              'INNER',
          ],
          [
              'project_category',
              'project_category_id',
              'project',
              'project_category_id',
              'INNER',
          ]
      ];

      $where_project = [
        // 'project_status' => 1,
        'project_date_start <=' => date('Y-m-d'),
        'project_date_end >=' => date('Y-m-d'),
        'project_id' => $id
      ];

      $data['project_slide'] = $this->model_db->db_select('project',true,[],
        [
          'project_date_start <=' => date('Y-m-d'),
          'project_date_end >=' => date('Y-m-d'),
          'project_id !=' => $id
        ],
      'project_date_end ASC',[[
              'member',
              'member_id',
              'project',
              'project_created_by',
              'INNER',
          ]],[6,0]);

      $data['project_detail'] = $this->model_db->db_select('project',true,[],$where_project,'project_date_end ASC',$join_member);

      if (isset($data['project_detail']) && !empty($data['project_detail'])) {
        $data['project_detail'] = $data['project_detail'][0];
      }else{
        $data['project_detail'] = [];
      }

      $join_project_comment = [
        [
          'member',
          'member_id',
          'project_comment',
          'project_comment_created_by',
          'INNER'
        ],
      ];

      $select_project_comment = [
        'project_comment.*',
        'member.member_username',
        'member.member_avatar',
      ];

      $data['project_comment'] = $this->model_db->db_select('project_comment',true,$select_project_comment,['project_comment_project_id'=>$id,'project_comment_status'=>1],'project_comment_created_at DESC',$join_project_comment,[$limit,$offset]);

      $join_project_update = [
        [
          'member',
          'member_id',
          'project_update',
          'project_update_created_by',
          'INNER'
        ],
      ];

      $select_project_update = [
        'project_update.*',
        'member.member_username',
        'member.member_avatar',
      ];

      $data['project_update'] = $this->model_db->db_select('project_update',true,$select_project_update,['project_update_project_id'=>$id],'project_update_created_at DESC',$join_project_update,[$limit,$offset]);

      $data['project_award'] = $this->model_db->db_select('project_award',true,[],['project_award_project_id'=>$id],'project_award_created_at DESC',[],['all',0]);
      

      $this->template->build('frontend/project/project_detail',$data);

    }

  
    public function comment($project_comment_project_id,$limit=10,$offset=0)
    {
      $join_member = [
        [
          'member',
          'member_id',
          'project_comment',
          'project_comment_created_by',
          'INNER'
        ],
      ];

      $select_product = [
        'project_comment.*',
        'member.member_first_name',
        'member.member_last_name',
        'member.member_avatar',
      ];

      $project_comment['project_comment'] = $this->model_db->db_select('project_comment',true,[],['project_comment_project_id'=>$project_comment_project_id],'project_comment_created_at DESC',$join_member,[$limit,$offset]);

      echo json_encode($project_comment);
    }

    public function save_comment($project_comment_project_id)
    {
      if (isset($_POST)) {
        $data_post = $_POST;
        $data_at = date('Y-m-d H:i:s');
        $data_project_comment = array(
          'project_comment_project_id' => $project_comment_project_id,
          'project_comment_detail' => $data_post['project_comment'],
          'project_comment_created_by' => $this->session->userdata('laun_id'),
          'project_comment_created_at' => $data_at
        );
      
        $data_project_comment_id = $this->model_db->db_insert('project_comment',$data_project_comment);

        $resp['status'] = true;
        $resp['data'] = [
          'project_comment_detail' => $data_post['project_comment'],
          'project_comment_created_at' => date("l, d F Y", strtotime($data_at)),
          'member_avatar' => $this->session->userdata('laun_avatar'),
          'member_username' => $this->session->userdata('laun_username')
        ];

      } else {
        $resp['status'] = false;
        $resp['message'] = validation_errors();
      }

      echo json_encode($resp);
    }

    public function index(){   

      if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
       /*  $data['menu_left'] = 'account';
        $data['account'] = $this->model_db->db_select('member',true,[],['member_id'=>$this->session->userdata('laun_id')])[0];
        $data['country'] = $this->model_db->db_select('country',false,[],[],'',[],['all',0]); */

        $data['province'] = $this->model_db->db_select('province',false,[],[],'',[],['all',0]);

        $join = [
          [
              'project_category_description',
              'project_category_description_project_category_id',
              'project_category',
              'project_category_id',
              'INNER'
          ]
      ];

      $where_category = [
        'project_category_status' => 1,
        'project_category_description_language_id' => lang_id()
      ];

      $data['project_category'] = $this->model_db->db_select('project_category',true,[],$where_category,'project_category_id ASC',$join);
      
      $this->template->build('frontend/project/project_create',$data);

    }

    public function project_edit($project_id){   

      if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
       
        $data['province'] = $this->model_db->db_select('province',false,[],[],'',[],['all',0]);
        $join = [
          [
              'project_category_description',
              'project_category_description_project_category_id',
              'project_category',
              'project_category_id',
              'INNER'
          ],
      ];

      $where_category = [
        'project_category_status' => 1,
        'project_category_description_language_id' => lang_id()
      ];

      $data['project_category'] = $this->model_db->db_select('project_category',true,[],$where_category,'project_category_id ASC',$join);



      
       $data['project_list'] = $this->model_db->db_select('project',true,[],['project_id'=>$project_id])[0];
  
      


        $this->template->build('frontend/project/project_edit',$data);
      
      //$this->template->build('frontend/project/project_create',$data);

    }

    

    public function payment_project($project_id){   
      
     if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
    

     //$data['bank'] = $this->model_db->db_select('bank',false,[],['bank_created_by'=>$this->session->userdata('laun_id')],'',[],['all',0]);

       $data['menu_left'] = 'payment';

        $join = [
            [
                'bank',
                'bank_id',
                'member_bank',
                'member_bank_bank_id',
                'INNER'
            ]
        ];
  
        $where = [
            'member_bank_status' => 1,
            'member_bank_created_by' => $this->session->userdata('laun_id'),
        ];
  
         $data['bank'] = $this->model_db->db_select('member_bank',true,[],$where,'member_bank_id ASC',$join);


        $data['project_id'] = $project_id;

        $this->template->build('frontend/project/payment_project',$data);

     //$data['project_by_id'] = $this->model_db->db_select('project',true,[],['project_id'=>$project_id])[0];


   }

    public function bank_payment_project(){ 
      
      
        //if(!empty($this->input->post('member_bank_id'))){

          $data = $this->model_db->db_select('member_bank',true,[],['member_bank_id'=>$this->input->post('member_bank_id')])[0];

          echo json_encode($data);

      
      }

   public function add_payment_project(){ 
    
 
        $data_update=[

          'project_bank_account' => $this->input->post('member_bank_bank_id'),
         
        ];

      $update = $this->model_db->db_update('project',$data_update,
                            [
                                'project_id'=>$this->input->post('project_id'),
                           
                            ]
                            );

      
      if($update){

          $data['status'] = true;
          //$data['member_bank_id'] = $insert;

      }else{

          $data['status'] = false;
      }
    
      echo json_encode($data);
     
 
     
   }

   

    public function add($pid)
  { 
    $favourite = $this->model_db->db_select('favourite',false,[],['favourite_product_id'=>$pid,'favourite_created_by'=>$this->session->userdata('mid')]);

    if (isset($favourite) && !empty($favourite)) {
      $this->model_db->db_delete('favourite',['favourite_product_id'=>$pid,'favourite_created_by'=>$this->session->userdata('mid')]);

      echo json_encode(['status' => true,'action'=>'unfavourite']);
    }else{
      $data_favourite = array(
        'favourite_product_id' => $pid,
        'favourite_created_by' => $this->session->userdata('mid'),
        'favourite_created_at' => date('Y-m-d H:i:s')
      );

      $data_favourite_id = $this->model_db->db_insert('favourite',$data_favourite);

      echo json_encode(['status' => true,'action'=>'favourite']);
    }

    
  }

    public function in_favourite($project_id){  

      $data_favourite = array(
        'favourite_project_id' => $project_id,
        'favourite_created_by' => $this->session->userdata('laun_id'),
        'favourite_created_at' => date('Y-m-d H:i:s')
      );

      $data_favourite_id = $this->model_db->db_insert('favourite',$data_favourite);

      echo json_encode(['status' => true,'action'=>'in_favourite']);

    }

    public function un_favourite($project_id){  

      $this->model_db->db_delete(
        'favourite',
        [
          'favourite_project_id'=>$project_id,
          'favourite_created_by'=>$this->session->userdata('laun_id')
        ]
      );

      echo json_encode(['status' => true,'action'=>'un_favourite']);

    }

    public function in_like($project_id){  

      $data_like = array(
        'like_project_id' => $project_id,
        'like_created_by' => $this->session->userdata('laun_id'),
        'like_created_at' => date('Y-m-d H:i:s')
      );

      $data_like_id = $this->model_db->db_insert('like',$data_like);

      echo json_encode(['status' => true,'action'=>'in_like']);

    }
    
    public function un_like($project_id){  

      $this->model_db->db_delete(
        'like',
        [
          'like_project_id'=>$project_id,
          'like_created_by'=>$this->session->userdata('laun_id')
        ]
      );

      echo json_encode(['status' => true,'action'=>'un_like']);

    }

    public function basic_info($project_id){   
      
       if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
         $data['project_id'] = $project_id;
         $data['province'] = $this->model_db->db_select('province',false,[],[],'',[],['all',0]);
         $data['project_by_id'] = $this->model_db->db_select('project',true,[],['project_id'=>$project_id])[0];
         $data['country'] = $this->model_db->db_select('country',false,[],[],'',[],['all',0]);


         $join = [
          [
              'project_category_description',
              'project_category_description_project_category_id',
              'project_category',
              'project_category_id',
              'INNER'
          ]
      ];

     
      $where_category = [
        'project_category_status' => 1,
        'project_category_description_language_id' => lang_id()
      ];

      $data['project_category'] = $this->model_db->db_select('project_category',true,[],$where_category,'project_category_id ASC',$join);


        $join_sub = [
          [
              'project_subcategory_description',
              'project_subcategory_description_project_subcategory_id',
              'project_subcategory',
              'project_subcategory_id',
              'LEFT'
          ]
        ];
        $where_subcategory = [
          'project_subcategory_status' => 1,
          'project_subcategory_description_language_id' => lang_id()
        ];

        $data['project_subcategory'] = $this->model_db->db_select('project_subcategory',true,[],$where_subcategory,'project_subcategory_id ASC',$join_sub);
         
         $this->template->build('frontend/project/basic_info',$data);
     }

    public function story($project_id){   
       if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
      $data['project_id'] = $project_id;
      $data['province'] = $this->model_db->db_select('province',false,[],[],'',[],['all',0]);
    //  $data['project_by_id'] = $this->model_db->db_select('project',true,[],['project_id'=>$project_id])[0];
      $data['country'] = $this->model_db->db_select('country',false,[],[],'',[],['all',0]);
      
      $this->template->build('frontend/project/story',$data);
    }

     public function rewards(){   
       if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
        $data['province'] = $this->model_db->db_select('province',false,[],[],'',[],['all',0]);
        $data['country'] = $this->model_db->db_select('country',false,[],[],'',[],['all',0]);
        //$data['reward_all'] = $this->model_db->db_select('project_award',true,[],['project_award_created_by'=>$this->session->userdata('laun_id')]);
        $data['users'] = $this->session->userdata('laun_id');

        $join_donate = [
           [
              'donate_award',
              'donate_award_project_award_id',
              'project_award',
              'project_award_id',
              'INNER'
          ], [
            'donate',
            'donate_id',
            'donate_award',
            'donate_award_donate_id',
            'INNER'
          ]
        ];

      $where_donate = [
        'project_award_created_by' => $this->session->userdata('laun_id'),
      ];

      $data['reward_all'] = $this->model_db->db_select('project_award',true,[],$where_donate,'project_award_id ASC',$join_donate);


      
        $this->template->build('frontend/project/rewards',$data);
    }


    public function rewards_add(){   
       
        if (!$this->session->userdata("laun_logged")) {
              redirect(BASE_URL.'auth');
        }

        $data['province'] = $this->model_db->db_select('province',false,[],[],'',[],['all',0]);
        $data['country'] = $this->model_db->db_select('country',false,[],[],'',[],['all',0]);
        $data['project_award_delivery'] = $this->model_db->db_select('project_award_delivery',false,[],[],'',[],['all',0]);
        $this->template->build('frontend/project/rewards_add',$data);

    }

    public function rewards_edit($project_award_id){   
       
      if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
      $data['province'] = $this->model_db->db_select('province',false,[],[],'',[],['all',0]);
      $data['country'] = $this->model_db->db_select('country',false,[],[],'',[],['all',0]);
      $data['reward_edit'] = $this->model_db->db_select('project_award',true,[],['project_award_id'=>$project_award_id])[0];
      $this->template->build('frontend/project/rewards_edit',$data);
  }


    public function add_rewards_project(){   
    
        $data_add=[

           'project_award_name' => $this->input->post('project_award_name'),
           'project_award_minimum_amount' => $this->input->post('project_award_minimum_amount'),
           'project_award_description' => $this->input->post('project_award_description'),
           'project_award_month' => $this->input->post('project_award_month'),
           'project_award_year' => $this->input->post('project_award_year'),
           'project_award_delivery' => $this->input->post('project_award_delivery'),
           'project_award_amount_assign' => $this->input->post('project_award_amount_assign'),
           'project_award_amount' => $this->input->post('project_award_amount'),
           'project_award_created_by' => $this->session->userdata('laun_id'),
           /* 'project_award_project_id' => $this->input->post('project_award_project_id'), */

        ];

        $insert = $this->model_db->db_insert('project_award',$data_add);

        if($insert){

           $data['status'] = true;
           $data['project_award_id'] = $insert;

        }else{

           $data['status'] = false;
        }
     
        echo json_encode($data);

    }
    

    public function update_rewards_project(){   
    
      $data_update=[

         'project_award_name' => $this->input->post('project_award_name'),
         'project_award_minimum_amount' => $this->input->post('project_award_minimum_amount'),
         'project_award_description' => $this->input->post('project_award_description'),
         'project_award_month' => $this->input->post('project_award_month'),
         'project_award_year' => $this->input->post('project_award_year'),
         'project_award_delivery' => $this->input->post('project_award_delivery'),
         'project_award_amount_assign' => $this->input->post('project_award_amount_assign'),
         'project_award_amount' => $this->input->post('project_award_amount'),
         'project_award_updated_by' => $this->session->userdata('laun_id'),
         'project_award_updated_at' => date('Y-m-d H:i:s'),
        

      ];

      

      $data_image_save = $this->model_db->db_update('project_award',$data_update,
                            [
                                'project_award_id'=>$this->input->post('project_award_id'),
                           
                            ]
                            );

      if($data_image_save){

         $data['status'] = true;
        // $data['project_award_id'] = $insert;

      }else{

         $data['status'] = false;
      }
   
      echo json_encode($data);

  }


    public function update_story(){  

        $data_update=[
          
            'project_updated_at' => date('Y-m-d H:i:s'),
            'project_updated_by' => $this->session->userdata('laun_id'),
            'project_detail' => $this->input->post('project_detail'),
          
        ];

        $update = $this->model_db->db_update('project',$data_update,['project_id'=>$this->input->post('project_id')]);
        if($update){

            $data['status'] = true;

        }else{

            $data['status'] = false;
        }

        echo json_encode($update);

    }

     public function update_basic_info(){   

        $date_start = str_replace('/', '-', $this->input->post('project_date_start'));
        $date_end = str_replace('/', '-', $this->input->post('project_date_end'));
       

        $data_update=[
           'project_name' => $this->input->post('project_name'),
           'project_description' => $this->input->post('project_description'),
           'project_location' => $this->input->post('project_location'),
           'project_country' => $this->input->post('country'),
           'project_target' => $this->input->post('project_target'),
           'project_date_start' => date('Y-m-d', strtotime($date_start)),
           'project_date_end' => date('Y-m-d', strtotime($date_end)),
           'project_updated_at' => date('Y-m-d H:i:s'),
           'project_updated_by' => $this->session->userdata('laun_id'),
           'project_category_id' => $this->input->post('project_category_id'),
           'project_subcategory_id' => $this->input->post('project_subcategory_id'),
           'project_country' => $this->input->post('country'),
        ];


        $update = $this->model_db->db_update('project',$data_update,['project_id'=>$this->input->post('project_id')]);

        $path_upload = 'uploads/project/';
       /*  if (!is_dir($path_upload)) {
        mkdir($path_upload, 0777, TRUE);
        } */
            $config['upload_path'] = $path_upload; 
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '1000000';
            $this->load->library('upload',$config); 
                if(!empty($_FILES['upload_image']['name'])){
            
                        $_FILES['file']['name'] = $_FILES['upload_image']['name'];
                        $_FILES['file']['type'] = $_FILES['upload_image']['type'];
                        $_FILES['file']['tmp_name'] = $_FILES['upload_image']['tmp_name'];
                        $_FILES['file']['error'] = $_FILES['upload_image']['error'];
                        $_FILES['file']['size'] = $_FILES['upload_image']['size'];
                
                        $config['file_name'] = md5($_FILES['upload_image']['name']);
                        $this->upload->initialize($config);

                        if($this->upload->do_upload('upload_image')){
                            $upload_data = $this->upload->data();
                            $filename = $upload_data['file_name'];
                            $data_image = [
                                'project_image' => $filename
                            ];


                            
                            $data_image_save = $this->model_db->db_update('project',$data_image,
                            [
                                'project_id'=>$this->input->post('project_id'),
                           
                            ]
                            );

                    $data['status_upload'] = true;
                   // echo json_encode($resp);
                        }
                 }

        if($update){

           $data['status'] = true;

        }else{

           $data['status'] = false;
        }
     
        echo json_encode($update);

    }

   

    public function add_project(){   
    
         $data_add=[
            'project_category_id' => $this->input->post('project_category_id'),
            'project_description' => $this->input->post('project_description'),
            'project_location' => $this->input->post('project_location'),
            'project_province' => $this->input->post('project_province'),
            'project_created_by' => $this->session->userdata('laun_id')
         ];

         $insert = $this->model_db->db_insert('project',$data_add);
         

         if($insert){

            $data['status'] = true;
            $data['project_id'] = $insert;

         }else{

            $data['status'] = false;
         }
      
         echo json_encode($data);

     }

     public function edit_project(){   
    
      $data_update=[
         'project_category_id' => $this->input->post('project_category_id'),
         'project_description' => $this->input->post('project_description'),
         'project_location' => $this->input->post('project_location'),
         'project_province' => $this->input->post('project_province'),
         'project_updated_by' => $this->session->userdata('laun_id'),
         'project_updated_at' => date('Y-m-d H:i:s'),
      ];


      $update = $this->model_db->db_update('project',$data_update,['project_id'=>$this->input->post('project_id')]);

      if($update){

         $data['status'] = true;
        
      }else{

         $data['status'] = false;
      }
   
      echo json_encode($data);

  }

    public function delete_project(){   

        $data_update=[
          'project_deleted' => 1,
        
      ];

      $update = $this->model_db->db_update('project',$data_update,['project_id'=>$this->input->post('project_id')]);

      if($update){

          $data['status'] = true;
        
      }else{

          $data['status'] = false;
      }
      

      echo json_encode($data);

    
  }



public function delete_reward(){   

  $data_update=[
    'project_award_deleted' => 1,
  
];
 
  $delete = $this->model_db->db_update('project_award',$data_update,['project_award_id'=>$this->input->post('project_reward_id')]);
  if($delete){

      $data['status'] = true;
    
  }else{

      $data['status'] = false;
  }

  echo json_encode($data);


}
}

/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */