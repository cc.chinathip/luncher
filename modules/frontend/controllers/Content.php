<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Content extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');
	}


    public function terms() 
    {   
         $join_content = [
            [
                'content_description',
                'content_description_content_id',
                'content',
                'content_id',
                'INNER'
            ]
        ];

        $where_content = [
            'content_id' => 11,
            'content_status' => 1,
            'content_description_language_id' => lang_id()
        ];

        $data['content'] = $this->model_db->db_select('content',true,[],$where_content,'content_id ASC',$join_content);

        $this->template->build('frontend/content/terms',$data);
    }

    public function guide() 
    {   
         $join_content = [
            [
                'content_description',
                'content_description_content_id',
                'content',
                'content_id',
                'INNER'
            ]
        ];

        $where_content = [
            'content_id' => 12,
            'content_status' => 1,
            'content_description_language_id' => lang_id()
        ];

        $data['content'] = $this->model_db->db_select('content',true,[],$where_content,'content_id ASC',$join_content);

        $this->template->build('frontend/content/guide',$data);
    }

    public function trust() 
    {   
         $join_content = [
            [
                'content_description',
                'content_description_content_id',
                'content',
                'content_id',
                'INNER'
            ]
        ];

        $where_content = [
            'content_id' => 13,
            'content_status' => 1,
            'content_description_language_id' => lang_id()
        ];

        $data['content'] = $this->model_db->db_select('content',true,[],$where_content,'content_id ASC',$join_content);

        $this->template->build('frontend/content/trust',$data);
    }

    public function policy() 
    {   
         $join_content = [
            [
                'content_description',
                'content_description_content_id',
                'content',
                'content_id',
                'INNER'
            ]
        ];

        $where_content = [
            'content_id' => 14,
            'content_status' => 1,
            'content_description_language_id' => lang_id()
        ];

        $data['content'] = $this->model_db->db_select('content',true,[],$where_content,'content_id ASC',$join_content);

        $this->template->build('frontend/content/policy',$data);
    }

    public function prohibited() 
    {   
         $join_content = [
            [
                'content_description',
                'content_description_content_id',
                'content',
                'content_id',
                'INNER'
            ]
        ];

        $where_content = [
            'content_id' => 15,
            'content_status' => 1,
            'content_description_language_id' => lang_id()
        ];

        $data['content'] = $this->model_db->db_select('content',true,[],$where_content,'content_id ASC',$join_content);

        $this->template->build('frontend/content/prohibited',$data);
    }

}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */