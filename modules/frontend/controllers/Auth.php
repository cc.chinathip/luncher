<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Auth extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');
        $this->load->model('model_auth');
	}


    public function index() 
    {   
        // var_dump($this->session->userdata('laun_logged'));
        $this->template->build('auth');
    }

    public function login_confirm() 
    {
        $response = [];

        $this->form_validation->set_rules('member_username', 'Please enter username', 'required');
        $this->form_validation->set_rules('member_password', 'Please enter password', 'required');

        if($this->form_validation->run()){

            $data_auth = $this->model_auth->login($this->input->post('member_username'),$this->input->post('member_password'));


            if ($data_auth == 'member_not_found') {

                $response['status'] = 'member_not_found';
                $response['message'] = 'This username cannot be found in the system.';

            }elseif($data_auth == 'password_not_match'){

                $response['status'] = 'password_not_match';
                $response['message'] = 'Your password is incorrect.';

            }else{

                
                $save_data_login = [
                    'member_last_login' => date('Y-m-d H:i:s'),
                ]; // data update

                $this->db->where('member_id',$data_auth->member_id);
                $this->db->update('member',$save_data_login);
        
                $member_data = array(
                    'laun_id'     => $data_auth->member_id,
                    'laun_username'     => $data_auth->member_username,
                    'laun_email'     => $data_auth->member_email,
                    'laun_avatar'     => BASE_URL.'uploads/member/'.$data_auth->member_avatar,
                    'laun_lastlogin'     => $data_auth->member_last_login,
                    'laun_date_regis'     => $data_auth->member_created_at,
                    'laun_logged'     => true,
                );

                $this->session->set_userdata($member_data);
              
                $response['status'] = 'successful_access';
                $response['message'] = 'Login successful';
            }
            
            echo json_encode($response);
        }else{

            $response['status'] = 'not_form_validation';
            $response['message'] = 'An error has occurred';
            echo json_encode($response);
        }
    }
 
    public function register_confirm() 
    {
        $this->form_validation->set_rules('member_username', 'username', 'trim|required');
        $this->form_validation->set_rules('member_email', 'email', 'trim|required');
        $this->form_validation->set_rules('member_password_register', 'password', 'trim|required');

        if ($this->form_validation->run()) {

            $data_auth = $this->model_auth->check_email_regis($this->input->post('member_email'));
            if ($data_auth) {
                $this->data['status'] = false;
                $this->data['message'] = 'This email is already used.';
            }else{
                $data_username_auth = $this->model_auth->check_username_regis($this->input->post('member_username'));
                if ($data_username_auth) {
                    $this->data['status'] = false;
                    $this->data['message'] = 'This username is already used.';
                }else{
                    $save_data = [
                        'member_username' => $this->input->post('member_username'),
                        'member_email' => $this->input->post('member_email'),
                        'member_password' => password_hash($this->input->post('member_password_register'), PASSWORD_DEFAULT),
                        'member_avatar' => 'member.png',
                        'member_status' => 1,
                        'member_created_by' => 0,
                        'member_created_at' => date('Y-m-d H:i:s'),
                    ];

                    $this->db->insert('member', $save_data);
                    $insert_id = $this->db->insert_id();

                    $this->data['status'] = true;
                }
               
            }
        } else {
            $this->data['status'] = false;
            $this->data['message'] = validation_errors();
        }

        echo json_encode($this->data);
    }

    public function logout()
    {
        if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
     
        $userdata = array(
            'laun_id',
            'laun_username',
            'laun_email',
            'laun_avatar',
            'laun_lastlogin',
            'laun_logged',
            'laun_date_regis'
        );

        $this->session->unset_userdata($userdata);

        redirect(BASE_URL);
    }
    

}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */