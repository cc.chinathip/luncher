<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Search extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');

        $this->load->library('pagination');
	}

 	
 	public function current_url()
    {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $linkParts = explode('?text_search=', $url);
        $link = $linkParts[0];

        $linkParts = explode('&location=', $link);
        $link = $linkParts[0];

        $linkParts = explode('&category=', $link);
        $link = $linkParts[0];

        $linkParts = explode('&subcategory=', $link);
        $link = $linkParts[0];

        return $link;
    }

    public function index(){   
     	
     	$limit = 12;
      $data['fillter'] = [];
      $data['text_search'] = [];
      $data['location'] = [];
      if (isset($_GET)) {
        if (!empty($_GET['per_page'])) {
          $offset = $_GET['per_page'];
        }else{
          $offset = 0;
        }

        if (isset($_GET['subcategory']) && !empty($_GET['subcategory'])) {
          $data['fillter'] = explode(',', $_GET['subcategory']);
        }


        if (isset($_GET['text_search']) && !empty($_GET['text_search'])) {
          $data['text_search'] = [
          	$_GET['text_search'],
          	['project_name','project_description','project_detail']
          ];
        }

        
        if (isset($_GET['location']) && !empty($_GET['location'])) {
          $data['location'] = [
          	'project_province'=>$_GET['location'],
          ];
        }
      }

      $data['current_url'] = $this->current_url();

      // select project
      $join_member = [
          [
              'member',
              'member_id',
              'project',
              'project_created_by',
              'INNER'
          ]
      ];


      	$where_project = [
	        'project_date_start <=' => date('Y-m-d'),
	        'project_date_end >=' => date('Y-m-d')
	    ];

      if (!empty($data['location'])) {
      	$where_project['project_province'] = $_GET['location'];
      }

      if (!empty($data['category'])) {
      	$where_project['project_category_id'] = $_GET['category'];
      }


      if (isset($_GET) && !empty($_GET)) {
          $data['project_list'] = $this->model_db->db_select_project_fillter('project',true,[],$where_project,'project_date_end ASC',$join_member,[$limit,$offset],['project_subcategory_id',$data['fillter']],$data['text_search']);
          $data['total_rows'] = $this->model_db->db_countrows_project_fillter('project',$where_project,true,$offset,['project_subcategory_id',$data['fillter']],$data['text_search']);
      }else{
          $data['project_list'] = $this->model_db->db_select('project',true,[],$where_project,'project_date_end ASC',$join_member,[$limit,$offset]);
          $data['total_rows'] = $this->model_db->db_countrows('project',$where_project,true,$offset);
      }
      
     
      $config = [
        'base_url'     => BASE_URL.'search',
        'total_rows'   => $data['total_rows'],
        'per_page'     => $limit,
        'page_query_string'  => TRUE,
        'reuse_query_string'  => TRUE,
      ];

      $this->pagination->initialize($config);
      
      $data['pagination'] = $this->pagination->create_links();

      // end select project
      $data['province'] = $this->model_db->db_select('province',false,[],[],'province_name_th ASC',[],['all','all']);
      
      $this->template->build('frontend/search',$data);

    }

   
}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */