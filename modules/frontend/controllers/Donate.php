<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Donate extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_db');

        $this->load->library('pagination');

        if (!$this->session->userdata("laun_logged")) {
            redirect(BASE_URL.'auth');
        }
	}

    public function project(){   
      
        if (isset($_GET['reward']) && !empty($_GET['reward'])) {

          $data_add=[
            'donate_no' => 'DN'.$_GET['project_id'].$this->session->userdata('laun_id').time(),
            'donate_project_id' => $_GET['project_id'],
            'donate_amount' => $_GET['amount'],
            'donate_total' => $_GET['amount'] + (($_GET['amount'] * 3) / 100),
            'donate_award' => $_GET['reward'],
            'donate_status' => 2,
            'donate_created_by' => $this->session->userdata('laun_id'),
            'donate_created_at' => date('Y-m-d H:i:s')
          ];

          $insert = $this->model_db->db_insert('donate',$data_add);

          $project_award = $this->model_db->db_select('project_award',false,[],['project_award_id'=>$_GET['reward']],'',[],[1,0])[0];

          $data_award=[
            'donate_award_project_id' => $project_award->project_award_project_id,
            'donate_award_project_award_id' => $_GET['reward'],
            'donate_award_donate_id' => $insert,
            'donate_award_name' => $project_award->project_award_name,
            'donate_award_minimum_amount' => $project_award->project_award_minimum_amount,
            'donate_award_description' => $project_award->project_award_description,
            'donate_award_month' => $project_award->project_award_month,
            'donate_award_year' => $project_award->project_award_year, 
            'donate_award_delivery' => $project_award->project_award_delivery, 
            'donate_award_amount' => $project_award->project_award_amount, 
            'donate_award_amount_assign' => $project_award->project_award_amount_assign, 
            'donate_award_status' => 1,
            'donate_award_created_by' => $this->session->userdata('laun_id'),
            'donate_award_created_at' => date('Y-m-d H:i:s')
          ];

          $this->model_db->db_insert('donate_award',$data_award);

          
        }else{

          $data_add=[
            'donate_no' => 'DN'.$_GET['project_id'].$this->session->userdata('laun_id').time(),
            'donate_project_id' => $_GET['project_id'],
            'donate_amount' => $_GET['amount'],
            'donate_total' => $_GET['amount'] + (($_GET['amount'] * 3) / 100),
            'donate_award' => 0,
            'donate_status' => 2,
            'donate_created_by' => $this->session->userdata('laun_id'),
            'donate_created_at' => date('Y-m-d H:i:s')
          ];

          $insert = $this->model_db->db_insert('donate',$data_add);

        }
         

         if($insert){
            $data['status'] = true;
            $data['data'] = [
              'donate_id'=>$insert,
              'project_id'=>$_GET['project_id']
            ];

         }else{

            $data['status'] = false;
         }
      
         echo json_encode($data);

     }
    public function payment(){   
    
      $data['donate'] = $this->model_db->db_select('donate',false,[],['donate_id'=>$_GET['donate_id']],'',[],[1,0]);
      $data['project'] = $this->model_db->db_select('project',true,[],['project_id'=>$_GET['project_id']],'project_id ASC');

      $this->template->build('frontend/donate/payment',$data);

    } 

   
}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */