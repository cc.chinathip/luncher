<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Upload extends Front
{
	
  	public function __construct()
  	{
  		  parent::__construct();
          $this->load->model('model_db');
          $this->load->library('pagination');
  	}

    public function image(){   
     	  $path_upload = 'uploads/editor/';

        if (!is_dir($path_upload)) {
            mkdir($path_upload, 0777, TRUE);
        }

        $config['upload_path'] = $path_upload; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '1000000';

        $this->load->library('upload',$config); 

        if(!empty($_FILES['image']['name'])){
            
            $_FILES['file']['name'] = $_FILES['image']['name'];
            $_FILES['file']['type'] = $_FILES['image']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['image']['error'];
            $_FILES['file']['size'] = $_FILES['image']['size'];
  
            $config['file_name'] = md5(time());

            $this->upload->initialize($config);

            if($this->upload->do_upload('file')){
                $upload_data = $this->upload->data();
                $filename = $upload_data['file_name'];

                $resp['status'] = true;
                $resp['url'] = BASE_URL.$path_upload.$filename;
                echo json_encode($resp);

            }

        }
     	
    }

   
}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */