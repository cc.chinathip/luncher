<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
	
	<div class="section gray-bg pb-4 has-space-top">
    	<div class="container">
			<ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="<?= BASE_URL ?>">หน้าแรก</a></li> 
			    <li class="breadcrumb-item active">ค้นหา <?= (isset($_GET['text_search']) && !empty($_GET['text_search'])) ? $_GET['text_search'] : 'All'?></li>
			</ol>
    	</div>
    </div>
 	<div class="section section-column in-project">
		<div class="container">
			
			<div class="sidebar">
				<div class="project-filter search">
					<div class="hgroup pb-0"> 
						<h3 class="regular" data-toggle="collapse" data-target="#accordion" aria-expanded="false">
							หมวดหมู่ <span class="icon"></span>
						</h3>
					</div>

					<div id="accordion" class="category-sidebar collapse">
						<?php $menu_category = menu_category(); ?>
						<?php foreach ($menu_category as $vm) { 
			        		$menu_subcategory = menu_subcategory($vm->project_category_id);
			        	?>
			        	<?php if (isset($menu_subcategory) && !empty($menu_subcategory)) { ?>
			        		<div class="category-group">
								<h4 data-toggle="collapse" data-target="#collapse_<?= $vm->project_category_id ?>" aria-expanded="true"><?= $vm->project_category_description_name ?></h4>
								<div id="collapse_<?= $vm->project_category_id ?>" class="collapse" data-parent="#accordion">
									<ul class="filter-list">
										<?php foreach ($menu_subcategory as $vs) { ?>
										<li>
											<div class="checkbox-group">
							                    <input id="subcategory_<?= $vs->project_subcategory_id ?>" value="<?= $vs->project_subcategory_id ?>" name="subcategory" type="checkbox" <?= (!empty($fillter) && in_array($vs->project_subcategory_id, $fillter)) ? 'checked' : '' ?>> 
							                    <label for="subcategory_<?= $vs->project_subcategory_id ?>"><?= $vs->project_subcategory_description_name ?></label>
							                </div>
								        </li>
								        <?php } ?>
									</ul>
								</div><!--collapse-->
							</div><!--category-group-->
			        	<?php }else{ ?>
			        		<div class="category-group">
								<h4 data-toggle="collapse" data-target="#collapse_<?= $vm->project_category_id ?>" aria-expanded="false"><?= $vm->project_category_description_name ?></h4>
								<div id="collapse_<?= $vm->project_category_id ?>" class="collapse" data-parent="#accordion">
									<p>ไม่มีหมวดหมู่ย่อย</p>
								</div><!--collapse-->
							</div><!--category-group-->
		        		<?php } ?>	
		        		<?php } ?>
						
					</div><!--category-sidebar-->

					<div class="hgroup has-line"> 
						<h3 class="regular">พื้นที่</h3>

						<select class="form-control" name="location" id="location">
							<option value="">กรุณาเลือก</option>
							<?php foreach ($province as $key => $v) { ?>
								<option value="<?= $v->province_id ?>" <?= ($_GET['location'] == $v->province_id) ? 'selected' : '' ?>><?= $v->province_name_th ?></option>
							<?php } ?>
						</select>
					</div>

					<button class="btn btn-full" id="fillter" data-url="<?= $current_url ?>" data-text_search="<?= $_GET['text_search'] ?>" data-category="<?= $_GET['category'] ?>" type="button" style="margin-top: 15px;">Filter</button>
					
				</div><!--project-filter--> 
			</div><!--sidebar-->

			<div class="content">  
 				<div class="project-list">
 					<div class="row">
 						<?php if (isset($project_list) && !empty($project_list)) { ?>
	 						<?php foreach ($project_list as $key => $pl) { ?>
	 						<div class="col-lg-4 col-sm-6">
	 							<div class="card card-project normal wow fadeIn">
									<div class="card-photo">
										<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $pl->project_image ?>);">
											<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
										</a>

										<div class="card-share">
											<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($pl->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $pl->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
											<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($pl->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $pl->project_id ?>" data-toggle="tooltip" title="Like"></div>
											<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>"></div>
										</div><!--card-share-->
									</div><!--card-photo-->

									<div class="card-body d-flex">
										<div class="col-left">
											<a class="avatar" href="<?= BASE_URL ?>profile?uid=<?= $pl->member_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $pl->member_avatar ?>);"></a>
										</div>
										<div class="col-right">
											<h3><a href="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>"><?= $pl->project_name ?></a></h3>
												<div class="post-info">
												<div class="author">by <a href="<?= BASE_URL ?>profile?uid=<?= $pl->member_id ?>"><?= $pl->member_username ?></a></div>
												<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($pl->project_date_end) ?> days left</div>
											</div><!--post-info-->
										</div><!--col-right-->
									</div><!--card-body-->

									<div class="card-footer">
										<div class="progress-box dark">
											<?php $donate_total = donate_total($pl->project_id,$pl->project_target); ?>
											<div class="progress">
					                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
											</div>

											<div class="progress-label">
												<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($pl->project_target) ?></div>
												<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= number_format($donate_total['per_cen'],2); ?>%</div>
											</div>
										</div><!--progress-box-->
									</div><!--card-footer-->
								</div><!--card-project-->
	 						</div><!--col-lg-4 col-sm-6-->
	 						<?php } ?>
	 					<?php }else{ ?>
	 						<div class="col-md-12 wow fadeInUp text-center">
								<div class="card-empty">
									<h5>ไม่พบข้อมูล</h5>
								</div>
								
							</div><!--col-md-4--> 
	 					<?php } ?>

 					</div><!--row-->

 					<div class="pagination-center">
	 					<?= $pagination; ?>
					</div>
 				</div><!--project-list-->
			</div><!--content-->
		</div><!--container-->
	</div><!--section-column-->
 
	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->
</div>


<!--====================== Modal ====================-->

<div class="modal modal-search fade" id="searchModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<button class="btn btn-close" data-dismiss="modal"></button>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="input-block search">
    					<input type="text" name="" class="form-control" placeholder="ค้นหาโปรเจคที่คุณสนใจ...">
    					<span class="icons icon-search"></span>
    				</div>
    			</div><!--col-md-12-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">หมวดหมู่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">พื้นที่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    		</div><!--row-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->


<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
 	
<script type="text/javascript">
	var swiperhighlight = new Swiper('.swiper-pj-highlight', {
    	slidesPerView:2,
	    spaceBetween:30,
	    slidesPerGroup: 2,
	    speed: 800,
	    loop: false,
	    observer: true,
	    effect: 'slide',
	    observeParents: true,
	    pagination: {
	        el: '.swiper-pagination.highlight',
	        clickable: true,
	    }, 
	    breakpoints: {
		    576: {
		        slidesPerView: 1, 
		        slidesPerGroup: 1,
	          	spaceBetween: 10, 
		    }  
	    }
    });

	$('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 

  	$('#fillter').on('click', function() { 
  		var c_url = $(this).data('url')
  		var text_search = $(this).data('text_search')
  		var category = $(this).data('category')
        var array = []; 
        var array_location = []; 
        $("input:checkbox[name=subcategory]:checked").each(function() { 
            array.push($(this).val()); 
        }); 

        array_location.push($('#location').val()); 

        window.location.href = c_url+'?text_search='+text_search+'&location='+array_location+'&category='+category+'&subcategory='+array;
    }); 
</script> 
</body>
</html>