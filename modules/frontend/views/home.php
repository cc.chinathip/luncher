<!DOCTYPE html>
<html lang="th">

<head> 
<?= $this->template->build('frontend/header'); ?>
<style type="text/css">
	.card-empty{
		padding: 15%;
	}
	.card-news .card-body {
   	 white-space: nowrap;
	}
</style>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 
<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
	<div class="section section-banner">
		<div class="swiper-container swiper-banner">
			<div class="swiper-wrapper">
	        	<div class="swiper-slide"> 
	        		<div class="background" style="background-image: url(<?= BASE_URL ?>asset/launcher/img/thumb/photo-1920x725--1.jpg);"></div>
				</div>
				<div class="swiper-slide"> 
	        		<div class="background" style="background-image: url(<?= BASE_URL ?>asset/launcher/img/thumb/photo-1920x725--1.jpg);"></div>
	            </div>
				<div class="swiper-slide"> 
	        		<div class="background" style="background-image: url(<?= BASE_URL ?>asset/launcher/img/thumb/photo-1920x725--1.jpg);"></div>
				</div>
			</div>
		</div><!--swiper-banner-->

		<div class="swiper-pagination banner"></div> 

		<div class="search-general-container align-items-center">
			<div class="inner">
				<h2 class="title-xl wow fadeInUp">Lorem ipsum dolor sit amet</h2>
				<p class="wow fadeInUp" data-wow-delay="0.2s">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eros, nibh nibh nisl ridiculus non praesent cursus viverra. Molestie eros, id nam in in eros, a. Urna, cras urna, sit cras tempor. Faucibus egestas pulvinar senectus ut arcu.</p>
				<form action="<?= BASE_URL ?>search" method="get">
				<div class="search-general wow fadeInUp" data-wow-delay="0.4s">
					
						<button class="btn btn-light btn-search" type="submit"></button>
						<input type="text" class="form-control" name="text_search" placeholder="ค้นหาโปรเจคที่คุณสนใจ..."> 
					
				</div>
				</form>
			</div><!--inner-->
		</div><!--search-general-container-->
	</div><!--section-banner-->

	<!--============[End] section 'Banner' ==============-->
	<?php if (isset($project_recom) && !empty($project_recom)) { ?>
	<div class="section section-pj-recommend pt-0 gray-bg">
		<div class="container">
			<div class="section-title">
				<h2 class="title-lg wow fadeIn">โครงการแนะนำ</h2>
			</div>
			<div class="row space-10">
				<div class="col-lg-6">
					<div class="highlight-box">
						<div class="card card-project normal wow fadeIn">
							<div class="card-photo">
								<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $project_recom[0]->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $project_recom[0]->project_image ?>);">
									<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
								</a>

								<div class="card-share">
									<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($project_recom[0]->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_recom[0]->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
									<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($project_recom[0]->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_recom[0]->project_id ?>" data-toggle="tooltip" title="Like"></div>
									<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $project_recom[0]->project_id ?>">
									</div> 
								</div><!--card-share-->
							</div><!--card-photo-->

							<div class="card-body d-flex">
								<div class="col-left">
									<a class="avatar" href="<?= BASE_URL ?>profile?uid=<?= $project_recom[0]->member_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $project_recom[0]->member_avatar ?>);"></a>
								</div>
								<div class="col-right">
									<h3><a href="<?= BASE_URL ?>project-detail/<?= $project_recom[0]->project_id ?>"><?= $project_recom[0]->project_name ?></a></h3>
		 							<div class="post-info">
										<div class="author">by <a href="<?= BASE_URL ?>profile?uid=<?= $project_recom[0]->member_id ?>"><?= $project_recom[0]->member_username ?></a></div>
										<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($project_recom[0]->project_date_end) ?> days left</div>
									</div><!--post-info-->
								</div><!--col-right-->
							</div><!--card-body-->

							<div class="card-footer">
								<?php $donate_total = donate_total($project_recom[0]->project_id,$project_recom[0]->project_target); ?>
								<div class="progress-box dark">
									<div class="progress">
			                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
									</div>

									<div class="progress-label">
										<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($project_recom[0]->project_target) ?></div>
										<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= number_format($donate_total['per_cen'],2); ?>%</div>
									</div>
								</div><!--progress-box-->
							</div><!--card-footer-->

						</div><!--card-project-->
					</div><!--highlight-box-->
				</div><!--col-lg-6-->
				<div class="col-lg-6">
					<div class="swiper-container swiper-project wow fadeIn">
						<div class="swiper-wrapper">
				        	<div class="swiper-slide"> 

				        		<div class="card-group">
				        			<?php for ( $i = 1 ; $i < (count($project_recom)-3) ; $i++ ) { ?>
				        				<div class="card card-project lg no-bg list">
											<div class="card-photo">
												<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $project_recom[$i]->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $project_recom[$i]->project_image ?>);">
													<img src="img/thumb/photo-100x75--blank.png" alt="">
												</a>

												<div class="card-share">
													<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($project_recom[$i]->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_recom[$i]->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
													<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($project_recom[$i]->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_recom[$i]->project_id ?>" data-toggle="tooltip" title="Like"></div>
													<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $project_recom[$i]->project_id ?>"></div> 
												</div><!--card-share-->
											</div><!--card-photo-->

											<div class="card-body d-flex">
												<div class="col-left">
													<a class="avatar" href="<?= BASE_URL ?>profile?uid=<?= $project_recom[$i]->member_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $project_recom[$i]->member_avatar ?>);"></a>
												</div>
												<div class="col-right">
													<h3><a href="<?= BASE_URL ?>project-detail/<?= $project_recom[$i]->project_id ?>"><?= $project_recom[$i]->project_name ?></a></h3>
						 							<div class="post-info">
														<div class="author">by <a href="<?= BASE_URL ?>profile?uid=<?= $project_recom[$i]->member_id ?>"><?= $project_recom[$i]->member_username ?></a></div>
														<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($project_recom[$i]->project_date_end) ?> days left</div>
													</div><!--post-info-->
												</div><!--col-right-->
											</div><!--card-body-->

											<div class="card-footer">
												<div class="progress-box dark">
													<?php $donate_total = donate_total($project_recom[$i]->project_id,$project_recom[$i]->project_target); ?>
													<div class="progress">
							                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
													</div>

													<div class="progress-label">
														<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($project_recom[$i]->project_target) ?></div>
														<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= number_format($donate_total['per_cen'],2); ?>%</div>
													</div>
												</div><!--progress-box-->
											</div><!--card-footer-->
										</div><!--card-project-->
				        			<?php } ?>
								</div><!--card-group-->
							</div><!--swiper-slide-->

							<div class="swiper-slide"> 
								<div class="card-group">
					        		<?php for ( $i = 4 ; $i < count($project_recom) ; $i++ ) { ?>
				        				<div class="card card-project lg no-bg list">
											<div class="card-photo">
												<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $project_recom[$i]->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $project_recom[$i]->project_image ?>);">
													<img src="img/thumb/photo-100x75--blank.png" alt="">
												</a>

												<div class="card-share">
													<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($project_recom[$i]->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_recom[$i]->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
													<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($project_recom[$i]->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_recom[$i]->project_id ?>" data-toggle="tooltip" title="Like"></div>
													<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $project_recom[$i]->project_id ?>"></div> 
												</div><!--card-share-->
											</div><!--card-photo-->

											<div class="card-body d-flex">
												<div class="col-left">
													<a class="avatar" href="<?= BASE_URL ?>profile?uid=<?= $project_recom[$i]->member_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $project_recom[$i]->member_avatar ?>);"></a>
												</div>
												<div class="col-right">
													<h3><a href="<?= BASE_URL ?>project-detail/<?= $project_recom[$i]->project_id ?>"><?= $project_recom[$i]->project_name ?></a></h3>
						 							<div class="post-info">
														<div class="author">by <a href="<?= BASE_URL ?>profile?uid=<?= $project_recom[$i]->member_id ?>"><?= $project_recom[$i]->member_username ?></a></div>
														<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($project_recom[$i]->project_date_end) ?> days left</div>
													</div><!--post-info-->
												</div><!--col-right-->
											</div><!--card-body-->

											<div class="card-footer">
												<div class="progress-box dark">
													<?php $donate_total = donate_total($project_recom[$i]->project_id,$project_recom[$i]->project_target); ?>
													<div class="progress">
							                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
													</div>

													<div class="progress-label">
														<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($project_recom[$i]->project_target) ?></div>
														<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= number_format($donate_total['per_cen'],2); ?>%</div>
													</div>
												</div><!--progress-box-->
											</div><!--card-footer-->
										</div><!--card-project-->
				        			<?php } ?>
								</div><!--card-group-->
							</div><!--swiper-slide-->
						</div><!--swiper-wrapper-->
					</div><!--swiper-project-->

					<div class="swiper-pagination project static"></div> 
				</div><!--col-lg-6-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-pj-recommend-->
	<?php } ?>

	<!--============[End] section 'โครงการแนะนำ' ==============-->
	 
	<div class="navbar-category">
		<div class="container">
			<div class="section-title wow fadeIn"><h2 class="title-lg">หมวดหมู่ยอดนิยม</h2></div>
			<div class="row space-10">
				<?php foreach ($project_category_rand as $key => $vcr) { ?>
					<div class="col-6 col-md-3 wow fadeIn">
						<a class="card card-category" href="<?= BASE_URL ?>project-list?category=<?= $vcr->project_category_id ?>">
							<div class="inner">
								<div class="card-icon"><img class="svg-js" src="<?= BASE_URL ?>uploads/project_category/<?= $vcr->project_category_image ?>" alt=""></div>
								<h4><?= nl2br($vcr->project_category_description_name_nl) ?></h4>
							</div>
						</a><!--card-category-->
					</div><!--col-md-3-->
				<?php } ?>
			</div><!--row-->
		</div><!--container-->
	</div><!--navbar-category-->
	<?php foreach ($project_category as $key => $pc) { ?>
	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="<?= BASE_URL ?>project-list?category=<?= $pc->project_category_id ?>">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="<?= BASE_URL ?>uploads/project_category/<?= $pc->project_category_image ?>" alt=""></div>
							<h4><span><?= nl2br($pc->project_category_description_name_nl) ?></span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<?php $project_list = project_home_by_category($pc->project_category_id); ?>

						<?php if (!empty($project_list)) { ?>
							<?php foreach ($project_list as $key => $ps) { ?>
							<div class="col-md-4 wow fadeInUp">
								<div class="card card-project normal wow fadeIn">
									<div class="card-photo">
										<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $ps->project_image ?>);">
											<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
										</a>

										<div class="card-share">
											<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($ps->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $ps->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
											<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($ps->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $ps->project_id ?>" data-toggle="tooltip" title="Like"></div>
											<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>"></div> 
										</div><!--card-share-->
									</div><!--card-photo-->

									<div class="card-body d-flex">
										<div class="col-left">
											<a class="avatar" href="<?= BASE_URL ?>profile?uid=<?= $ps->member_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $ps->member_avatar ?>);"></a>
										</div>
										<div class="col-right">
											<h3><a href="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>"><?= $ps->project_name ?></a></h3>
				 							<div class="post-info">
												<div class="author">by <a href="<?= BASE_URL ?>profile?uid=<?= $ps->member_id ?>"><?= $ps->member_username ?></a></div>
												<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($ps->project_date_end) ?> days left</div>
											</div><!--post-info-->
										</div><!--col-right-->
									</div><!--card-body-->

									<div class="card-footer">
										<div class="progress-box dark">
											<?php $donate_total = donate_total($ps->project_id,$ps->project_target); ?>
											<div class="progress">
					                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
											</div>

											<div class="progress-label">
												<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($ps->project_target) ?></div>
												<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= number_format($donate_total['per_cen'],2); ?>%</div>
											</div>
										</div><!--progress-box-->
									</div><!--card-footer-->

								</div><!--card-project-->
							</div><!--col-md-4--> 
							<?php } ?>
							<div class="col-md-12 wow fadeIn">
								<div class="viewall">
									<a href="<?= BASE_URL ?>project-list?category=<?= $pc->project_category_id ?>">ดูทั้งหมด <span></span></a>
								</div>
							</div>
						<?php }else{ ?>
							<div class="col-md-12 wow fadeInUp text-center">
								<div class="card-empty">
									<h5>ไม่พอข้อมูลในหมวดหมู่ <?= $pc->project_category_description_name ?></h5>
								</div>
								
							</div><!--col-md-4--> 
						<?php } ?>
						
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 
	<?php } ?>
	<!--================[End] Section 'Comics & Illustration' ====================-->

	
	<div class="footer-space b-bottom"></div>

	<!--================[End] Section 'Latest News' ====================-->

	<div class="section section-news pt-0">
		<div class="container">
			<div class="section-title"><h2 class="title-lg">Latest News</h2></div>

			<div class="swiper-overflow">
				<div class="swiper-container swiper-news">
					<div class="swiper-wrapper">
					<?php
					/* var_dump($project_news); */
						foreach($project_news as $project_news_oj){
							?>
			        	<div class="swiper-slide">
			        		<div class="card card-news wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="<?= BASE_URL ?>news-detail-<?= $project_news_oj->project_news_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project_news/<?= $project_news_oj->project_news_image ?>);">
										<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>news-detail-<?= $project_news_oj->project_news_id ?>"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="<?= BASE_URL ?>news-detail-<?= $project_news_oj->project_news_id ?>"> <?= $project_news_oj->project_news_title ?></a></h3>
									<p><?= $project_news_oj->project_news_detail ?></p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ <?= $project_news_oj->project_news_created_at ?></div>
									<div class="readmore-link"><a href="<?= BASE_URL ?>news-detail-<?= $project_news_oj->project_news_id ?>">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
			        	</div><!--swiper-slide-->
						<?php
						}

						?>

					</div><!--swiper-wrapper-->
				</div><!--swiper-news-->

				<div class="swiper-pagination static news"></div> 
			</div><!--swiper-overflow-->
		</div><!--container-->
	</div><!--section-news-->

	<!--================[End] Section 'Latest News' ====================-->

	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->

<!--====================== Modal ====================-->

<?= $this->template->build('frontend/script'); ?>

</body>
</html>