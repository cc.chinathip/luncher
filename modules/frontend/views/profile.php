<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
	
	<div class="section section-userhomepage">
 		<div class="background" style="background-image: url(<?= BASE_URL ?>asset/launcher/img/thumb/photo-1920x935--1.jpg);"></div>
 		<div class="container">
 			<div class="user-homepage-box">
 				<div class="row space-10 align-items-center">
 					<div class="avatar wow fadeIn" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $project_list[0]->member_avatar ?>);"></div>
 					<div class="col">
	 					<h2 class="wow fadeInUp" data-wow-delay="0.1s"><?= $project_list[0]->member_username ?></h2>
	 					<p class="wow fadeInUp" data-wow-delay="0.15s"><?= $project_list[0]->member_biography ?></p>
	 				</div>
 				</div><!--row-->
 			</div><!--user-info-box-->
 		</div><!--container-->
 	</div><!--section-userhomepage-->

	<div class="section section-body pt-0">
 		<div class="container">
 			<ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="<?= BASE_URL ?>">หน้าแรก</a></li> 
			    <li class="breadcrumb-item active"><span><?= $project_list[0]->member_username ?></span></li> 
			</ol>

			<div class="project-list"> 
				<div class="row">
					<?php if (isset($project_list) && !empty($project_list)) { ?>
						<?php foreach ($project_list as $key => $pl) { ?>
						<div class="col-xl-3 col-md-4 col-sm-6 wow fadeIn">
							<div class="card card-project normal">
								<div class="card-photo">
									<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $pl->project_image ?>);">
										<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($pl->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $pl->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($pl->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $pl->project_id ?>" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>"></div>
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>"><?= $pl->project_name ?></a></h3>
									<div class="post-info pt-1">
										<div class="author">by <a href="javascript:void(0)"><?= $pl->member_username ?></a></div>
											<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($pl->project_date_end) ?> days left</div>
									</div><!--post-info-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box dark">
										<?php $donate_total = donate_total($pl->project_id,$ps->project_target); ?>
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($pl->project_target) ?></div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= number_format($donate_total['per_cen'],2); ?>%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-3-->
						<?php } ?>
 					<?php }else{ ?>
 						<div class="col-md-12 wow fadeInUp text-center">
							<div class="card-empty">
								<h5>ไม่พบข้อมูล</h5>
							</div>
							
						</div><!--col-md-4--> 
 					<?php } ?>
				</div><!--row-->
			</div><!--project-list-->
 		</div><!--container-->
 	</div><!--section-body-->
 
	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->
</div>


<!--====================== Modal ====================-->

<div class="modal modal-search fade" id="searchModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<button class="btn btn-close" data-dismiss="modal"></button>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="input-block search">
    					<input type="text" name="" class="form-control" placeholder="ค้นหาโปรเจคที่คุณสนใจ...">
    					<span class="icons icon-search"></span>
    				</div>
    			</div><!--col-md-12-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">หมวดหมู่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">พื้นที่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    		</div><!--row-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->


<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
 	
<script type="text/javascript">
	var swiperhighlight = new Swiper('.swiper-pj-highlight', {
    	slidesPerView:2,
	    spaceBetween:30,
	    slidesPerGroup: 2,
	    speed: 800,
	    loop: false,
	    observer: true,
	    effect: 'slide',
	    observeParents: true,
	    pagination: {
	        el: '.swiper-pagination.highlight',
	        clickable: true,
	    }, 
	    breakpoints: {
		    576: {
		        slidesPerView: 1, 
		        slidesPerGroup: 1,
	          	spaceBetween: 10, 
		    }  
	    }
    });

	$('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 
</script> 
</body>
</html>