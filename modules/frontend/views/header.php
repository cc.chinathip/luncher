<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes"/>

<title>Launcher</title>   
 
<link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap&subset=thai" rel="stylesheet"> 

<link href="<?= base_url('asset/launcher/fonts/fonts.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/animate.css') ?>" rel="stylesheet">  
<link href="<?= base_url('asset/launcher/css/swiper.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/jquery.fancybox.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/bootstrap-datepicker.css') ?>" rel="stylesheet">
<link href="<?= base_url('asset/launcher/css/global.css') ?>" rel="stylesheet">
<style type="text/css">
	.card-project.normal .post-info .author{
		width: 42%;
	}
	.card-project .post-info a{
		word-wrap: break-word;
	}
</style>