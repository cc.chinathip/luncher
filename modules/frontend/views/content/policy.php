<!DOCTYPE html>
<html lang="th">
<head> 
    <?= $this->template->build('frontend/header'); ?>
</head>
<body>

<div class="page"> 

	<?= $this->template->build('frontend/navigation'); ?>
	<div class="page-slideout"> 
	    <div class="section has-space-top">
	    	<div class="container">
	    		<ol class="breadcrumb">
				    <li class="breadcrumb-item"><a href="<?= BASE_URL ?>">หน้าแรก</a></li> 
				    <li class="breadcrumb-item active"><span><?= $content[0]->content_description_name ?></span></li> 
	            </ol>
	            
				<div class="content-detail">
					<h2><?= $content[0]->content_description_name ?></h2>
					<div class="mt-3"><?= $content[0]->content_description_detail ?></div>
					
				</div><!--content-detail-->
	    	</div><!--container-->
	    </div><!--section-->
	    
	    <?= $this->template->build('frontend/footer'); ?>
	</div><!--page-slideout-->
</div><!--page-->

</body>
</html>
<?= $this->template->build('frontend/script'); ?>
