<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 <style>
	 .upload_image_file{
		/*  opacity:0; */
		display:none;
	 }
</style>
<body>
<div class="preload"></div>
 

<div class="page "> 

<?= $this->template->build('frontend/project/navigation'); ?>
<div class="page-slideout"> 
	<div class="section section-body pt-0">

	<?= $this->template->build('frontend/project/project_header'); ?>

		<div class="project-form-content wow fadeIn">
			<div class="container">

				<div class="form-hgroup">
					<h2 class="title-md text-center">ข้อมูลพื้นฐานของโครงการ</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Molestie nibh faucibus tellus vivamus elit. Mi aliquet lacinia ut aliquet.</p>
				</div>

		<form id="form-basic-info" class="form" method="post">
			<input type="hidden" name="project_id" value="<?= $project_id; ?>">
			<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
				<div class="row form-2cols">
					<div class="col-lg-6 col-left">
						<h4 class="form-title mt-0"><span>ข้อมูลโครงการ</span></h4>
						<div class="input-block">
							<input type="text" class="form-control" name="project_name" placeholder="ชื่อโครงการ" required>
						</div>

						<div class="input-block">
							<textarea class="form-control" placeholder="บอกไอเดียที่จะสร้าง" name="project_description" required><?= $project_by_id->project_description ?></textarea>
						</div>

						<div class="input-block">
							<select name="project_category_id" class="form-control" required>
								<option>เลือกหมวดหมู่</option>
								<?php foreach ($project_category as $project_category_oj) { ?>
										<option <?= $project_by_id->project_category_id == $project_category_oj->project_category_id ? 'selected' : '' ?> value="<?= $project_category_oj->project_category_id ?>"><?= $project_category_oj->project_category_name ?></option>
								<?php } ?>
							</select>
						</div>

						<div class="input-block">
							<select name="project_subcategory_id" class="form-control">
								<option>เลือกหมวดหมู่ย่อย (ไม่จำเป็น)</option>
								<?php foreach ($project_subcategory as $project_subcategory_oj) { ?>
										<option value="<?= $project_subcategory_oj->project_subcategory_id ?>"><?= $project_subcategory_oj->project_subcategory_name ?></option>
								<?php } ?>
							</select>
						</div>

						<h4 class="form-title"><span>ที่ตั้งโครงการ</span></h4>

						<div class="input-block">
							<input type="text" class="form-control" name="project_location" placeholder="สถานที่" value="<?= $project_by_id->project_location ?>" required>
						</div>

						<div class="input-block">
								<select name="country" class="form-control" required>
									<option value="">ประเทศ</option>
									<?php foreach ($country as $v) { ?>
										<option value="<?= $v->country_id ?>"><?= $v->country_name ?></option>
									<?php } ?>
								</select>
						</div>
					</div><!--col-lg-6-->

					<div class="col-lg-6 col-right">
						<h4 class="form-title mt-0"><span>รูปภาพโครงการ</span></h4>

						<a href="javascript:void(0)" onclick="uploadImage()">
							<div class="photo-upload-box">
								<div class="group-text">
									<span class="icons icon-plus"></span>
									<h4>เพิ่มรูปภาพ</h4>
									<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
								</div>
							</div>
						</a>
						<input type="file" name="upload_image" class="upload_image_file" required>

						<h4 class="form-title"><span>เป้าหมายการระดมทุน</span></h4>

						<div class="input-block has-icon">
							<span class="icon"><span>฿</span></span>
							<input type="text" class="form-control" name="project_target" placeholder="0.00">
						</div>

						<h4 class="form-title"><span>ระยะเวลาโครงการ</span></h4>
						<div class="row">
							<div class="col-sm-6">
								<div class="input-block has-icon right">
									<span class="icon"><span class="icons icon-calendar"></span></span>
									<input type="text" class="form-control date date_start" name="project_date_start" placeholder="เปิดโครงการ" required>
								</div>
							</div><!--col-sm-6-->

							<div class="col-sm-6">
								<div class="input-block has-icon right">
									<span class="icon"><span class="icons icon-calendar"></span></span>
									<input type="text" class="form-control date date_end" name="project_date_end" placeholder="สิ้นสุดโครงการ" required>
								</div>
							</div><!--col-sm-6-->
						</div><!--row-->
					</div><!--col-lg-6-->
				</div><!--row-->

		</form>

				<div class="buttons d-block d-lg-none">
					<button class="btn btn-save" >บันทึก</button>
				</div> 
			</div><!--container-->
		</div><!--project-form-content-->
	</div><!--section-body-->
    <?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->


<!--====================== Modal ====================-->

<div class="modal modal-search fade" id="searchModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<button class="btn btn-close" data-dismiss="modal"></button>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="input-block search">
    					<input type="text" name="" class="form-control" placeholder="ค้นหาโปรเจคที่คุณสนใจ...">
    					<span class="icons icon-search"></span>
    				</div>
    			</div><!--col-md-12-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">หมวดหมู่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">พื้นที่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    		</div><!--row-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->


<div class="modal fade" id="projectSuccessModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<div class="project-success">

    			<div class="icon">
    				<img src="<?= base_url('asset/launcher/img/icons/icon-award-blue.svg')?>" alt="">
    			</div>
    			<h3>สร้างโครงการสำเร็จ</h3>

    			<a href="basic-info.html" class="btn btn-full">ตกลง</a>
    		</div>
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->


<?= $this->template->build('frontend/script'); ?>

 	<script type="text/javascript">
	/*------------[Start] bootstrap-datepicker.js ------------*/
	
	function uploadImage(){

		$('.upload_image_file').click()

		console.log('upload')
	}

	$(".upload_image_file").change(function(){

		readURLbg(this,'photo-upload-box');
	});

	/*------------[Start] circle-progress.js ------------*/
	 
    $(window).on('load resize', function(){
    	var window_size = $(window).width();
    	var p_size = 40;
    	var p_thickness = 3;

	  	if (window_size > 1281) {
	     	var p_size = 70;
    		var p_thickness = 5;
  
	  	}else if (window_size > 768) {
	     	var p_size = 50;
    		var p_thickness = 3;
  
	  	} 

	 	$('.nav .progress-circle').each(function(){
	    	$(this).appear(function() {
		      $(this).find(".circle").circleProgress({
			    //value: .84,
			    fill: {color: '#21D695'},
			    emptyFill: '#E0E0E0',
			    startAngle: -Math.PI / 4 * 2,
			    thickness:p_thickness,
			    size:p_size,
			  }).on('circle-animation-progress', function(event, progress, stepValue) {
			   // $(this).find('strong').text(stepValue.toFixed(2).substr(2));
			  });
			}); 
		});
	});
	$(function() { 

		$('.date_start').datepicker({
			dateFormat: "dd/mm/yyyy",
			todayHighlight: true,
			onSelect: function(dateText, instance) {

				console.log('dateText= ',dateText,'instance= ',instance)
				date = $.datepicker.parseDate(instance.settings.dateFormat, dateText, instance.settings);
				date.setMonth(date.getMonth() + 3);
				$('.date_end').datepicker("setDate", date);
			}
		});

		$('.date_end').datepicker({
			dateFormat: "dd/mm/yy",
			
		});

		//$('#form-basic-info').on('submit', function(e) {
	 $(".btn-save").click(function(e){

			console.log('prj submit',$('input[name="project_date_start"]').val())
			e.preventDefault();

			var formData = new FormData($('#form-basic-info')[0]);
			console.log('formData',formData)


			if($('input[name="project_name"]').val() !='' && $('input[name="project_description"]').val() !='' && $('input[name="project_description"]').val() !='' && $('input[name="project_category_id"]').val() !='' && $('input[name="project_location"]').val() !='' && $('input[name="country"]').val() !='' && $('input[name="project_target"]').val() !='' && $('input[name="project_date_start"]').val() !=''  && $('input[name="project_date_end"]').val() !=''){

					$.ajax({
					type: 'post',
					url: BASE_URL + 'frontend/project/project/update_basic_info',
					data: formData,
					async:false,
					enctype: 'multipart/form-data',
					cache: false,
					contentType: false,
					processData: false,
					success: function(data) {

						let json = JSON.parse(data);

						if(json === true){

							/* window.location.href = BASE_URL + 'rewards'; */
							$.confirm({
										title: 'บันทึก',
										content: 'บันทึกสำเร็จ',
										autoClose: 'Close|2000',
										type: 'green',
										buttons: {
											Close: {}
										}
								});
							window.location.href = BASE_URL + 'story-'+'<?= $project_id; ?>';

						}
						console.log(json)
					},
					error: function(data) {
						console.log("error : " + data);
					}
				});

			}else{

				/* window.location.href = BASE_URL + 'rewards'; */
				$.confirm({
										title: 'แจ้งเตือน',
										content: 'กรุณากรอกข้อมูลให้ครบ',
										autoClose: 'Close|3000',
										type: 'red',
										buttons: {
											Close: {}
										}
								});


			}
		
			
	 });
				

	//});
});

</script>
</body>
</html>