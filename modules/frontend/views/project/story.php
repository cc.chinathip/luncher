<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 <style>
	 .upload_image_file{
		/*  opacity:0; */
		display:none;
	 }
</style>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
<body>
<div class="preload"></div>
 

<div class="page "> 
<?= $this->template->build('frontend/project/navigation'); ?>
<div class="page-slideout"> 
	<div class="section section-body pt-0">
    <?= $this->template->build('frontend/project/project_header'); ?>
	<form id="form-story">
	<input type="hidden" name="project_id" value="<?= $project_id; ?>" class="project_id">
			<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
		<div class="project-form-content wow fadeIn">
			<div class="container">
				<div class="form">
					<div class="form-hgroup">
						<h2 class="title-md text-center">แนะนำโครงการของคุณ</h2>
						<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Molestie nibh faucibus tellus vivamus elit. Mi aliquet lacinia ut aliquet.</p> -->
					</div>

					<h4 class="form-title mt-2"><span>รายละเอียดโครงการ</span></h4>
					<div class="input-block text-only">
					<!-- 	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna pharetra quis congue pellentesque ipsum. Nunc diam at enim mi sit neque. Id diam, massa magna pellentesque id suspendisse aliquam. Convallis pellentesque habitasse sed urna eget eu laoreet erat.</div>
 -->
					<!-- <div class="texteditor" name="project_detail">
					</div> --><!--texteditor-->
					<textarea class="texteditor" name="project_detail"></textarea>
 
				</div><!--form--> 

				<div class="buttons d-block d-lg-none">
					<button class="btn btn-save btn_save_description" >บันทึก</button>
				</div> 
			</div><!--container-->
		</div><!--project-form-content-->
	</div><!--section-body-->
	</form>


    <?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->



<?= $this->template->build('frontend/project/search_project'); ?>
<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 	<script type="text/javascript">
	
    $('.texteditor').summernote();

	$('.note-insert .btn').click(function(event){
	    $('.modal-backdrop').appendTo(".note-editor");
	});	 
   /*------------[Start] circle-progress.js ------------*/
    
   $(window).on('load resize', function(){
       var window_size = $(window).width();
       var p_size = 40;
       var p_thickness = 3;

         if (window_size > 1281) {
            var p_size = 70;
           var p_thickness = 5;
 
         }else if (window_size > 768) {
            var p_size = 50;
           var p_thickness = 3;
 
         } 

        $('.nav .progress-circle').each(function(){
           $(this).appear(function() {
             $(this).find(".circle").circleProgress({
               //value: .84,
               fill: {color: '#21D695'},
               emptyFill: '#E0E0E0',
               startAngle: -Math.PI / 4 * 2,
               thickness:p_thickness,
               size:p_size,
             }).on('circle-animation-progress', function(event, progress, stepValue) {
              // $(this).find('strong').text(stepValue.toFixed(2).substr(2));
             });
           }); 
       });
   });
   
	$(function() { 

		//$('#form-basic-info').on('submit', function(e) {

	$('.note-editable').attr('name', 'project_detail');
	 $(".btn-save").click(function(e){

		 

		/* var detail_project = $('.note-editable').summernote('code');
		var project_id = $('.project_id').val(); */
		 //console.log('note-editable',detail_project)


			e.preventDefault();

			var formData = new FormData($('#form-story')[0]);
			console.log('formData',formData)
		
			$.ajax({
				type: 'post',
				url: BASE_URL + 'frontend/project/project/update_story',
				data: formData,
				async:false,
				enctype: 'multipart/form-data',
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {

					let json = JSON.parse(data);

					if(json === true){

						window.location.href = BASE_URL + 'payment-project-'+'<?= $project_id; ?>';

					}
					console.log(json)
				},
				error: function(data) {
					console.log("error : " + data);
				}
			});
	 });
				

	//});
});



</script>
</body>
</html>