<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 <style>
	 .upload_image_file{
		display:none;
	 }
</style>
<body>
<div class="preload"></div>
 

<div class="page "> 
<?= $this->template->build('frontend/project/navigation'); ?>

<div class="page-slideout"> 
	<div class="section section-body pt-0">

	<?= $this->template->build('frontend/project/project_header'); ?>
	<form id="form-rewards" class="form" method="post">	 	
		<div class="project-form-content wow fadeIn">
			<div class="container">
				<div class="form-hgroup pb-3">
					<h2 class="title-md text-center">เพิ่มรางวัลของคุณ</h2>
					<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Molestie nibh faucibus tellus vivamus elit. Mi aliquet lacinia ut aliquet.</p> -->
				</div>

				<div class="buttons d-block d-lg-none pt-0">
					<button class="btn btn-rewards-add">
						<span class="icons"></span>
						เพิ่มรางวัล
					</button>
                </div>
        <?php
        /* var_dump($reward_all); */
        
                foreach($reward_all as $reward_all_oj){

                    ?>
                        <div class="rewards-box">
                            <table class="table table-rewards">
                                <tr>
                                    <th class="col1">จำนวนเงิน<span class="nowrap">ขั้นต่ำ</span></th>
                                    <th class="col2">รายละเอียด</th>
                                    <th class="col3">รวม</th>
                                </tr>
                                <tr>
                                    <td class="col1 price">฿<?= $reward_all_oj->project_award_minimum_amount ?></td>
                                    <td class="col2">
                                        <h3><?= $reward_all_oj->project_award_description ?></h3>
                                        <p class="date">การจัดส่งโดยประมาณ เม.ย. 2563</p>
                                    </td>
                                    <td class="col3 price">฿<?= $reward_all_oj->project_award_amount ?></td>
                                </tr>
                            </table>

                            <div class="rewards-manage">
                                <div class="row align-items-center h-100">
                                    <div class="col-4"><?= $reward_all_oj->project_award_amount ?> ผู้สนับสนุน</div>
                                    <div class="col-8">
                                        <ul class="nav manage-item">
                                            <li><a href="<?= base_url('rewards-edit-'.$reward_all_oj->project_award_id) ?>"><img class="svg-js" src="img/icons/icon-pencil-blue.svg" alt=""> แก้ไข</a></li>
                                            <li><a href="javascript:void(0)" onclick="delete_reward(<?= $reward_all_oj->project_award_id ?>)" ><img class="svg-js" src="img/icons/icon-bin.svg" alt=""> ลบ</a></li>
                                        </ul>
                                    </div>
                                </div><!--row-->
                            </div><!--rewards-manage-->
						</div><!--rewards-box-->
						<input type="hidden"  name="project_reward_id" value="<?= $reward_all_oj->project_award_id ?>">
                
                    <?php

                }

			?>
			 <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">

				
			</div><!--container-->
		</div><!--project-form-content-->
	</div><!--section-body-->
</form>
    <?= $this->template->build('frontend/footer'); ?>


</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/project/search_project'); ?>
<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
	 <script type="text/javascript">
	 
	 function delete_reward(id){

		console.log('delete_reward',id)

			//var formData = new FormData($('#form-rewards')[0]);
			//console.log('formData',formData)

			var csrfName = '<?=$this->security->get_csrf_token_name();?>',
			csrfHash = '<?=$this->security->get_csrf_hash();?>';
			var dataJson = { [csrfName]: csrfHash, project_reward_id:id };

				$.confirm({
							title: 'Confirm!',
							content: 'คุณจะยืนยันจะลบหรือไม่!',
							buttons: {
								confirm: function () {
									$.alert('Confirmed!');
									$.ajax({
										type: 'post',
										url: BASE_URL + 'frontend/project/project/delete_reward',
										/* data: formData, */
										data:dataJson,
										async:false,
										enctype: 'multipart/form-data',
										cache: false,
										/* contentType: false,
										processData: false, */
										success: function(data) {

											let json = JSON.parse(data);

											if(json.status === true){

												$.confirm({
                                                        title: 'บันทึก',
                                                        content: 'ลบรางวัลสำเร็จ',
                                                        autoClose: 'Close|2000',
                                                        type: 'green',
                                                        buttons: {
                                                            Close: {}
                                                        }
                                                });

                                                setTimeout(function(){ location.reload(); }, 3000);
											}

										
											console.log(json)
										},
										error: function(data) {
											console.log("error : " + data);
										}
									});
								},
								cancel: function () {
									$.alert('Canceled!');
								},
								
							}
					});

			
	 }
	/*------------[Start] bootstrap-datepicker.js ------------*/
	$('input.date').datepicker({
	    format: "dd/mm/yyyy",
	    todayHighlight: true
	});
	function uploadImage(){

		$('.upload_image_file').click()

		console.log('upload')
	}

	$(".upload_image_file").change(function(){

		readURLbg(this,'photo-upload-box');
	});

	/*------------[Start] circle-progress.js ------------*/
	 
    $(window).on('load resize', function(){
    	var window_size = $(window).width();
    	var p_size = 40;
    	var p_thickness = 3;

	  	if (window_size > 1281) {
	     	var p_size = 70;
    		var p_thickness = 5;
  
	  	}else if (window_size > 768) {
	     	var p_size = 50;
    		var p_thickness = 3;
  
	  	} 

	 	$('.nav .progress-circle').each(function(){
	    	$(this).appear(function() {
		      $(this).find(".circle").circleProgress({
			    //value: .84,
			    fill: {color: '#21D695'},
			    emptyFill: '#E0E0E0',
			    startAngle: -Math.PI / 4 * 2,
			    thickness:p_thickness,
			    size:p_size,
			  }).on('circle-animation-progress', function(event, progress, stepValue) {
			   // $(this).find('strong').text(stepValue.toFixed(2).substr(2));
			  });
			}); 
		});
	});
	$(function() { 

		//$('#form-basic-info').on('submit', function(e) {
	 $(".btn-save").click(function(e){

			console.log('prj submit',$('input[name="project_date_start"]').val())
			e.preventDefault();

			var formData = new FormData($('#form-basic-info')[0]);
			console.log('formData',formData)
		
			$.ajax({
				type: 'post',
				url: BASE_URL + 'frontend/project/project/update_basic_info',
				data: formData,
				async:false,
				enctype: 'multipart/form-data',
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {

					let json = JSON.parse(data);

					if(json.status === true){

						window.location.href = BASE_URL + 'rewards';

					}
					console.log(json)
				},
				error: function(data) {
					console.log("error : " + data);
				}
			});
	 });
				

	//});
});

</script>
</body>
</html>