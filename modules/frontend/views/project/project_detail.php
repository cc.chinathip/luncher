<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
	
	<div class="section has-space-top">
		<div class="container">
			<ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="<?= BASE_URL ?>">หน้าแรก</a></li> 
			    <li class="breadcrumb-item"><a href="<?= BASE_URL ?>project-list?category=<?= $project_detail->project_category_id ?>"><?= $project_detail->project_category_name ?></a></li>
			    <li class="breadcrumb-item active"><span><?= $project_detail->project_name ?></span></li> 
			</ol>

			<div class="row">
				<div class="col-xl-7 mb-3 light">
					<h2 class="pj-title"><?= $project_detail->project_name ?></h2>
					<p><?= $project_detail->project_description ?></p>
				</div><!--col-xl-7-->
				<div class="col-xl-12">
					<div class="clearfix"></div>
				</div>
				<input type="hidden" name="project_id" id="project_id" value="<?= $project_detail->project_id ?>">
				<input type="hidden" name="laun_logged" id="laun_logged" value="<?= $this->session->userdata("laun_logged") ?>">
				<div class="col-xl-8">
					<div class="project-image" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $project_detail->project_image ?>);">
						<a data-fancybox href="<?= BASE_URL ?>uploads/project/<?= $project_detail->project_image ?>" data-fa>
							<img class="w-100" src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x63--blank.png" alt="">
						</a>
					</div>
				</div><!--col-xl-8-->

				<div class="col-xl-4">
					<div class="project-infos">
						<div class="row user-group align-items-center">
							<div class="avatar" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $project_detail->member_avatar ?>);"></div>
							<div class="col">
								<h4><?= $project_detail->project_name ?></h4>
								<ul class="user-infos">
									<li><a href="<?= BASE_URL ?>profile?uid=<?= $project_detail->member_id ?>"><span class="icons icon-user"></span> <span class="text"><?= $project_detail->member_username ?></span></a></li>
									<li><a href="<?= BASE_URL ?>project-list?category=<?= $project_detail->project_category_id ?>"><span class="icons icon-tag"></span> <span class="text"><?= $project_detail->project_category_name ?></span></a></li>
									<li><a href="javascript:void(0)"><span class="icons icon-pin"></span> <span class="text"><?= $project_detail->project_location ?></span></a></li>
								</ul>
							</div>
						</div><!--row user-group-->

						<div class="progress-box dark">
							<?php $donate_total = donate_total($project_detail->project_id,$project_detail->project_target); ?>
							<div class="progress">
	                        	<div class="progress-bar" data-percentage="<?= $donate_total['per_cen']; ?>"></div>  
							</div>

							<div class="progress-label">
								<div class="price">฿<?= number_format($donate_total['total']); ?></div>
								<div class="percentage">เป้าหมาย ฿<?= number_format($project_detail->project_target) ?></div>
							</div>
						</div><!--progress-box-->

						<ul class="statistics">
							<li>
								<div class="group">
									<h2 class="pj-title"><?= donate_count($project_detail->project_id) ?></h2>
									<p>ผู้สนับสนุน</p>
								</div>
							</li>
							<li>
								<div class="group">
									<h2 class="pj-title"><?= count_day($project_detail->project_date_start) ?></h2>
									<p>วันที่ผ่านมา</p>
								</div>
							</li>
						</ul>

						<div class="buttons">
							<div class="row">
								<div class="col-12">
									<button class="btn btn-full m-0" data-toggle="modal" data-target="#donateModal">สนับสนุนโครงการนี้</button>
								</div>

								<div class="col-4">
									<div class="followus">
										<a class="icons icon-facebook" href="javascript:void(0)" data-sharer="facebook" data-url="<?= BASE_URL ?>project-detail/<?= $project_detail->project_id ?>"></a>
										<a class="icons icon-twitter" href="javascript:void(0)" data-sharer="twitter" data-url="<?= BASE_URL ?>project-detail/<?= $project_detail->project_id ?>"></a> 
									</div>
								</div>

								<div class="col-8">
									<p class="text">วันสิ้นสุดโครงการ <?= date("l, d F Y", strtotime($project_detail->project_date_end)) ?>
								<!-- พฤหัสบดี, 19 ธันวาคม 2020  -->
							</p>
								</div>
							</div><!--row-->
						</div><!--buttons-->
					</div><!--project-infos-->
				</div><!--col-xl-4-->
			</div><!--row-->

			<div class="project-detail">
				<ul class="nav nav-pjdetails-tabs">
				  <li class="nav-item">
				    <a class="nav-link active" data-toggle="tab" href="#tab_campaign"><span>แคมเปญ</span></a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab"  href="#tab_update"><span>อัพเดท</span></a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#tab_comment"><span>คอมเมนต์</span></a>
				  </li>
				</ul>

				<div class="tab-content tab-pjdetails-content">
				  <div class="tab-pane fade show active" id="tab_campaign">
				  	<div class="article">
				  		<h2 class="pj-title">เรื่องราว</h2>

				  		<?= $project_detail->project_detail ?>
				  	</div><!--article-->
				  </div><!--tab-pane-->

				  <!--==============[End] 'แคมเปญ' =============-->

				  <div class="tab-pane fade" id="tab_update">
				  	<?php foreach ($project_update as $vpc) { ?>
				  		<div class="card card-pj-info news">
				  			<div class="card-header">
				  				<div class="infos">
				  					<div class="row">
				  						<div class="avatar" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $vpc->member_avatar ?>);"></div>
				  						<div class="col">
				  							<h4><?= $vpc->member_username ?></h4>
				  							<p><?= date("l, d F Y", strtotime($vpc->project_update_created_at)) ?></p>
				  						</div>
				  					</div><!--row-->
				  				</div><!--infos-->
				  			</div><!--card-header-->

				  			<div class="card-body">
				  				<div><?= $vpc->project_update_detail ?></div>
				  				<div class="readmore" data-id="<?= $vpc->project_update_id ?>"><span>อ่านเพิ่มเติม</span></div>
				  			</div><!--card-body-->
				  		</div><!--card-pj-info-->
				  		<div class="news-detail-content" id="update-content-<?= $vpc->project_update_id ?>">

				  			<div class="news-details-close" data-id="<?= $vpc->project_update_id ?>">
				  				<span class="icons icon-goback"></span>
				  				ย้อนกลับ
				  			</div>
				  			
				  			<div class="card card-pj-info details">
					  			<div class="card-header">
					  				<h3><?= $project_detail->project_name ?></h3>

					  				<div class="infos">
					  					<div class="row align-items-center">
					  						<div class="avatar" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $vpc->member_avatar ?>);"></div>
					  						<div class="col">
					  							<h4><?= $vpc->member_username ?></h4>
					  							<p><?= date("l, d F Y", strtotime($vpc->project_update_created_at)) ?></p>
					  						</div>
					  					</div><!--row-->
					  				</div><!--infos-->
					  			</div><!--card-header-->

					  			<div class="card-body">
					  				<?= $vpc->project_update_detail ?>
					  			</div><!--card-body-->
					  		</div><!--card-pj-info-->
				  		</div><!--news-detail-content-->
				  		<?php } ?>

				  </div><!--tab-pane-->

				  <div class="tab-pane fade" id="tab_comment">
				  	<div id="new_comment"></div>
				  	<?php foreach ($project_comment as $vpc) { ?>
				  		<div class="card card-pj-info comment">
				  			<div class="card-header"> 
				  				<div class="infos">
				  					<div class="row">
				  						<div class="avatar" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $vpc->member_avatar ?>);"></div>
				  						<div class="col">
				  							<h4><?= $vpc->member_username ?></h4>
				  							<p><?= date("l, d F Y", strtotime($vpc->project_comment_created_at)) ?></p> 
				  						</div>
				  					</div>
				  				</div>
				  			</div>
				  			<div class="card-body">
				  				<p><?= nl2br($vpc->project_comment_detail) ?></p> 
				  			</div>
				  		</div>
					<?php } ?>
						<?php if ($this->session->userdata("laun_logged")) { ?>
							<div class="type-comment-group">
					  			<div class="avatar" style="background-image: url(<?= $this->session->userdata('laun_avatar') ?>);"></div>
					  			<div class="col">
					  				<form id="form-comment" data-action="<?= BASE_URL ?>project/save_comment/<?= $project_detail->project_id ?>">
					  					<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
						  				<textarea class="form-control" placeholder="พิมพ์ข้อความ" name="project_comment" required></textarea>
						  				<button class="btn btn-send" type="submit"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-pencil-blue.svg" alt=""></button>
					  				</form>
					  			</div>
					  		</div><!--type-comment-group-->
						<?php }else{ ?>
							<h6 class="text-center">กรุณา <a href="<?= BASE_URL ?>auth">เข้าสู่ระบบ</a> ก่อนแสดงความคิดเห็น</h6>
						<?php } ?>
				  		
				  </div><!--tab-pane-->

				  <!--==============[End] 'คอมเมนต์' =============-->

				</div><!--tab-content-->
			</div><!--project-detail-->

			<div class="project-related">
				<hr>
				<h2 class="pj-title">โครงการอื่นๆที่คุณอาจชอบ</h2>

				<div class="swiper-overflow">
					<div class="swiper-container swiper-related">
						<div class="swiper-wrapper">
							<?php foreach ($project_slide as $key => $ps) { ?>
								<div class="swiper-slide">
					        		<div class="card card-project normal wow fadeIn">
										<div class="card-photo">
											<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $ps->project_image ?>);">
												<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
											</a>

											<div class="card-share">
												<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($ps->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $ps->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
												<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($ps->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $ps->project_id ?>" data-toggle="tooltip" title="Like"></div>
												<div class="item share" data-toggle="tooltip" title="Share"></div> 
											</div><!--card-share-->
										</div><!--card-photo-->

										<div class="card-body d-flex">
											<div class="col-left">
												<a class="avatar" href="javascript:void(0)" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $ps->member_avatar ?>);"></a>
											</div>
											<div class="col-right">
												<h3><a href="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>"><?= $ps->project_name ?></a></h3>
													<div class="post-info">
													<div class="author">by <a href="javascript:void(0)"><?= $ps->member_username ?></a></div>
													<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($ps->project_date_end) ?> days left</div>
												</div><!--post-info-->
											</div><!--col-right-->
										</div><!--card-body-->

										<div class="card-footer">
											<div class="progress-box dark">
												<?php $donate_total = donate_total($ps->project_id,$ps->project_target); ?>
												<div class="progress">
						                        	<div class="progress-bar" data-percentage="<?= $donate_total['per_cen']; ?>"></div>  
												</div>

												<div class="progress-label">
													<div class="price wow fadeIn" data-wow-delay="0.1s">฿<?= number_format($ps->project_target) ?></div>
													<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= $donate_total['per_cen']; ?>%</div>
												</div>
											</div><!--progress-box-->
										</div><!--card-footer-->
									</div><!--card-project-->
					        	</div><!--swiper-slide-->
							<?php } ?>

				        </div><!--swiper-wrapper-->

				        <div class="swiper-pagination relative related"></div> 
				    </div><!--swiper-related-->
				</div><!--swiper-overflow-->
			</div><!--project-related-->
		</div><!--container-->

		
	</div><!--section-->
 	<div class="modal modal-donate fade" id="donateModal">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	    		<button class="btn btn-close" data-dismiss="modal"></button>
	    		
	    		<h2 class="pj-title">โปรดเลือกสนับสนุนโครงการนี้</h2>

	    		<div class="donate-box">
	    			<div class="radio-group">
	                    <input id="donate_1" value="not_reward" class="type_donate" name="type_donate" type="radio" checked=""> 
	                    <label for="donate_1">สนับสนุนโดยไม่มีรางวัล</label>
	                </div>
	    			<div class="inner">
	    				<h6>คำอธิบายถึงรางวัลนี้</h6> 
	    				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et erat vulputate, accumsan tellus in, facilisis elit. Nulla eu odio sed nulla pretium sagittis lobortis eget orci.</p>

		    			<div class="supporting-balance">
		    				<h3>ยอดเงินสนับสนุน</h3>
		    				<div class="d-flex">
		    					<div class="input-block has-icon">
									<span class="icon"><span>฿</span></span>
									<input type="number" class="form-control donate_not_reward" name="donate_amount_0" id="donate_amount_0" placeholder="100">
								</div>

								<button class="btn f16 donate_not_reward donate_not_reward_btn" type="button" data-id="0">ตกลง</button>
		    				</div><!--d-flex-->
		    			</div><!--supporting-balance-->
		    		</div><!--inner-->
	    		</div><!--donate-box-->
				<?php if (isset($project_award) && !empty($project_award)) { ?>
					<?php foreach ($project_award as $va) { ?>
		    		<div class="donate-box">
		    			<div class="radio-group">
		                    <input id="donate_2" value="reward" class="type_donate" name="type_donate" type="radio"> 
		                    <label for="donate_2">฿<?= $va->project_award_minimum_amount ?> หรือมากว่า</label>
		                </div>
		    			<div class="inner">
		    				<h6>คำอธิบายถึงรางวัลนี้</h6> 
		    				<p><?= $va->project_award_description ?></p>

		    				<!-- <h6>การจัดส่งโดยประมาณ</h6>
		    				<p><strong><?= $va->project_award_month ?> <?= $va->project_award_year ?></strong></p> -->


			    			<div class="supporting-balance">
			    				<h3>ยอดเงินสนับสนุน</h3>
			    				<div class="d-flex">
			    					<div class="input-block has-icon">
										<span class="icon"><span>฿</span></span>
										<input type="number" disabled class="form-control donate_reward" min="<?= $va->project_award_minimum_amount ?>" data-min="<?= $va->project_award_minimum_amount ?>" id="donate_amount_<?= $va->project_award_id ?>" placeholder="<?= $va->project_award_minimum_amount ?>">
									</div>

									<button class="btn f16 donate_reward donate_reward_btn" disabled type="button" data-id="<?= $va->project_award_id ?>">ตกลง</button>
			    				</div><!--d-flex-->
			    			</div><!--supporting-balance-->
			    		</div><!--inner-->
		    		</div><!--donate-box-->
		    		<?php } ?>
	    		<?php } ?>
	        </div><!--modal-content-->
	    </div><!--modal-dialog-->
	</div><!--modal-->
	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->
</div>


<!--====================== Modal ====================-->

<div class="modal modal-search fade" id="searchModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<button class="btn btn-close" data-dismiss="modal"></button>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="input-block search">
    					<input type="text" name="" class="form-control" placeholder="ค้นหาโปรเจคที่คุณสนใจ...">
    					<span class="icons icon-search"></span>
    				</div>
    			</div><!--col-md-12-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">หมวดหมู่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">พื้นที่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    		</div><!--row-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->


<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 

<script src="<?= base_url('asset/launcher/js/jquery.youtube.js') ?>"></script>  
<script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.5.2/vue.min.js"></script>
<script type="text/javascript"> 

	var swiperRelated = new Swiper('.swiper-related', {
    	slidesPerView:4,
	    spaceBetween:30,
	    slidesPerGroup: 4,
	    speed: 800,
	    loop: false,
	    observer: true,
	    effect: 'slide',
	    observeParents: true,
	    pagination: {
	        el: '.swiper-pagination.related',
	        clickable: true,
	    }, 
	    breakpoints: {
	    	1280: {
		        spaceBetween:20,
		    },
		    1024: {
		        slidesPerView:3,
			    spaceBetween:20,
			    slidesPerGroup: 3,
		    },
		    767: {
		        slidesPerView:2,
			    spaceBetween:20,
			    slidesPerGroup: 2,
		    },  
		    576: {
		        slidesPerView: 1, 
		        slidesPerGroup: 1,
	          	spaceBetween: 20, 
		    }  
	    }
    });

	$('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 

  	/*---------------jquery.youtube.js -----------------*/

	$('.video-box').click(function () {
	      $(this).addClass('playing');
	});

	/*---------------#tab_news -----------------*/

	$('.card-pj-info .readmore').click(function () {
		var data_id = $(this).data('id')
		console.log(data_id)
		$('#update-content-'+data_id).fadeIn();
		$('.card-pj-info.news').fadeOut();
	});

	$(".type_donate").click(function(){
            var radioValue = $("input[name='type_donate']:checked").val();
            if(radioValue == 'not_reward'){
            	$('.donate_not_reward').removeAttr('disabled')
            	$('.donate_reward').attr('disabled', 'disabled');
            }else{
            	$('.donate_not_reward').attr('disabled', 'disabled');
            	$('.donate_reward').removeAttr('disabled');
            }
        });

	$('.news-details-close').click(function () {
		var data_id = $(this).data('id')
		$('.news-detail-content').fadeOut();
		$('.card-pj-info.news').fadeIn();
	});

	$('.donate_not_reward_btn').click(function () {
		console.log($('#donate_amount_0').val())
		if ($('#laun_logged').val()) {
			if ($('#donate_amount_0').val() > 0) {
				$.ajax({
		            type: 'get',
		            url: BASE_URL+'donate/project?project_id='+$('#project_id').val()+'&amount='+$('#donate_amount_0').val(),
		            async:false,
		            cache: false,
		            contentType: false,
		            processData: false,
		            success: function(data) {
		              let json = JSON.parse(data);
		              console.log(json)
		              if (json.status) {
		              	window.location.href = BASE_URL + 'donate/payment?project_id='+$('#project_id').val()+'&donate_id='+json.data.donate_id;
		              }
		            },
		            error: function(data) {
		            	console.log(data)
		            }
		        });
			}else{
	        	$.confirm({
				    title: '',
				    content: 'ยอดเงินน้อยกว่าที่กำหนด',
				    autoClose: 'Close|3000',
				    type: 'green',
				    buttons: {
				        Close: {}
				    }
				});
			}
			
        }else{
        	$.confirm({
			    title: '',
			    content: 'กรุณาเข้าสู่ระบบก่อนสนับสนุนโครงการ',
			    autoClose: 'Close|3000',
			    type: 'green',
			    buttons: {
			        Close: {}
			    }
			});
		}
	});

	$('.donate_reward_btn').click(function () {
		var data_id = $(this).data('id')
		var data_min = $(this).data('min')
		if ($('#laun_logged').val()) {

			if ($('#donate_amount_'+data_id).val() > data_min) {
				$.ajax({
		            type: 'get',
		            url: BASE_URL+'donate/project?project_id='+$('#project_id').val()+'&amount='+$('#donate_amount_'+data_id).val()+'&reward='+data_id,
		            async:false,
		            cache: false,
		            contentType: false,
		            processData: false,
		            success: function(data) {
		              let json = JSON.parse(data);
		              console.log(json)
		              if (json.status) {
		              	window.location.href = BASE_URL + 'donate/payment?project_id='+$('#project_id').val()+'&donate_id='+json.data.donate_id+'&reward='+data_id;
		              }
		            },
		            error: function(data) {
		            	console.log(data)
		            }
		        });
			}else{
				$.confirm({
				    title: '',
				    content: 'ยอดเงินน้อยกว่าที่กำหนด',
				    autoClose: 'Close|3000',
				    type: 'green',
				    buttons: {
				        Close: {}
				    }
				});
			}
		}else{
			$.confirm({
			    title: '',
			    content: 'กรุณาเข้าสู่ระบบก่อนสนับสนุนโครงการ',
			    autoClose: 'Close|3000',
			    type: 'green',
			    buttons: {
			        Close: {}
			    }
			});
		}
	});

	

	$('#form-comment').each(function() {  //	
		if ($('#laun_logged').val()) {
			$(this).validate({
	   
			    submitHandler: function(form) {
			      
		            var formData = new FormData(form);

		            $.ajax({
		                type: 'post',
		                url: $('#form-comment').data('action'),
		                data: formData,
		                async:false,
		                enctype: 'multipart/form-data',
		                cache: false,
		                contentType: false,
		                processData: false,
		                success: function(data) {
		                  let json = JSON.parse(data);
		                  if (json.status) {
		                  	$('#form-comment')[0].reset();

		                  	var html = '<div class="card card-pj-info comment">'+
						  			'<div class="card-header"> '+
						  				'<div class="infos">'+
						  					'<div class="row">'+
						  						'<div class="avatar" style="background-image: url('+json.data.member_avatar+');"></div>'+
						  						'<div class="col">'+
						  							'<h4>'+json.data.member_username+' <small style="background: #21d695;color: black; padding: 1px 8px;border-radius: 8px;">รอตรวจสอบ</small></h4>'+
						  							'<p>'+json.data.project_comment_created_at+'</p> '+
						  						'</div>'+
						  					'</div>'+
						  				'</div>'+
						  			'</div>'+
						  			'<div class="card-body">'+
						  				'<p>'+json.data.project_comment_detail+'</p> '+
						  			'</div>'+
						  		'</div>'
		                  	$('#new_comment').prepend(html)
		                  }
		                 
		                },
		                error: function(data) {
		                	console.log(data)
		                }
		            });
			    }
			});
		}else{
			$.confirm({
			    title: '',
			    content: 'กรุณาเข้าสู่ระบบก่อนแสดงความคิดเห็น',
			    autoClose: 'Close|3000',
			    type: 'green',
			    buttons: {
			        Close: {}
			    }
			});
		}
	  	
	});
	
</script> 
</body>
</html>