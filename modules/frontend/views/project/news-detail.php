<!DOCTYPE html>
<html lang="th">
<head> 
    <?= $this->template->build('frontend/header'); ?>
</head>
<body>

<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>
<div class="page-slideout"> 
    <div class="section has-space-top">
    	<div class="container">
    		<ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="<?= BASE_URL ?>">หน้าแรก</a></li> 
			    <li class="breadcrumb-item"><a href="news.html">บทความ</a></li> 
			    <li class="breadcrumb-item active"><span>ชื่อบทความ</span></li> 
            </ol>
            
			<div class="content-detail">
				<div class="banner card">
                
					<img class="w-100" src="ฺ<?= BASE_URL.'uploads/project_news/' ?><?= $news_detail->project_news_cover ?>" alt="">
					<div class="card-share"> 
						<div class="item share" data-toggle="tooltip" title="Share"></div> 
					</div><!--card-share-->
				</div><!--banner-->
				<div class="hgroup">
					<h2 class="title-main"><?= $news_detail->project_news_title ?></h2>
					<p class="date">โพสต์เมื่อ พฤหัสบดี, <?= $news_detail->project_news_created_at ?></p>
				</div>
				<div class="article">
                    <p><?= $news_detail->project_news_detail ?></p>
                  
				</div><!--article-->
			</div><!--content-detail-->
    	</div><!--container-->
    </div><!--section-->

    <div class="section section-related">
    	<div class="container">
    		<h4 class="title-xs">บทความที่เกี่ยวข้อง</h4>

    		<div class="swiper-overflow">
	    		<div class="swiper-container swiper-related">
					<div class="swiper-wrapper">

                    <?php
                        foreach($project_news as $project_news_oj){

                        ?>
			        	<div class="swiper-slide">
			        		<div class="card card-news">
								<div class="card-photo">
									<a class="photo" href="<?= BASE_URL ?>news-detail-<?= $project_news_oj->project_news_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project_news/<?= $project_news_oj->project_news_image ?>);">
										<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png ?>" alt="">
									</a>

									<div class="card-share"> 
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="<?= BASE_URL ?>news-detail-<?= $project_news_oj->project_news_id ?>"><?= $project_news_oj->project_news_title ?></a></h3>
									<p><?= $project_news_oj->project_news_detail ?></p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ <?= $project_news_oj->project_news_created_at ?></div>
									<div class="readmore-link"><a href="<?= BASE_URL ?>news-detail-<?= $project_news_oj->project_news_id ?>">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
                        </div>
                        <?php
                        }

                        ?>

			        
			        </div><!--swiper-wrapper-->

			        <div class="swiper-pagination relative related"></div> 
			    </div><!--swiper-related-->
			</div><!--swiper-overflow-->
    	</div><!--container-->
    </div><!--section-related-->

    <?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->
</div><!--page-->

</body>
</html>
<?= $this->template->build('frontend/script'); ?>

<script type="text/javascript">
	var swiperRelated = new Swiper('.swiper-related', {
    	slidesPerView:4,
	    spaceBetween:30,
	    slidesPerGroup: 4,
	    speed: 800,
	    loop: false,
	    observer: true,
	    effect: 'slide',
	    observeParents: true,
	    pagination: {
	        el: '.swiper-pagination.related',
	        clickable: true,
	    }, 
	    breakpoints: {
	    	1280: {
		        spaceBetween:20,
		    },
		    1024: {
		        slidesPerView:3,
			    spaceBetween:20,
			    slidesPerGroup: 3,
		    },
		    767: {
		        slidesPerView:2,
			    spaceBetween:20,
			    slidesPerGroup: 2,
		    },  
		    576: {
		        slidesPerView: 1, 
		        slidesPerGroup: 1,
	          	spaceBetween: 20, 
		    }  
	    }
    });
</script> 