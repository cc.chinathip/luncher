<div class="project-form-header wow fadeIn">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-10">
						<ul class="nav nav-project-form">
							<li>
								<a href="javascript:void(0)">
									<div class="progress-circle">
										<div class="circle" data-value="0.75"></div> 
								    </div>

								    <span class="text">ข้อมูลพื้นฐาน</span>
								</a>
							</li>

							<li>
								<a href="<?= base_url('rewards') ?>">
									<div class="progress-circle">
										<div class="circle" data-value="0"></div> 
								    </div>

								    <span class="text">รางวัล</span>
								</a>
							</li>

							<li class="active">
								<a href="javascript:void(0)">
									<div class="progress-circle">
										<div class="circle" data-value="0.50"></div> 
								    </div>

								    <span class="text">เรื่องราว</span>
								</a>
							</li>

							<li>
								<a href="javascript:void(0)">
									<div class="progress-circle">
										<div class="circle full" data-value="1"></div> 
								    </div>

								    <span class="text">การชำระเงิน</span>
								</a>
							</li>
						</ul>
					</div><!--col-lg-10-->
					


					<?php
					  $uri = current_url(true);
					  $uri2 =explode('/',$uri);
 
						 if($uri2[3] == 'rewards'){
							?>
							<div class="col-lg-2 d-none d-lg-block">
								<button class="btn btn-rewards-add" onclick="location.href='<?= base_url('rewards-add') ?>'">
									<span class="icons"></span>
									เพิ่มรางวัล
								</button>
							</div>

							<?php

						}else{
							?>
							 <div class="col-lg-2 d-none d-lg-block">
								<button class="btn btn-save" >บันทึก</button>
							</div>
							<?php

						}
					?>
				   

					

					
			    </div>
			</div><!--container-->
		</div><!--project-header-->