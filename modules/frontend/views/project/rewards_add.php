<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 <style>
	 .upload_image_file{
		display:none;
	 }
     .project_award_amount{
         display:none;
     }
     
</style>
<body>
<div class="preload"></div>
 

<div class="page "> 
<?= $this->template->build('frontend/project/navigation'); ?>

<div class="page-slideout"> 
	<div class="section section-body pt-0">

	<?= $this->template->build('frontend/project/project_header'); ?>
 <form id="form-rewards-add" class="form" method="post">
		<div class="project-form-content wow fadeIn">
			<div class="container">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
				<div class="form-hgroup">
					<h2 class="title-md text-center">เพิ่มรางวัลของคุณ</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Molestie nibh faucibus tellus vivamus elit. Mi aliquet lacinia ut aliquet.</p>
				</div>

				<div class="row form-2cols">
					<div class="col-md-6 col-left">
						<h4 class="form-title mt-0"><span>หัวข้อ</span></h4>
						<div class="input-block">
							<input type="text" class="form-control" name="project_award_name" placeholder="อธิบายสั้น ๆ ถึงรางวัลนี้">
						</div>

						<h4 class="form-title"><span>กำหนดจำนวนเงินจำนำขั้นต่ำสำหรับรางวัลนี้</span></h4>

						<div class="input-block has-icon">
							<span class="icon"><span>฿</span></span>
							<input type="text" class="form-control" name="project_award_minimum_amount" placeholder="1">
						</div>

						<h4 class="form-title"><span>Description</span></h4>
						<div class="input-block">
							<textarea class="form-control" placeholder="อธิบายอย่างละเอียด" nmae="project_award_description"></textarea>
						</div>

						<h4 class="form-title"><span>การจัดส่งโดยประมาณ</span></h4>
						<div class="row space-7">
							<div class="col-6">
								<div class="input-block">

                                    <select name="project_award_month" class="form-control"> 
                                         <option>เลือกเดือน</option>
                                        <?php

                                        $formattedMonthArray = array(
                                            "1" => "January",
                                            "2" => "February", 
                                            "3" => "March", 
                                            "4" => "April",
                                            "5" => "May", 
                                            "6" => "June", 
                                            "7" => "July", 
                                            "8" => "August",
                                            "9" => "September", 
                                            "10" => "October", 
                                            "11" => "November", 
                                            "12" => "December",
                                        );

                                            for ($i=1;$i<=count($formattedMonthArray);$i++){
                                                ?>
                                                <option  value="<?= $i ?>"><?= $formattedMonthArray[$i] ?></option>
                                                <?php
                                            }

                                       
                                        ?>
                                    </select>
								</div>
							</div><!--col-6-->

							<div class="col-6">
								<div class="input-block">
									<select name="project_award_year" class="form-control">
                                         <option>เลือกปี</option>
                                        <?php
                                            for ($i=2020;$i<=2050;$i++){
                                                    ?>
                                                 <option  value="<?= $i ?>"><?= $i ?></option>
                                                    <?php
                                            }
                                        ?>
									</select>
								</div>
							</div><!--col-6-->

							<div class="col-12">
								<div class="input-block">
									<select name="project_award_delivery" class="form-control">
												<option>เลือกการจัดส่ง</option>
									<?php	
											
										foreach($project_award_delivery as $project_award_delivery_oj){
											?>
											<option value="<?= $project_award_delivery_oj->project_award_delivery_id ?>"><?= $project_award_delivery_oj->project_award_delivery_name ?></option>
											<?php
										}
									?>
									</select>
								</div>
							</div><!--col-6-->
						</div><!--row-->

						<h4 class="form-title"><span>ปริมาณรางวัล</span></h4>
						<div class="row space-7 align-items-center">
							<div class="col-lg-6">
								<div class="input-block">
									<div class="radio-group">
					                    <input id="reward_amount1" value="1" name="project_award_amount_assign" type="radio" checked=""> 
					                    <label for="reward_amount1">ไม่จำกัด</label>
					                </div>

					                <div class="radio-group">
					                    <input id="reward_amount2" value="2" name="project_award_amount_assign" type="radio"> 
					                    <label for="reward_amount2">จำกัด</label>
					                </div>
								</div>
							</div><!--col-6-->

							<div class="col-lg-6">
								<div class="input-block">
									<input type="text" class="form-control project_award_amount" name="project_award_amount" placeholder="ระบุจำนวน">
								</div>
							</div><!--col-6-->
						</div><!--row-->
					</div><!--col-md-6-->

					<div class="col-md-6">
						<div class="award-preview wow fadeIn" data-wow-delay="0.2s">
							<div class="hgroup">
								<h3>แสดงตัวอย่างรางวัล</h3>
								<p>ดูว่ารางวัลนี้จะมีลักษณะอย่างไรในหน้าโครงการของคุณ</p>
							</div>

							<div class="inner">
								<h2>ลงเงิน 1 บาท หรือมากกว่า</h2>

								<p>อธิบายสั้น ๆ ถึงรางวัลนี้ <br>อธิบายอย่างละเอียด</p>

								<h6>การจัดส่งโดยประมาณ</h6>
								<p><strong>เดือน ปี</strong></p>

								<h6>ปริมาณรางวัล</h6>
								<p><strong>ไม่จำกัด</strong></p>
							</div>
						</div><!--award-preview-->
					</div><!--col-md-6-->
						 
				</div><!--row-->

				<div class="buttons d-block d-lg-none">
					<button class="btn btn-save" onclick="location.href='story.html'">บันทึก</button>
				</div> 
			</div><!--container-->
		</div><!--project-form-content-->
    </div><!--section-body-->
</form>
 
    <?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/project/search_project'); ?>
<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
 	<script type="text/javascript">
    /*------------[Start] bootstrap-datepicker.js ------------*/
    
    $("input[name='project_award_amount_assign']").change(function(){
        console.log('project_award_amount_assign',$(this).val())

        if($(this).val() == 2){

            $('.project_award_amount').show()
        }else{
            $('.project_award_amount').hide()
        }
    });

	$('input.date').datepicker({
	    format: "dd/mm/yyyy",
	    todayHighlight: true
	});
	function uploadImage(){

		$('.upload_image_file').click()

		console.log('upload')
	}

	$(".upload_image_file").change(function(){

		readURLbg(this,'photo-upload-box');
	});

	/*------------[Start] circle-progress.js ------------*/
	 
    $(window).on('load resize', function(){
    	var window_size = $(window).width();
    	var p_size = 40;
    	var p_thickness = 3;

	  	if (window_size > 1281) {
	     	var p_size = 70;
    		var p_thickness = 5;
  
	  	}else if (window_size > 768) {
	     	var p_size = 50;
    		var p_thickness = 3;
  
	  	} 

	 	$('.nav .progress-circle').each(function(){
	    	$(this).appear(function() {
		      $(this).find(".circle").circleProgress({
			    //value: .84,
			    fill: {color: '#21D695'},
			    emptyFill: '#E0E0E0',
			    startAngle: -Math.PI / 4 * 2,
			    thickness:p_thickness,
			    size:p_size,
			  }).on('circle-animation-progress', function(event, progress, stepValue) {
			   // $(this).find('strong').text(stepValue.toFixed(2).substr(2));
			  });
			}); 
		});
	});
	$(function() { 

	
	 $(".btn-save").click(function(e){

			e.preventDefault();

			var formData = new FormData($('#form-rewards-add')[0]);
			console.log('formData',formData)
		
			$.ajax({
				type: 'post',
				url: BASE_URL + 'frontend/project/project/add_rewards_project',
				data: formData,
				async:false,
				enctype: 'multipart/form-data',
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {

					let json = JSON.parse(data);

					if(json.status === true){

						$.confirm({
								    title: 'บันทึก',
								    content: 'สร้างรางวัลสำเร็จ',
								    autoClose: 'Close|2000',
								    type: 'green',
								    buttons: {
								        Close: {}
								    }
							});
							
							window.location.href = BASE_URL + 'rewards';

					}
					console.log(json)
				},
				error: function(data) {
					console.log("error : " + data);
				}
			});
	 });
				

	//});
});

</script>
</body>
</html>