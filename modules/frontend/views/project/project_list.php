<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
	<style type="text/css">
		.project-filter .hgroup .icon img{
			width: 40px;
    		height: 40px;
		}
	</style>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
	
	<div class="section gray-bg pb-4 has-space-top">
		<div class="container">
			<ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="<?= BASE_URL ?>">หน้าแรก</a></li> 
			    <li class="breadcrumb-item active"><span><?= $project_category[0]->project_category_description_name ?></span></li> 
			</ol>
			<?php if (isset($project_slide) && !empty($project_slide)) { ?>	
			<div class="swiper-container swiper-pj-highlight">
				<div class="swiper-wrapper">
					<?php foreach ($project_slide as $key => $ps) { ?>
						<div class="swiper-slide"> 
			        		<div class="card card-project lg no-bg wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $ps->project_image ?>);">
										<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($ps->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $ps->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($ps->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $ps->project_id ?>" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="<?= BASE_URL ?>profile?uid=<?= $ps->member_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $ps->member_avatar ?>);"></a>
									</div>
									<div class="col-right">
										<h3><a href="<?= BASE_URL ?>project-detail/<?= $ps->project_id ?>"><?= $ps->project_name ?></a></h3>
			 							<div class="post-info">
											<div class="author">by <a href="<?= BASE_URL ?>profile?uid=<?= $ps->member_id ?>"><?= $ps->member_username ?></a></div>
											<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($ps->project_date_end) ?> days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box dark">
										<?php $donate_total = donate_total($ps->project_id,$ps->project_target); ?>
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($ps->project_target) ?></div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">8<?= number_format($donate_total['per_cen'],2); ?>%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--swiper-slide-->
					<?php } ?>
					 
				</div><!--swiper-wrapper-->

				<div class="swiper-pagination relative highlight"></div> 
			</div><!--swiper-pj-highlight-->
			<?php } ?>	
		</div><!--container-->
	</div><!--section-->

 	<div class="section section-column in-project mt-3">
		<div class="container">

			<div class="sidebar">
				<div class="project-filter">
					<div class="hgroup">
						<div class="icon"><img class="svg-js" src="<?= BASE_URL ?>uploads/project_category/<?= $project_category[0]->project_category_image ?>" alt=""></div>
						<h3><?= $project_category[0]->project_category_description_name ?></h3>

						<hr>
					</div>
					<ul class="filter-list">
						<!-- <li>
							<div class="checkbox-group">
			                    <input id="subcategory_0" value="0" name="subcategory" type="checkbox"> 
			                    <label for="subcategory_0">All</label>
			                </div>
				        </li> -->
				        <?php foreach ($project_subcategory as $key => $psc) { ?>
				        	<li>
								<div class="checkbox-group">
				                    <input id="subcategory_<?= $psc->project_subcategory_id ?>" value="<?= $psc->project_subcategory_id ?>" name="subcategory" type="checkbox" <?= (!empty($fillter) && in_array($psc->project_subcategory_id, $fillter)) ? 'checked' : '' ?>> 
				                    <label for="subcategory_<?= $psc->project_subcategory_id ?>"><?= $psc->project_subcategory_description_name ?></label>
				                </div>
					        </li>
				        <?php } ?>
					</ul>
					<button class="btn btn-full" id="fillter" data-url="<?= $current_url ?>" type="button" style="margin-top: 15px;">Filter</button>
				</div><!--project-filter--> 
			</div><!--sidebar-->

			<div class="content">  
 				<div class="project-list">
 					<div class="row">
 						<?php if (isset($project_list) && !empty($project_list)) { ?>
 						<?php foreach ($project_list as $key => $pl) { ?>
 						<div class="col-lg-4 col-sm-6">
 							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/project/<?= $pl->project_image ?>);">
										<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($pl->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $pl->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($pl->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $pl->project_id ?>" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>"></div>
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="<?= BASE_URL ?>profile?uid=<?= $pl->member_id ?>" style="background-image: url(<?= BASE_URL ?>uploads/member/<?= $pl->member_avatar ?>);"></a>
									</div>
									<div class="col-right">
										<h3><a href="<?= BASE_URL ?>project-detail/<?= $pl->project_id ?>"><?= $pl->project_name ?></a></h3>
											<div class="post-info">
											<div class="author">by <a href="<?= BASE_URL ?>profile?uid=<?= $pl->member_id ?>"><?= $pl->member_username ?></a></div>
											<div class="date"><span class="icons icon-clock"></span> <?= set_day_letf($pl->project_date_end) ?> days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box dark">
										<?php $donate_total = donate_total($pl->project_id,$pl->project_target); ?>
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="<?= number_format($donate_total['per_cen'],2); ?>"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$<?= number_format($pl->project_target) ?></div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s"><?= number_format($donate_total['per_cen'],2); ?>%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
 						</div><!--col-lg-4 col-sm-6-->
 						<?php } ?>
 					<?php } ?>

 					<?php if ((empty($project_slide)) && (empty($project_list))) { ?>
 						<div class="col-md-12 wow fadeInUp text-center">
							<div class="card-empty">
								<h5>ไม่พบข้อมูลในหมวดหมู่ <?= $pc->project_category_description_name ?></h5>
							</div>
							
						</div><!--col-md-4--> 
 					<?php } ?>
 					</div><!--row-->

 					<div class="pagination-center">
 						<?= $pagination; ?>
					</div>
 				</div><!--project-list-->
			</div><!--content-->
		</div><!--container-->
	</div><!--section-column-->
 
	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->
</div>


<!--====================== Modal ====================-->

<div class="modal modal-search fade" id="searchModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<button class="btn btn-close" data-dismiss="modal"></button>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="input-block search">
    					<input type="text" name="" class="form-control" placeholder="ค้นหาโปรเจคที่คุณสนใจ...">
    					<span class="icons icon-search"></span>
    				</div>
    			</div><!--col-md-12-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">หมวดหมู่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    			<div class="col-md-6">
    				<div class="input-block">
    					<span class="input-text">พื้นที่</span>
    					<select>
    						<option>กรุณาเลือก</option>
    					</select>
    				</div>
    			</div><!--col-md-6-->

    		</div><!--row-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->


<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
 	
<script type="text/javascript">
	var swiperhighlight = new Swiper('.swiper-pj-highlight', {
    	slidesPerView:2,
	    spaceBetween:30,
	    slidesPerGroup: 2,
	    speed: 800,
	    loop: false,
	    observer: true,
	    effect: 'slide',
	    observeParents: true,
	    pagination: {
	        el: '.swiper-pagination.highlight',
	        clickable: true,
	    }, 
	    breakpoints: {
		    576: {
		        slidesPerView: 1, 
		        slidesPerGroup: 1,
	          	spaceBetween: 10, 
		    }  
	    }
    });

	$('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 

  	$('#fillter').on('click', function() { 
  		var c_url = $(this).data('url')
        var array = []; 
        $("input:checkbox[name=subcategory]:checked").each(function() { 
            array.push($(this).val()); 
        }); 

        window.location.href = c_url+'&subcategory='+array;
        console.log(array)
    }); 
</script> 
</body>
</html>