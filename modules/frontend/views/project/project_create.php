<!DOCTYPE html>
<html lang="th">
<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
<body>
<div class="preload"></div>
 
<div class="page"> 
<?= $this->template->build('frontend/project/navigation'); ?>

<div class="page-slideout">
	<div class="section section-body">
		<div class="container">
			<form id="form-project-create" class="form mw-600" method="post">
			<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
				<div class="form-hgroup">
					<h2 class="title-md text-center">เริ่มต้นโครงการของคุณ</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Molestie nibh faucibus tellus vivamus elit. Mi aliquet lacinia ut aliquet.</p>
				</div>
				<div class="input-block">
					<select name="project_category_id" class="form-control" required>
						<option>เลือกหมวดหมู่</option>
						<?php foreach ($project_category as $project_category_oj) { ?>
										<option value="<?= $project_category_oj->project_category_id ?>"><?= $project_category_oj->project_category_name ?></option>
							<?php } ?>
					</select>
				</div>
				<div class="input-block">
					<textarea class="form-control" placeholder="บอกไอเดียที่จะสร้าง" name="project_description" required></textarea>
				</div>
				<div class="input-block">
					<input type="text" class="form-control" placeholder="สถานที่" name="project_location" required>
				</div>
				<div class="input-block">
					<select name="project_province" class="form-control" required>
						<option value="">จังหวัด</option>
							<?php foreach ($province as $v) { ?>
										<option value="<?= $v->province_id ?>"><?= $v->province_name_th ?></option>
						<?php } ?>
					</select> 
				</div>

				<div class="input-block">
					<div class="checkbox-group">
	                    <input id="accept_condition" value="accept_condition" name="accept_condition" type="checkbox" checked=""> 
	                    <label for="accept_condition"><a href="<?= base_url('guide.html') ?>" target="_blank">ยอมรับเงื่อนไข</a></label>
	                </div>
				</div>

				<div class="buttons">
					<button class="btn btn-full m-0" type="submit" data-toggle="modal" data-target="#projectSuccessModal">สร้างโครงการ</button>
				</div>
			</form> 
		</div><!--container-->
	</div><!--section-body-->
 
	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->
</div>

<?= $this->template->build('frontend/project/search_project'); ?>
<?= $this->template->build('frontend/script'); ?>

<div class="display_modal"></div>

 	<script type="text/javascript">
	/*------------[Start] bootstrap-datepicker.js ------------*/

	$(function() { 

			$('#form-project-create').on('submit', function(e) {

				if($('#accept_condition').is(':checked')){

					console.log('prj submit')
					e.preventDefault();
					let fm = this
					var formData = new FormData(fm);
					console.log('formData',formData)
				
					$.ajax({
						type: 'post',
						url: BASE_URL + 'frontend/project/project/add_project',
						data: formData,
						async:false,
						enctype: 'multipart/form-data',
						cache: false,
						contentType: false,
						processData: false,
						success: function(data) {

							let json = JSON.parse(data);
							var html =''
							if(json.status === true){

								$.confirm({
										title: 'บันทึก',
										content: 'สร้างโปรเจคสำเร็จ',
										autoClose: 'Close|2000',
										type: 'green',
										buttons: {
											Close: {}
										}
								});

								window.location.href = BASE_URL + 'basic-info-'+json.project_id;

							}
							$('.display_modal').html
							console.log(json.project_id)
						},
						error: function(data) {
							console.log("error : " + data);
						}
					});

				}else{
							$.confirm({
										title: 'ยกเลิก',
										content: 'คลิกยอมรับเงื่อนไขก่อนสร้างโครงการ',
										autoClose: 'Close|3000',
										type: 'red',
										buttons: {
											Close: {}
										}
								});


				}
						

			});
		
	});

</script>
</body>
</html>