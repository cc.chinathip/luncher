<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 <style>
	 .upload_image_file{
		/*  opacity:0; */
		display:none;
	 }
</style>
<body>
<div class="preload"></div>
<div class="page "> 
<?= $this->template->build('frontend/project/navigation'); ?>
<div class="page-slideout"> 
	<form class="section section-body pt-0" id="form-payment-project" method="post">
    <?= $this->template->build('frontend/project/project_header'); ?>
    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
		<div class="project-form-content wow fadeIn">
			<div class="container">

				<div class="form mw-600">
					<div class="form-hgroup">
						<h2 class="title-md text-center">เชื่อมโยงบัญชีธนาคาร</h2> 
						<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Molestie nibh faucibus tellus vivamus elit. Mi aliquet lacinia ut aliquet.</p> -->
					</div>
					<p><u><a target="_blank" href="<?= base_url('account/payment-methods.html?project_id='.$project_id) ?>">เพิ่มบัญชีธนาคาร</a></u></p>

					<input type="hidden" name="project_id" value="<?= $project_id ?>">

					

					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_fullname" placeholder="ชื่อ-นามสกุล" readonly>
					</div>
 
					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_card_id" placeholder="หมายเลขบัตรประชาชน" readonly>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_account_name" placeholder="ชื่อ-นามสกุลที่ปรากฏในบัญชีธนาคาร" readonly>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_number" placeholder="เลขทีบัญชี" readonly>
					</div>
                   
					<div class="input-block">
						<select name="member_bank_bank_id" class="form-control date">
                            <option>โปรดเลือกธนาคาร</option>
                            <?php foreach ($bank as $bank_oj) { ?>
										<option <?= $bank_oj->member_bank_main === '1' ? 'selected' : '' ?> value="<?= $bank_oj->member_bank_id ?>"><?= $bank_oj->bank_name ?></option>
								<?php } ?>
						</select> 
					</div>
				</div><!--form--> 

				<div class="buttons d-block d-lg-none">
					<button class="btn btn-save" type="submit">บันทึก</button>
				</div> 

			</div><!--container-->
		</div><!--project-form-content-->
	</form><!--section-body-->
 
	<div class="footer">
		<div class="footer-primary">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<h3 class="footer-title d-block d-md-none">หมวดหมู่</h3>
						<ul class="footer-links category">
							<li><a href="project.html">Comics & Illustration</a></li>
							<li><a href="project.html">Design & Tech</a></li>
							<li><a href="project.html">Food & Craft</a></li>
							<li><a href="project.html">Games</a></li>
							<li><a href="project.html">Music</a></li>
							<li><a href="project.html">Mobile App</a></li>
							<li><a href="project.html">Donation Project </a></li>
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3 col-6">
						<h3 class="footer-title">เกี่ยวกับเรา</h3>
						<ul class="footer-links">
							<li><a href="help.html">ศูนย์ความช่วยเหลือ</a></li>
							<li><a href="#">เงื่อนไขและความเป็นส่วนตัว</a></li>
							<li><a href="#">แผนผังเว็บไซต์</a></li> 
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3 col-6">
						<h3 class="footer-title">กฏระเบียบการใช้บริการ</h3>
						<ul class="footer-links">
							<li><a href="#">คู่มือการสร้างแคมเปญ</a></li>
							<li><a href="#">Trust & Safety</a></li>
							<li><a href="#">Cookie & Policy</a></li>
							<li><a href="#">Prohibited item สิ่งของต้องห้าม</a></li> 
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3">
						<div class="footer-followus">
							<div class="inner">
								<div class="followus">
									<a class="icons icon-facebook" href="#" target="_blank"></a>
									<a class="icons icon-instagram"  href="#" target="_blank"></a>
									<a class="icons icon-twitter"  href="#" target="_blank"></a>
									<a class="icons icon-youtube"  href="#" target="_blank"></a>
								</div>

								<div class="dropdown currency">
									<a data-toggle="dropdown" href="#">Currency Exchange</a>
									<div class="dropdown-menu">
										<p class="text-center mb-0">DATA</p>
									</div><!--dropdown-menu-->
								</div><!--dropdown-->
							</div><!--inner-->
						</div><!--footer-followus-->
					</div><!--col-md-3-->
				</div><!--row-->
			</div><!--container-->
		</div><!--footer-primary-->

		<div class="footer-secondary">
			© 2020 All rights reserved. <span class="nowrap">Powerde by <a href="https://gramickhouse.com/" target="_blank">Gramick House</a></span>
		</div><!--footer-secondary-->
	</div><!--footer-->
</div><!--page-slideout-->

<?= $this->template->build('frontend/project/search_project'); ?>
<?= $this->template->build('frontend/script'); ?>

<script>

		console.log('bank_main','<?= $bank_main ?>')

			var csrfName = '<?=$this->security->get_csrf_token_name();?>',
			csrfHash = '<?=$this->security->get_csrf_hash();?>';
			var dataJson = { [csrfName]: csrfHash, member_bank_id:$("select[name='member_bank_bank_id']").val() };
			console.log('member_bank_bank_id',dataJson)

			
			$.ajax({
				
                type: 'post',
                url: BASE_URL + 'frontend/project/project/bank_payment_project',
                data: dataJson,
                async:false,
                enctype: 'multipart/form-data',
                cache: false,
                success: function(data) {

					let json = JSON.parse(data);

                
					$('input[name="member_bank_fullname"]').val(json.member_bank_fullname)
					$('input[name="member_bank_card_id"]').val(json.member_bank_card_id)
					$('input[name="member_bank_account_name"]').val(json.member_bank_account_name)
					$('input[name="member_bank_number"]').val(json.member_bank_number)
                },
                error: function(data) {
                    console.log("error : " + data);
                }
            });


		$("select[name='member_bank_bank_id']").change(function(e){

			

			var csrfName = '<?=$this->security->get_csrf_token_name();?>',
			csrfHash = '<?=$this->security->get_csrf_hash();?>';
			

			var dataJson = { [csrfName]: csrfHash, member_bank_id:$(this).val() };


			console.log('member_bank_bank_id',dataJson)

			

			$.ajax({
				
                type: 'post',
                url: BASE_URL + 'frontend/project/project/bank_payment_project',
                data: dataJson,
                async:false,
                enctype: 'multipart/form-data',
                cache: false,
               /*  contentType: false,
                processData: false, */
                success: function(data) {

					let json = JSON.parse(data);

                   // console.log('select member bank_id',json.member_bank_id)

					$('input[name="member_bank_fullname"]').val(json.member_bank_fullname)
					$('input[name="member_bank_card_id"]').val(json.member_bank_card_id)
					$('input[name="member_bank_account_name"]').val(json.member_bank_account_name)
					$('input[name="member_bank_number"]').val(json.member_bank_number)
                },
                error: function(data) {
                    console.log("error : " + data);
                }
            });


		});
    /*------------[Start] bootstrap-datepicker.js ------------*/

	$('input.date').datepicker({
	    format: "dd/mm/yyyy",
	    todayHighlight: true
	});

	/*------------[Start] circle-progress.js ------------*/
	 
    $(window).on('load resize', function(){
    	var window_size = $(window).width();
    	var p_size = 40;
    	var p_thickness = 3;

	  	if (window_size > 1281) {
	     	var p_size = 70;
    		var p_thickness = 5;
  
	  	}else if (window_size > 768) {
	     	var p_size = 50;
    		var p_thickness = 3;
  
	  	} 

	 	$('.nav .progress-circle').each(function(){
	    	$(this).appear(function() {
		      $(this).find(".circle").circleProgress({
			    //value: .84,
			    fill: {color: '#21D695'},
			    emptyFill: '#E0E0E0',
			    startAngle: -Math.PI / 4 * 2,
			    thickness:p_thickness,
			    size:p_size,
			  }).on('circle-animation-progress', function(event, progress, stepValue) {
			   // $(this).find('strong').text(stepValue.toFixed(2).substr(2));
			  });
			}); 
		});
	});
    //$(function() { 
        $(".btn-save").click(function(e){

            //console.log('prj submit',$('input[name="project_date_start"]').val())
            e.preventDefault();

            var formData = new FormData($('#form-payment-project')[0]);
            console.log('formData',formData)

            $.ajax({
                type: 'post',
                url: BASE_URL + 'frontend/project/project/add_payment_project',
                data: formData,
                async:false,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {

                    let json = JSON.parse(data);
					
                    if(json.status === true){

                        $.confirm({
                                        title: 'บันทึก',
                                        content: 'เชื่อมโยงบัญชีธนาคารสำเร็จ',
                                        autoClose: 'Close|2000',
                                        type: 'green',
                                        buttons: {
                                            Close: {}
                                        }
                                });

                        window.location.href = BASE_URL + 'account.html';

                    }
                    console.log(json)
                },
                error: function(data) {
                    console.log("error : " + data);
                }
            });
        });

   // });

</script>
</body>
</html>