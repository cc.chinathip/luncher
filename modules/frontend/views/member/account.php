<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
 	
	<?= $this->template->build('frontend/member/header'); ?>
 	<div class="section section-column">
		<div class="container">

			<?= $this->template->build('frontend/member/navleft'); ?>

			<div class="content">
				<form class="form" id="form-update" method="post">
					<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
					<h3 class="title-xs">จัดการบัญชี</h3>

					<div class="row space-10 ">
						<div class="col-sm-12">
							<div class="input-block">
								<input type="text" class="form-control" name="member_username" value="<?= $account->member_username ?>" readonly placeholder="ชื่อผู้ใช้งาน" required>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="input-block">
								<input type="text" class="form-control" name="member_first_name" value="<?= $account->member_first_name ?>" placeholder="ชื่อจริง" required>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="input-block">
								<input type="text" class="form-control" name="member_last_name" value="<?= $account->member_last_name ?>" placeholder="นามสกุล" required>
							</div>
						</div>

						<div class="col-xl-6">
							<div class="input-block radio">
								<span class="input-text inline-block">เพศ</span>
								<div class="radio-group">
				                    <input id="male" value="1" name="member_gender" type="radio" <?= ($account->member_gender == 1) ? 'checked' : '' ?>> 
				                    <label for="male">ชาย</label>
				                </div>

				                <div class="radio-group">
				                    <input id="female" value="2" name="member_gender" type="radio" <?= ($account->member_gender == 2) ? 'checked' : '' ?>> 
				                    <label for="female">หญิง</label>
				                </div>

				                <div class="radio-group">
				                    <input id="other" value="3" name="member_gender" type="radio" <?= ($account->member_gender == 3) ? 'checked' : '' ?>> 
				                    <label for="other">ไม่ระบุ</label>
				                </div>
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="input-block">
								<input type="text" class="form-control" name="member_phone_number" value="<?= $account->member_phone_number ?>" placeholder="เบอร์โทรศัพท์" required>
							</div>
						</div>

						<div class="col-xl-4 col-sm-6">
							<div class="input-block has-icon right">
								<span class="icon"><span class="icons icon-calendar"></span></span>
								<input type="text" class="form-control calendar" name="member_date_open_project" value="<?= date_format(date_create($account->member_date_open_project),"d/m/Y") ?>" placeholder="เปิดโครงการ" required>
							</div>
						</div>

						<div class="col-xl-4 col-sm-6">
							<div class="input-block">
								<select class="form-control" name="member_country">
									<option value="">ประเทศ</option>
									<?php foreach ($country as $v) { ?>
										<option value="<?= $v->country_id ?>" <?= ($account->member_country == $v->country_id) ? 'selected' : ''?>><?= $v->country_name ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="input-block">
								<textarea class="form-control" placeholder="Biography" name="member_biography"><?= $account->member_biography ?></textarea>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="input-block">
								<input type="email" class="form-control" name="member_email" placeholder="อีเมล์" value="<?= $account->member_email ?>" required>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="input-block">
								<a class="forgotpass" href="<?= BASE_URL ?>forgot">เปลี่ยนรหัสผ่าน</a>
							</div>
						</div>

						
					</div><!--row-->

					<div class="buttons">
						<button class="btn btn-save w-150" type="submit">บันทึก</button>
					</div> 
				</form><!--form-->
			</div><!--content-->
		</div><!--container-->
	</div><!--section-column-->

	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
 	<script type="text/javascript">
	/*------------[Start] bootstrap-datepicker.js ------------*/

	$('input.calendar').datepicker({
	    format: "dd/mm/yyyy",
	    todayHighlight: true
	});

	$(function() { 
	  $("#form-update").validate({
	    
	    rules: { 
	      username: "required",
	      firstname: "required",
	      lastname: "required",
	      email: {
	        required: true, 
	        email: true
	      }
	    },
	    // Specify validation error messages
	    messages: {
	      username: "กรุณากรอกชื่อผู้ใช้งาน",
	      firstname: "กรุณากรอกชื่อจริง",
	      lastname: "กรุณากรอกนามสกุล",
	      email: "กรุณากรอกอีเมลให้ถูกต้อง"
	    },
	    // Make sure the form is submitted to the destination defined
	    // in the "action" attribute of the form when valid
	    submitHandler: function(form) {
	      	$.confirm({
	            title: 'ยืนยัน',
	            content: 'คุณต้องการแก้ไขข้อมูล ?',
	            buttons: {
	                confirm: function () {
	                    var formData = new FormData(form);
	      
	                    $.ajax({
	                        type: 'post',
	                        url: BASE_URL + 'account/account_update',
	                        data: formData,
	                        async:false,
	                        enctype: 'multipart/form-data',
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        success: function(data) {
	                          let json = JSON.parse(data);
	                          if (json.status) {
	                          	$.confirm({
								    title: 'บันทึก',
								    content: 'แก้ไขข้อมูลสำเร็จ',
								    autoClose: 'Close|2000',
								    type: 'green',
								    buttons: {
								        Close: {}
								    }
								});
	                          }
	                         
	                        },
	                        error: function(data) {
	                        	$.alert({
				                    title: '',
				                    content: 'เกิดข้อผิดพลาด',
				                    type: 'red',
				                });
	                        }
	                    });
	                },
	                cancel: function () {
	                }
	            }
	        });
	    }
	  });
	});

</script>
</body>
</html>