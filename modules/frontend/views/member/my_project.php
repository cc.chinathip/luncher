<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
     <?= $this->template->build('frontend/member/header'); ?>
     <form id="form-my-project" class="form" method="post">	 

 	<div class="section section-column" id="scoll-member">
		<div class="container">

			<?= $this->template->build('frontend/member/navleft'); ?>
			
			<div class="content">  
				<div class="form">
					<h3 class="title-xs">
						โครงการของฉัน
						<a class="btn btn-add w-auto" href="<?= BASE_URL ?>project-create">
							<span class="icons icon-plus"></span>
							เพิ่มโครงการ
						</a>
					</h3>
	 				<div class="table-responsive-lg">
	 					<table class="table table-hover myproject">
						  <thead>
						    <tr>
						      <th class="col-no">#</th>
						      <th class="col-info">ชื่อโครงการ</th>
						      <th class="col-sponsor">เงินทุนสนับสนุน</th>
						      <th class="col-datetime">วันที่สิ้นสุด</th>
						      <th></th>
						      <th></th>
						    </tr>
						  </thead>
						  <tbody>
						  	<?php
								foreach($project_list as $key => $project_list_oj){
							?>
						    <tr>
						    	<?php $donate_total = donate_total($project_list_oj->project_id,$project_list_oj->project_target); ?>
						      	<td class="col-no"><?= ($offset + $key+1) ?></td>
						      	<td class="col-info"><a href="<?= BASE_URL ?>project-detail/<?= $project_list_oj->project_id ?>" target="_blank"><?= $project_list_oj->project_name ?></a></td>
						      	<td class="col-sponsor">฿<?= number_format($donate_total['total']); ?></td>
						      	<td class="col-datetime"><?= date("l, d F Y", strtotime($project_list_oj->project_date_end)) ?></td>
						      	<td class="col-manage">
						      		<a class="item-manage green updateProject" href="javascript:void(0)" data-id="<?= $project_list_oj->project_id ?>">
						      			<img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-pencil-blue.svg"> อัพเดต
						      		</a>
						      	</td>
						      	<td class="col-manage"><a class="item-manage green" href="<?= BASE_URL ?>project-edit-<?= $project_list_oj->project_id ?>"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-pencil-blue.svg"> แก้ไข</a></td>
						    
						    </tr>
						    <?php
								}
							?>
						     
						  </tbody>
						</table>
	 				</div><!--table-responsive-->

	 				<?= $pagination; ?>

					<div class="clearfix"></div>
	 			</div><!--form-->
			</div><!--content-->
			
		</div><!--container-->
	</div><!--section-column-->
    </form>
	<?= $this->template->build('frontend/footer'); ?>

	<div class="modal modal-address fade" id="updateModal">
	  	<div class="modal-dialog">
	  		
	    	<div class="modal-content">
	    		<button class="btn btn-close" data-dismiss="modal"></button>
	    		<form id="form_update" class="form" method="post" style="padding: 30px;">
	    			<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
	    			<input type="hidden" name="project_id" id="project_id" value="">
					<h3 class="title text-left">รายละเอียด</h3> 
	 
					<div class="input-block">
						<textarea class="form-control texteditor" name="project_update_detail" required=""></textarea>
					</div>

					<div class="buttons text-right">
						<button class="btn btn-full w-150" type="submit">ยืนยัน</button>
					</div>
				</form> 
	        </div><!--modal-content-->
	    </div><!--modal-dialog-->
	</div><!--modal-->
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>

<script type="text/javascript">
	  $('html, body').animate({
    scrollTop: $("#scoll-member").offset().top - 140
  },1000);
	  
	$('.texteditor').summernote({
		callbacks: {
        	onImageUpload: function(image) {
            	uploadImage(image[0]);
	        }
	    }
	});

	function uploadImage(image) {
	    var data = new FormData();
	    var csrfName = '<?=$this->security->get_csrf_token_name();?>',
	    csrfHash = '<?=$this->security->get_csrf_hash();?>';

	    data.append(csrfName, csrfHash);
	    data.append("image", image);

	    $.ajax({
	        url: BASE_URL+'upload/image',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: data,
	        type: "post",
	        success: function(data) {
	        	let json = JSON.parse(data);
                if (json.status) {
	            	var image = $('<img>').attr('src', json.url);
		            $('.texteditor').summernote("insertNode", image[0]);
		        }
	        },
	        error: function(data) {
	            console.log(data);
	        }
	    });
	}
function delete_project(project_id){

  
    var csrfName = '<?=$this->security->get_csrf_token_name();?>',
    csrfHash = '<?=$this->security->get_csrf_hash();?>';

    var dataJson = { [csrfName]: csrfHash, project_id:project_id };

                    $.confirm({
							title: 'Confirm!',
							content: 'คุณจะยืนยันจะลบหรือไม่!',
							buttons: {
								confirm: function () {
									$.alert('Confirmed!');
									$.ajax({
										type: 'post',
										url: BASE_URL + 'frontend/project/project/delete_project',
										/* data: formData, */
                                        data:dataJson,
										async:false,
										enctype: 'multipart/form-data',
										cache: false,
										/* contentType: false,
										processData: false, */
										success: function(data) {
                                            console.log('data',data)

											let json = JSON.parse(data);

											if(json.status === true){

                                                $.confirm({
                                                        title: 'บันทึก',
                                                        content: 'ลบโปรเจคสำเร็จ',
                                                        autoClose: 'Close|2000',
                                                        type: 'green',
                                                        buttons: {
                                                            Close: {}
                                                        }
                                                });

                                                setTimeout(function(){ location.reload(); }, 3000);


											}

										
											console.log(json)
										},
										error: function(data) {
											console.log("error : " + data);
										}
									});
								},
								cancel: function () {
									$.alert('Canceled!');
								},
								
							}
					});


}
	$('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 

	$('.updateProject').click(function(e){
		var data_id = $(this).data('id')
		$('#project_id').val(data_id)
		$('#updateModal').modal('show');
	})

	$('#form_update').each(function() {  //	
	  $(this).validate({
	    
	    submitHandler: function(form) {
	      $.confirm({
            title: 'ยืนยัน',
            content: 'คุณต้องการบันทึกข้อมูล ?',
            buttons: {
                confirm: function () {
                    var formData = new FormData(form);
      
                    $.ajax({
                        type: 'post',
                        url: BASE_URL + 'my_project_update',
                        data: formData,
                        async:false,
                        enctype: 'multipart/form-data',
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                          let json = JSON.parse(data);
                          console.log(json)
                          if (json.status) {

                          	$('#form_update')[0].reset();
							$('#updateModal').modal('hide');   
							$.confirm({
							    title: 'บันทึก',
							    content: 'บันทึกข้อมูลสำเร็จ',
							    autoClose: 'Close|2000',
							    type: 'green',
							    buttons: {
							        Close: {}
							    }
							});   
                          }
                         
                        },
                        error: function(data) {
                        	$.alert({
			                    title: '',
			                    content: 'เกิดข้อผิดพลาด',
			                    type: 'red',
			                });
                        }
                    });
                },
                cancel: function () {
                }
            }
        });
	    }
	  });
	});
    
</script> 
</body>
</html>

