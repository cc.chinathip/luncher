<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
 	<?= $this->template->build('frontend/member/header'); ?>

 	<div class="section section-column" id="scoll-member">
		<div class="container">

			<?= $this->template->build('frontend/member/navleft'); ?>

			<div class="content">
				<h3 class="title-xs">การแจ้งเตือนบัญชี</h3>

				<div class="notification-box first">
					<h5>ความคิดเห็น</h5>
					<div class="row">
						<div class="noti-option">
							<div class="item email"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-email.svg" alt=""></div>
							<div class="item mobile"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-mobile.svg" alt=""></div>
						</div>
						<div class="col">คำตอบใหม่ในกระทู้ที่คุณใช้งานอยู่</div>
					</div><!--row-->
				</div><!--notification-box-->

				<div class="notification-box">
					<h5>ข้อความ</h5>
					<div class="row">
						<div class="noti-option">
							<div class="item email"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-email.svg" alt=""></div>
							<div class="item mobile"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-mobile.svg" alt=""></div>
						</div>
						<div class="col">แจ้งเตือนข้อความใหม่</div>
					</div><!--row-->
				</div><!--notification-box-->

				<div class="notification-box">
					<h5>โครงการที่คุณเปิดตัว</h5>
					<div class="row">
						<div class="noti-option">
							<div class="item email"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-email.svg" alt=""></div>
							<div class="item mobile"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-mobile.svg" alt=""></div>
						</div>
						<div class="col">สรุปกิจกรรมโครงการ</div>
					</div><!--row-->

					<div class="row mt-4">
						<div class="noti-option">
							<div class="item email"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-email.svg" alt=""></div>
							<div class="item mobile"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-mobile.svg" alt=""></div>
						</div>
						<div class="col">ความคิดเห็นใหม่</div>
					</div><!--row-->
				</div><!--notification-box-->

				<div class="buttons">
					<button class="btn w-150">บันทึก</button>
				</div>
			</div><!--content-->
		</div><!--container-->
	</div><!--section-column-->

	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
<script type="text/javascript">
	  $('html, body').animate({
	    scrollTop: $("#scoll-member").offset().top - 140
	  },1000);
	$('.noti-option .item').click(function(){
	    $(this).toggleClass("active");
	});
</script>
</body>
</html>