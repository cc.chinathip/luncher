<div class="sidebar" id="nav-left-member">
	<h3 class="title-sidebar d-block d-md-none" data-toggle="collapse" data-target="#collapseSidebar">เมนูย่อย <span class="icon"></span></h3>
	<div id="collapseSidebar" class="inner collapse">
		<ul class="nav-sidebar">
			<li class="<?= ($menu_left == 'account' || $menu_left == 'address') ? 'active' : '' ?> mt-0 mb-0">
				<a href="<?= BASE_URL ?>account.html">
					<span class="icon"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-pencil-blue.svg" alt=""></span>
					<span class="text">จัดการบัญชี</span>
				</a>
			</li>
			<li class="mt-md-2">
				<a class="pt-0" href="<?= BASE_URL ?>address.html">
					<span class="icon"></span>
					<span class="text">ที่อยู่</span>
				</a>
			</li>
			<li class="<?= ($menu_left == 'notification') ? 'active' : '' ?>">
				<a href="<?= BASE_URL ?>notification.html">
					<span class="icon"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-notification-blue.svg" alt=""></span>
					<span class="text">จัดการแจ้งเตือน</span>
				</a>
			</li>
			<li class="<?= ($menu_left == 'payment') ? 'active' : '' ?>">
				<a href="<?= BASE_URL ?>account/payment-methods.html">
					<span class="icon"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-card-blue.svg" alt=""></span>
					<span class="text">ช่องทางการชำระเงิน</span>
				</a>
			</li>
			<li class="<?= ($menu_left == 'favorite') ? 'active' : '' ?>">
				<a href="<?= BASE_URL ?>favorite">
					<span class="icon"><img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-favorite-blue.svg" alt=""></span>
					<span class="text">ที่ชื่นชอบ</span>
				</a>
			</li>
		</ul>
	</div><!--inner-->
</div><!--sidebar-->