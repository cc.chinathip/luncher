<div class="section secion-userpage-header">
	<div class="container">
		<div class="user-info-box">
			<div class="row space-10">
				<div class="col-12">
					<div class="user-info align-items-center">
						<div class="avatar">
							<div class="image-upload" id="imageHeader" style="background-image: url(<?= $this->session->userdata("laun_avatar") ?>);"></div>
							<button class="btn btn-upload">
								<input class="file-upload-input" style="display: none;" type='file' id="file_upload_avatar" onchange="readURLHeader(this);" accept="image/*" />
								<img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-camera.svg" alt="" onclick="_upload()">
							</button>
						</div>

						<div class="col" style="width: 50%;">
		 					<h2><?= $this->session->userdata("laun_username") ?></h2>
		 					<p>วันที่เข้าร่วม <?= date("d.m.Y", strtotime($this->session->userdata("laun_date_regis"))) ?></p>
		 				</div>
					</div><!--user-info-->
					
				</div>

				<div class="col-6">
					<a class="statistics-item" href="<?= BASE_URL ?>my-project">
						<p>โครงการของฉัน</p>
						<h3><?= account_project_count($this->session->userdata("laun_id")) ?></h3>
					</a>
				</div><!--col-sm-6-->
				<div class="col-6">
					<a class="statistics-item" href="<?= BASE_URL ?>sponsor">
						<p>โครงการที่สนับสนุน</p>
						<h3><?= account_donate_count($this->session->userdata("laun_id")) ?></h3>
					</a>
				</div><!--col-sm-6-->
			</div><!--row-->
		</div><!--user-info-box-->
	</div><!--container-->
</div><!--secion-userpage-header-->