<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
 	<?= $this->template->build('frontend/member/header'); ?>

 	<div class="section section-column" id="scoll-member">
		<div class="container">

			<?= $this->template->build('frontend/member/navleft'); ?>
			
			<div class="content">  
				<div class="form">
					<h3 class="title-xs">
						โครงการที่สนับสนุน
					</h3>
	 				<div class="table-responsive-lg">
	 					<table class="table table-hover sponsor">
						  <thead>
						    <tr>
						      <th class="col-no">#</th>
						      <th class="col-info">ชื่อโครงการ</th>
						      <th class="col-sponsor">เงินทุนสนับสนุน</th>
						      <th class="col-datetime">วันที่</th>
						    </tr>
						  </thead>
						  <tbody>
						  	<?php
								foreach($donate_list as $key => $v){
							?>
						    <tr>
						      <td class="col-no"><?= ($offset + $key+1) ?></td>
						      <td class="col-info"><?= $v->project_name ?></td>
						      <td class="col-sponsor">฿<?= number_format($v->donate_amount) ?></td>
						      <td class="col-datetime"><?= date("l, d F Y H:i", strtotime($v->donate_created_at)) ?></span></td>
						    </tr>
						    <?php
								}
							?>
						  </tbody>
						</table>
	 				</div><!--table-responsive-->

	 				<?= $pagination; ?>

					<div class="clearfix"></div>
	 			</div><!--form-->
			</div><!--content-->
			
		</div><!--container-->
	</div><!--section-column-->

	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script type="text/javascript">
	  $('html, body').animate({
    scrollTop: $("#scoll-member").offset().top - 140
  },1000);
	$('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 
</script> 
</body>
</html>