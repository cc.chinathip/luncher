<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 <style>
 .btn_back{
	margin-right: 210px;
 }
 </style>
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
 	<?= $this->template->build('frontend/member/header'); ?>

 	<div class="section section-column" id="scoll-member">
		<div class="container">

			<?= $this->template->build('frontend/member/navleft'); ?>

			<div class="content">
				<div class="form">
				
					<h3 class="title-xs has-line">
						บัญชีธนาคารของฉัน

			
						<button class="btn btn-add " data-toggle="modal" data-target="#addressModal">
							<span class="icons icon-plus"></span>
							เพิ่มบัญชีธนาคาร
						</button>

						<?php

							if(!empty($_GET['project_id'])){

								?>

								<a href="<?= base_url('payment-project-'.$_GET['project_id']) ?>"><button class="btn btn-add btn_back">
									<span class="icons icon-plus"></span>
									ย้อนกลับ
								</button></a>

								<?php

							}
 							

 						?>

						
					</h3>
					
					<?php foreach ($member_bank as $key => $v) { ?>
						<div class="bank-box <?= ($key == 0) ? 'first' : '' ?>" id="list_payment_<?= $v->member_bank_id ?>">
							<div class="row">
								<div class="col-lg-9">
									<table class="table-bank">
										<tr>
											<td class="col-info">
												<p><?= $v->bank_name ?>
													<span id="text_main_<?= $v->member_bank_id ?>" class="text_main">
														<?php if ($v->member_bank_main == 1) { ?>
															<span class="pin">บัญชีหลัก</span>
														<?php } ?>
													</span>
												</p>
												<p>ชื่อนามสกุล : <?= $v->member_bank_fullname ?></p>
											</td>
											<td class="col-code"><?= $v->member_bank_number ?></td>
										</tr>
									</table>
								</div><!--col-lg-9-->
								<div class="col-lg-3">
									<div class="bank-setting">
										<a class="delete delete_payment" href="javascript:void(0)" data-id="<?= $v->member_bank_id ?>">
											<img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-bin2.svg" alt="">
											ลบ
										</a>
										<button class="btn btn-outline btn-xs set_main <?= ($v->member_bank_main == 1) ? 'disabled' : '' ?> btn-main-bank" id="btn_main_<?= $v->member_bank_id ?>" data-id="<?= $v->member_bank_id ?>">ตั้งเป็นบัญชีหลัก</button>
									</div><!--bank-setting-->
								</div><!--col-lg-3-->
							</div><!--row-->
						</div><!--bank-box-->
					<?php } ?>
				</div><!--form-->
			</div><!--content-->
		</div><!--container-->
	</div><!--section-column-->

	<?= $this->template->build('frontend/footer'); ?>
	
	<div class="modal modal-address fade" id="addressModal">
	  	<div class="modal-dialog modal-dialog-centered">
	  		
	    	<div class="modal-content">
	    		<button class="btn btn-close" data-dismiss="modal"></button>
	    		<form id="form_add" class="form mw-600">
	    			<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
					<h3 class="title">เพิ่มบัญชีธนาคาร</h3> 
	 
					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_fullname" placeholder="ชื่อ-นามสกุล" required>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_card_id" placeholder="หมายเลขบัตรประชาชน" required>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_account_name" placeholder="ชื่อ-นามสกุลที่ปรากฏในบัญชีธนาคาร" required>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_bank_number" placeholder="เลขทีบัญชี" required>
					</div>

					<div class="input-block">
						<select name="member_bank_bank_id" class="form-control required" required>
							<option value="">โปรดเลือกธนาคาร</option>
							<?php foreach ($bank as $key => $v) { ?>
								<option value="<?= $v->bank_id ?>"><?= $v->bank_name ?></option>
							<?php } ?>
						</select>
					</div> 

					<div class="buttons">
						<button class="btn btn-full w-150" type="submit">ยืนยัน</button>
					</div>
				</form> 
	        </div><!--modal-content-->
	    </div><!--modal-dialog-->
	</div><!--modal-->
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 

<script type="text/javascript">
	  $('html, body').animate({
    scrollTop: $("#scoll-member").offset().top - 140
  },1000);
	$('.noti-option .item').click(function(){
	    $(this).toggleClass("active");
	});

	/*------------[Start] jquery.validate.js ------------*/

	$(function() { 
		
		
        
		$('.set_main').click(function(e){
			let data_id = $(this).data('id')

			$.ajax({
                type: 'get',
                url: BASE_URL + 'account/set_mai_payment/'+data_id,
                async:false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                  let json = JSON.parse(data);
                  if (json.status) {
                  	$('.text_main').html('')
                  	$('#text_main_'+data_id).html('<span class="pin">บัญชีหลัก</span>')
                  	$('.btn-main-bank').removeClass('disabled')
                  	$('#btn_main_'+data_id).addClass('disabled')
                  }
                 
                },
                error: function(data) {
                	
                }
            });
		})

		$('#form_add').each(function() {  //	
		  $(this).validate({
		    
		   
		    submitHandler: function(form) {
		      $.confirm({
	            title: 'ยืนยัน',
	            content: 'คุณต้องการเพิ่มข้อมูล ?',
	            buttons: {
	                confirm: function () {
	                    var formData = new FormData(form);
	      
	                    $.ajax({
	                        type: 'post',
	                        url: BASE_URL + 'account/payment_add',
	                        data: formData,
	                        async:false,
	                        enctype: 'multipart/form-data',
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        success: function(data) {
	                          let json = JSON.parse(data);
	                          if (json.status) {

	                          	$('#form_add')[0].reset();
	                          	window.location.href = BASE_URL + 'account/payment-methods.html';
	      
	                          }
	                         
	                        },
	                        error: function(data) {
	                        	$.alert({
				                    title: '',
				                    content: 'เกิดข้อผิดพลาด',
				                    type: 'red',
				                });
	                        }
	                    });
	                },
	                cancel: function () {
	                }
	            }
	        });
		    }
		  });
		});

		$('.delete_payment').click(function(e){
			let data_id = $(this).data('id')
			$.confirm({
	            title: 'ยืนยัน',
	            content: 'คุณต้องการลบข้อมูล ?',
	            buttons: {
	                confirm: function () {
	                    $('#list_payment_'+data_id).remove()
							$.ajax({
				                type: 'get',
				                url: BASE_URL + 'account/delete_payment/'+data_id,
				                async:false,
				                cache: false,
				                contentType: false,
				                processData: false,
				                success: function(data) {
				                  let json = JSON.parse(data);
				                  if (json.status) {
				                  
				                  }
				                 
				                },
				                error: function(data) {
				                	
				                }
				        });
	                },
	                cancel: function () {
	                }
	            }
	        });
			
			// console.log(data_id)
		})
	});
</script>
</body>
</html>