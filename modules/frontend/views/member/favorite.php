<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
 	<?= $this->template->build('frontend/member/header'); ?>

 	<div class="section section-column" id="scoll-member">
		<div class="container">

			<?= $this->template->build('frontend/member/navleft'); ?>

			<div class="content">
				<div class="favorite-wrap">
 					<div class="row">
 					
						<?php
						foreach($project_favor as $project_favor_oj){
							?>
 						<div class="col-lg-4 col-sm-6">
 							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="<?= BASE_URL ?>project-detail/<?= $project_favor_oj->project_id ?>" style="background-image: url(<?= BASE_URL ?>/uploads/project/<?= $project_favor_oj->project_image ?>);">
										<img src="<?= BASE_URL ?>asset/launcher/img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite <?= (($this->session->userdata("laun_logged"))) ? check_favourite($project_favor_oj->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_favor_oj->project_id ?>" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like <?= (($this->session->userdata("laun_logged"))) ? check_like($project_favor_oj->project_id,$this->session->userdata("laun_id")) : 'goto_login' ?> " data-id="<?= $project_favor_oj->project_id ?>" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share" data-sharer="facebook" data-hashtag="hashtag" data-url="<?= BASE_URL ?>project-detail/<?= $project_favor_oj->project_id ?>"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="#" style="background-image: url(<?= BASE_URL ?>asset/launcher/img/thumb/avatar--3.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="<?= BASE_URL ?>project-detail/<?= $project_favor_oj->project_id ?>"><?= $project_favor_oj->project_description ?></a></h3>
											<div class="post-info">
											<div class="author">by <a href="#"><?= $project_favor_oj->member_username ?></a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box dark">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
 						</div><!--col-lg-4 col-sm-6-->
						 <?php
						}
						?>



 					</div><!--row-->
 				</div><!--favorite-wrap-->
			</div><!--content-->
		</div><!--container-->
	</div><!--section-column-->

	<?= $this->template->build('frontend/footer'); ?>
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script type="text/javascript">
	  $('html, body').animate({
	    scrollTop: $("#scoll-member").offset().top - 140
	  },1000);
	$('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 
</script> 
</body>
</html>