<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
	<style type="text/css">
		.article.address:before{
			border: none;
		}
		.address-empty{
			text-align: center;
			padding: 30px;
		}
	</style>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 

<?= $this->template->build('frontend/navigation'); ?>

<div class="page-slideout">
 	<?= $this->template->build('frontend/member/header'); ?>

 	<div class="section section-column" id="scoll-member">
		<div class="container">

			<?= $this->template->build('frontend/member/navleft'); ?>

			<div class="content">
				<div class="form">
					<h3 class="title-xs has-line">
						จัดการบัญชี

						<button class="btn btn-add w-150" data-toggle="modal" data-target="#addressModal">
							<span class="icons icon-plus"></span>
							เพิ่มที่อยู่
						</button>
					</h3>

					<?php if (isset($member_address) && !empty($member_address)) { ?>
						<?php foreach ($member_address as $key => $v) { ?>
						<div class="address-box <?= ($key == 0) ? 'first' : '' ?>" id="list_address_<?= $v->member_address_id ?>">
							<div class="row">
								<div class="col-lg-8">
									<table>
										<tr>
											<th>ชื่อ-สกุล</th>
											<td><?= $v->member_address_fullname ?> 
											<span id="text_main_<?= $v->member_address_id ?>" class="text_main">
												<?php if ($v->member_address_main == 1) { ?>
													<span class="pin">ที่อยู่หลัก</span>
												<?php } ?>
											</span>
											</td>
											
										</tr>
										<tr>
											<th>โทรศัพท์</th>
											<td><?= $v->member_address_phone_number ?></td>
										</tr>
										<tr>
											<th>ที่อยู่</th>
											<td>
												<div class="mw-140px">
													<?= $v->member_address_address ?> <?= $v->tambon_name_th ?>
													<?= $v->amphoe_name_th ?> 
													<?= $v->province_name_th ?> 
													<?= $v->tambon_zipcode ?>
												</div>
											</td>
										</tr>
									</table>
								</div>
								<div class="col-lg-4">
									<div class="address-setting">
										<div class="row space-0 align-items-center">
											<div class="col-lg-12 col-8">
												<div class="d-flex">
													<a class="item edit addressEdit" href="javascript:void(0)" data-id="<?= $v->member_address_id ?>">
														<img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-edit.svg" alt="">
														แก้ไข
													</a>

													<a class="item delete delete_address" href="javascript:void(0)" data-id="<?= $v->member_address_id ?>">
														<img class="svg-js" src="<?= BASE_URL ?>asset/launcher/img/icons/icon-bin2.svg" alt="">
														ลบ
													</a>
												</div>
											</div><!--col-lg-12-->
											 
											<div class="col-lg-12 col-4">
												<button class="btn btn-outline btn-xs set_main <?= ($v->member_address_main == 1) ? 'disabled' : '' ?> btn-main-address" id="btn_main_<?= $v->member_address_id ?>" data-id="<?= $v->member_address_id ?>">ตั้งเป็นที่อยู่หลัก</button>
											</div><!--col-lg-12-->
										</div><!--row-->
									</div>
								</div>
							</div><!--row-->
						</div><!--address-box-->
						<?php } ?>
					<?php }else{ ?>
						<div class="address-empty">
							<div class="row">
								<div class="col-lg-12">
									<p class="text-center">ไม่พอข้อมูล</p>
								</div>
							</div>
						</div>		
					<?php } ?>
					 
				</div><!--form-->

				<div class="article address">
					 
					<h5>กรุณาสแกนส่งเอกสารเพื่อใช้ในการอนุมัติโครงการดังนี้</h5>
					<ul>
						<li>สำเนาบัตรประชาชน หรือพาสพอร์ท หรือ หนังสือรับรองนิติบุคคล พร้อมลายเซ็นรับรองสำเนา (หากเป็นนิติบุคคล ต้องส่งสำเนาบัตรประชาชนคู่กัน)</li>
						<li>สำเนาทำเบียนบ้าน พร้อมลายเซ็นรับรองสำเนา</li>
						<li>หน้าแรกสมุดบัญชีธนาคาร (เพื่อใช้ในการโอนรับเงิน)</li>
						<li>หนังสือรับรองบัญชีจากธนาคาร</li>
					</ul>

					<p>กรุณาส่งเอกสารขออนุมัติได้ที่ <a class="regular" href="mailto:launcher.project@gmail.com">launcher.project@gmail.com</a></p>
				</div>
			</div><!--content-->
		</div><!--container-->
	</div><!--section-column-->

	<?= $this->template->build('frontend/footer'); ?>
	
	<div class="modal modal-address fade" id="addressModal">
	  	<div class="modal-dialog modal-dialog-centered">
	  		
	    	<div class="modal-content">
	    		<button class="btn btn-close" data-dismiss="modal"></button>
	    		<form id="form_add" class="form mw-600" action="#" method="post">
	    			<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
					<h3 class="title">เพิ่มที่อยู่</h3> 
	 
					<div class="input-block">
						<input type="text" class="form-control" name="member_address_fullname" placeholder="ชื่อ-สกุล" required="">
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_address_phone_number" placeholder="หมายเลขโทรศัพท์" required="">
					</div>

					<div class="input-block">
						<select name="member_address_province" class="form-control custom-select" id="member_address_province" required>
							<option value="">จังหวัด</option>
							<?php foreach ($province as $key => $v) { ?>
								<option value="<?= $v->province_id ?>"><?= $v->province_name_th ?></option>
							<?php } ?>
							
						</select>
					</div>

					<div class="input-block">
						<select name="member_address_amphoe" id="member_address_amphoe" class="form-control custom-select required" required>
							<option>เขต/อำเภอ</option>
						</select>
					</div>

					<div class="input-block">
						<select name="member_address_tambon" id="member_address_tambon" class="form-control custom-select required" required>
							<option>แขวง/ตำบล</option>
						</select>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_address_postcode" id="member_address_postcode" placeholder="รหัสไปรษณีย์" class="required" required>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_address_address" id="member_address_address" placeholder="อาคาร , ถนน และอื่นๆ">
					</div>
					
					 
					<div class="input-block">
						<div class="checkbox-group">
		                    <input id="member_address_main" value="1" name="member_address_main" type="checkbox" checked=""> 
		                    <label for="member_address_main">เลือกเป็นที่อยู่หลัก</label>
		                </div>
					</div>

					<div class="buttons">
						<button class="btn btn-full w-150" type="submit">ยืนยัน</button>
					</div>
				</form> 
	        </div><!--modal-content-->
	    </div><!--modal-dialog-->
	</div><!--modal-->

	<div class="modal modal-address fade" id="addressEditModal">
	  	<div class="modal-dialog modal-dialog-centered">
	  		
	    	<div class="modal-content">
	    		<button class="btn btn-close" data-dismiss="modal"></button>
	    		<form id="form_edit" class="form mw-600" action="#" method="post">
	    			<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
	    			<input type="hidden" class="form-control" name="member_address_edit_id" id="member_address_edit_id" required="">
					<h3 class="title">แก้ไขที่อยู่</h3> 
	 
					<div class="input-block">
						<input type="text" class="form-control" name="member_address_edit_fullname" id="member_address_edit_fullname" placeholder="ชื่อ-สกุล" required="">
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_address_edit_phone_number" id="member_address_edit_phone_number" placeholder="หมายเลขโทรศัพท์" required="">
					</div>

					<div class="input-block">
						<select name="member_address_edit_province" class="form-control custom-select" id="member_address_edit_province" required>
							<option value="">จังหวัด</option>
							<?php foreach ($province as $key => $v) { ?>
								<option value="<?= $v->province_id ?>"><?= $v->province_name_th ?></option>
							<?php } ?>
							
						</select>
					</div>

					<div class="input-block">
						<select name="member_address_edit_amphoe" id="member_address_edit_amphoe" class="form-control custom-select required" required>
							<option>เขต/อำเภอ</option>
						</select>
					</div>

					<div class="input-block">
						<select name="member_address_edit_tambon" id="member_address_edit_tambon" class="form-control custom-select required" required>
							<option>แขวง/ตำบล</option>
						</select>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_address_edit_postcode" id="member_address_edit_postcode" placeholder="รหัสไปรษณีย์" class="required" required>
					</div>

					<div class="input-block">
						<input type="text" class="form-control" name="member_address_edit_address" id="member_address_edit_address" placeholder="อาคาร , ถนน และอื่นๆ">
					</div>
					
					 
					<div class="input-block">
						<div class="checkbox-group">
		                    <input id="member_address_edit_main" value="1" name="member_address_edit_main" type="checkbox"> 
		                    <label for="member_address_edit_main">เลือกเป็นที่อยู่หลัก</label>
		                </div>
					</div>

					<div class="buttons">
						<button class="btn btn-full w-150" type="submit">ยืนยัน</button>
					</div>
				</form> 
	        </div><!--modal-content-->
	    </div><!--modal-dialog-->
	</div><!--modal-->
</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
<script type="text/javascript">
	/*------------[Start] bootstrap-datepicker.js ------------*/
	  $('html, body').animate({
	    scrollTop: $("#scoll-member").offset().top - 140
	  },1000);
	$('input.date').datepicker({
	    format: "dd/mm/yyyy",
	    todayHighlight: true
	});

	/*------------[Start] jquery.validate.js ------------*/

	$(function() { 

		$('.set_main').click(function(e){
			let data_id = $(this).data('id')

			$.ajax({
                type: 'get',
                url: BASE_URL + 'address/set_main/'+data_id,
                async:false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                  let json = JSON.parse(data);
                  if (json.status) {
                  	$('.text_main').html('')
                  	$('#text_main_'+data_id).html('<span class="pin">ที่อยู่หลัก</span>')
                  	$('.btn-main-address').removeClass('disabled')
                  	$('#btn_main_'+data_id).addClass('disabled')
                  }
                 
                },
                error: function(data) {
                	
                }
            });
		})

		$('.delete_address').click(function(e){
			let data_id = $(this).data('id')
			$.confirm({
	            title: 'ยืนยัน',
	            content: 'คุณต้องการลบข้อมูล ?',
	            buttons: {
	                confirm: function () {
	                    $('#list_address_'+data_id).remove()
							$.ajax({
				                type: 'get',
				                url: BASE_URL + 'address/delete_address/'+data_id,
				                async:false,
				                cache: false,
				                contentType: false,
				                processData: false,
				                success: function(data) {
				                  let json = JSON.parse(data);
				                  if (json.status) {
				                  
				                  }
				                 
				                },
				                error: function(data) {
				                	
				                }
				        });
	                },
	                cancel: function () {
	                }
	            }
	        });
			
			// console.log(data_id)
		})

		$('.addressEdit').click(function(e){
			let data_id = $(this).data('id')
			
			$('#form_edit')[0].reset();
			$('#member_address_edit_id').val(data_id);

			request = $.ajax({
	              type: 'get',
	                url: BASE_URL + 'address/get_data/'+data_id
	          })
	          .done()
	          .fail(function (jqXHR, textStatus, errorThrown){
	              console.error(
	                  "The following error occurred: "+
	                  textStatus, errorThrown
	              );
	          })
	          .always(function (res,status, jqXHR) {
	              var json = $.parseJSON(res);
	              
	              if (json.status) {
	              	$('#member_address_edit_fullname').val(json.data['member_address'].member_address_fullname)
	              	$('#member_address_edit_phone_number').val(json.data['member_address'].member_address_phone_number)
	              	$('#member_address_edit_address').val(json.data['member_address'].member_address_address)
	              	$('#member_address_edit_postcode').val(json.data['member_address'].member_address_postcode)

	              	if(json.data['member_address'].member_address_main == 1){
	              		$('#member_address_edit_main').attr('checked',true)
	              	}else{
	              		$('#member_address_edit_main').attr('checked',false)
	              	}
	              		

	              	$('#member_address_province').find('option').remove().end();
	              	$('#member_address_edit_province')
	                  .append($("<option></option>")
	                              .attr("value",'')
	                              .text('เลือกจังหวัด')); 
		              $.each(json.data['province'], function(key, value) { 
		               	let province_selected = false
		              	if (json.data['member_address'].member_address_province == value.province_id) {
		              		province_selected = true
		              	}
		                  $('#member_address_edit_province')
		                      .append($("<option></option>")
		                                  .attr("value",value.province_id)
		                                  .attr("selected",province_selected)
		                                  .text(value.province_name_th)); 
		              });

	              	$('#member_address_edit_amphoe').find('option').remove().end();
	              		$('#member_address_edit_amphoe')
	                  	.append($("<option></option>")
	                              .attr("value",'')
	                              .text('เลือกเขต/อำเภอ')); 
		              	$.each(json.data['amphoe'], function(key, value) {   
		              		let amphoe_selected = false
			              	if (json.data['member_address'].member_address_amphoe == value.amphoe_id) {
			              		amphoe_selected = true
			              	}
		                  	$('#member_address_edit_amphoe')
		                      .append($("<option></option>")
		                                  .attr("value",value.amphoe_id)
		                                  .attr("selected",amphoe_selected)
		                                  .text(value.amphoe_name_th)); 
		              	});

		            $('#member_address_edit_tambon').find('option').remove().end();
	              	$('#member_address_edit_tambon')
	                  .append($("<option></option>")
	                              .attr("value",'')
	                              .text('เลือกแขวง/ตำบล')); 
		              $.each(json.data['tambon'], function(key, value) {   
		              		let tambon_selected = false
			              	if (json.data['member_address'].member_address_tambon == value.tambon_id) {
			              		tambon_selected = true
			              	}
		                  $('#member_address_edit_tambon')
		                      .append($("<option></option>")
		                                  .attr("value",value.tambon_id)
		                                  .attr("selected",tambon_selected)
		                                  .text(value.tambon_name_th)); 
		              });  
	              }
	              
	          });
			$('#addressEditModal').modal('show');
		})

		$("#member_address_edit_province").change(function(e){
	        $('#member_address_edit_amphoe').find('option').remove().end();
	        $('#member_address_edit_tambon').find('option').remove().end();
	        $('#member_address_edit_postcode').val('');

	        $('#member_address_edit_tambon').prop("disabled", true);
	        $('#member_address_edit_postcode').prop("disabled", true);
	        if ($(this).val() == '') {
	          $('#member_address_edit_amphoe').prop("disabled", true);
	          $('#member_address_edit_amphoe')
	            .append($("<option></option>")
	                    .attr("value",'')
	                    .text('เลือกเขต/อำเภอ')); 
	          $('#member_address_edit_tambon')
	            .append($("<option></option>")
	                    .attr("value",'')
	                    .text('เลือกแขวง/ตำบล')); 
	        }else{
	          $('#member_address_edit_tambon')
	            .append($("<option></option>")
	                    .attr("value",'')
	                    .text('เลือกแขวง/ตำบล')); 
	          request = $.ajax({
	              type: 'get',
	                url: BASE_URL + 'address/get_amphoe/'+$(this).val()
	          })
	          .done()
	          .fail(function (jqXHR, textStatus, errorThrown){
	              console.error(
	                  "The following error occurred: "+
	                  textStatus, errorThrown
	              );
	          })
	          .always(function (res,status, jqXHR) {
	              var json = $.parseJSON(res);

	              $('#member_address_edit_amphoe')
	                  .append($("<option></option>")
	                              .attr("value",'')
	                              .text('เลือกเขต/อำเภอ')); 
	              $.each(json.data.amphoe, function(key, value) {   
	                  $('#member_address_edit_amphoe')
	                      .append($("<option></option>")
	                                  .attr("value",value.amphoe_id)
	                                  .text(value.amphoe_name_th)); 
	              });
	              $('#member_address_edit_amphoe').prop("disabled", false);
	          });
	        }
	      });

      $("#member_address_edit_amphoe").change(function(e){

        $('#member_address_edit_tambon').find('option').remove().end();
        $('#member_address_edit_postcode').val('');
        $('#member_address_edit_postcode').prop("disabled", true);
        if ($(this).val() == '') {
          $('#member_address_edit_tambon').prop("disabled", true);
              $('#member_address_edit_tambon')
                  .append($("<option></option>")
                              .attr("value",'')
                              .text('เลือกแขวง/ตำบล')); 
          }else{
            request = $.ajax({
                type: 'get',
                url: BASE_URL + 'address/get_tambon/'+$(this).val()
            })
            .done()
            .fail(function (jqXHR, textStatus, errorThrown){
                console.error(
                    "The following error occurred: "+
                    textStatus, errorThrown
                );
            })
            .always(function (res,status, jqXHR) {
                var json = $.parseJSON(res);
                $('#member_address_edit_tambon')
                    .append($("<option></option>")
                                .attr("value",'')
                                .text('เลือกแขวง/ตำบล')); 
                $.each(json.data.tambon, function(key, value) {   
                $('#member_address_edit_tambon')
                    .append($("<option></option>")
                                .attr("value",value.tambon_id)
                                .text(value.tambon_name_th)); 
            });
            $('#member_address_edit_tambon').prop("disabled", false);
          });
        }


      });

      $("#member_address_edit_tambon").change(function(e){

        $('#member_address_edit_postcode').val('');
        if ($(this).val() == '') {
          $('#member_address_edit_tambon').prop("disabled", true);
        }else{
          request = $.ajax({
              type: 'get',
                url: BASE_URL + 'address/get_zipcode/'+$(this).val()
          })
          .done()
          .fail(function (jqXHR, textStatus, errorThrown){
              console.error(
                  "The following error occurred: "+
                  textStatus, errorThrown
              );
          })
          .always(function (res,status, jqXHR) {
              var json = $.parseJSON(res);
              $('#member_address_edit_postcode').val(json.data.zipcode.tambon_zipcode);
              $('#member_address_edit_postcode').prop("disabled", false);
          });
        }

      });

		$("#member_address_province").change(function(e){
	        $('#member_address_amphoe').find('option').remove().end();
	        $('#member_address_tambon').find('option').remove().end();
	        $('#member_address_postcode').val('');

	        $('#member_address_tambon').prop("disabled", true);
	        $('#member_address_postcode').prop("disabled", true);
	        if ($(this).val() == '') {
	          $('#member_address_amphoe').prop("disabled", true);
	          $('#member_address_amphoe')
	            .append($("<option></option>")
	                    .attr("value",'')
	                    .text('เลือกเขต/อำเภอ')); 
	          $('#member_address_tambon')
	            .append($("<option></option>")
	                    .attr("value",'')
	                    .text('เลือกแขวง/ตำบล')); 
	        }else{
	          $('#member_address_tambon')
	            .append($("<option></option>")
	                    .attr("value",'')
	                    .text('เลือกแขวง/ตำบล')); 
	          request = $.ajax({
	              type: 'get',
	                url: BASE_URL + 'address/get_amphoe/'+$(this).val()
	          })
	          .done()
	          .fail(function (jqXHR, textStatus, errorThrown){
	              console.error(
	                  "The following error occurred: "+
	                  textStatus, errorThrown
	              );
	          })
	          .always(function (res,status, jqXHR) {
	              var json = $.parseJSON(res);

	              $('#member_address_amphoe')
	                  .append($("<option></option>")
	                              .attr("value",'')
	                              .text('เลือกเขต/อำเภอ')); 
	              $.each(json.data.amphoe, function(key, value) {   
	                  $('#member_address_amphoe')
	                      .append($("<option></option>")
	                                  .attr("value",value.amphoe_id)
	                                  .text(value.amphoe_name_th)); 
	              });
	              $('#member_address_amphoe').prop("disabled", false);
	          });
	        }
	      });

      $("#member_address_amphoe").change(function(e){

        $('#member_address_tambon').find('option').remove().end();
        $('#member_address_postcode').val('');
        $('#member_address_postcode').prop("disabled", true);
        if ($(this).val() == '') {
          $('#member_address_tambon').prop("disabled", true);
              $('#member_address_tambon')
                  .append($("<option></option>")
                              .attr("value",'')
                              .text('เลือกแขวง/ตำบล')); 
          }else{
            request = $.ajax({
                type: 'get',
                url: BASE_URL + 'address/get_tambon/'+$(this).val()
            })
            .done()
            .fail(function (jqXHR, textStatus, errorThrown){
                console.error(
                    "The following error occurred: "+
                    textStatus, errorThrown
                );
            })
            .always(function (res,status, jqXHR) {
                var json = $.parseJSON(res);
                $('#member_address_tambon')
                    .append($("<option></option>")
                                .attr("value",'')
                                .text('เลือกแขวง/ตำบล')); 
                $.each(json.data.tambon, function(key, value) {   
                $('#member_address_tambon')
                    .append($("<option></option>")
                                .attr("value",value.tambon_id)
                                .text(value.tambon_name_th)); 
            });
            $('#member_address_tambon').prop("disabled", false);
          });
        }


      });

      $("#member_address_tambon").change(function(e){

        $('#member_address_postcode').val('');
        if ($(this).val() == '') {
          $('#member_address_tambon').prop("disabled", true);
        }else{
          request = $.ajax({
              type: 'get',
                url: BASE_URL + 'address/get_zipcode/'+$(this).val()
          })
          .done()
          .fail(function (jqXHR, textStatus, errorThrown){
              console.error(
                  "The following error occurred: "+
                  textStatus, errorThrown
              );
          })
          .always(function (res,status, jqXHR) {
              var json = $.parseJSON(res);
              $('#member_address_postcode').val(json.data.zipcode.tambon_zipcode);
              $('#member_address_postcode').prop("disabled", false);
          });
        }

      });

		$('#form_add').each(function() {  //	
		  $(this).validate({
		    
		   
		    submitHandler: function(form) {
		      $.confirm({
	            title: 'ยืนยัน',
	            content: 'คุณต้องการเพิ่มข้อมูล ?',
	            buttons: {
	                confirm: function () {
	                    var formData = new FormData(form);
	      
	                    $.ajax({
	                        type: 'post',
	                        url: BASE_URL + 'address/address_add',
	                        data: formData,
	                        async:false,
	                        enctype: 'multipart/form-data',
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        success: function(data) {
	                          let json = JSON.parse(data);
	                          if (json.status) {

	                          	$('#form_add')[0].reset();
	                          	window.location.href = BASE_URL + 'address.html';
	      
	                          }
	                         
	                        },
	                        error: function(data) {
	                        	$.alert({
				                    title: '',
				                    content: 'เกิดข้อผิดพลาด',
				                    type: 'red',
				                });
	                        }
	                    });
	                },
	                cancel: function () {
	                }
	            }
	        });
		    }
		  });
		});

		$('#form_edit').each(function() {  //	
		  $(this).validate({
		    
		   
		    submitHandler: function(form) {
		      $.confirm({
	            title: 'ยืนยัน',
	            content: 'คุณต้องการแก้ไขข้อมูล ?',
	            buttons: {
	                confirm: function () {
	                    var formData = new FormData(form);
	      
	                    $.ajax({
	                        type: 'post',
	                        url: BASE_URL + 'address/address_edit',
	                        data: formData,
	                        async:false,
	                        enctype: 'multipart/form-data',
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        success: function(data) {
	                          let json = JSON.parse(data);
	                          if (json.status) {

	                          	$('#form_edit')[0].reset();
	                          	window.location.href = BASE_URL + 'address.html';
	      
	                          }
	                         
	                        },
	                        error: function(data) {
	                        	$.alert({
				                    title: '',
				                    content: 'เกิดข้อผิดพลาด',
				                    type: 'red',
				                });
	                        }
	                    });
	                },
	                cancel: function () {
	                }
	            }
	        });
		    }
		  });
		});
	});


</script>
</body>
</html>