<div class="footer">
	<div class="footer-primary">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h3 class="footer-title d-block d-md-none">หมวดหมู่</h3>
					<ul class="footer-links category">
						<?php $menu_category = menu_category(); ?>
			        	<?php foreach ($menu_category as $vm) { ?>
			        		<li><a href="<?= BASE_URL ?>project-list?category=<?= $vm->project_category_id ?>"><?= $vm->project_category_description_name ?></a></li>
			        	<?php } ?>
					</ul>
				</div><!--col-md-3-->

				<div class="col-md-3 col-6">
					<h3 class="footer-title">เกี่ยวกับเรา</h3>
					<ul class="footer-links">
						<li><a href="help.html">ศูนย์ความช่วยเหลือ</a></li>
						<li><a href="<?= BASE_URL ?>terms.html">เงื่อนไขและความเป็นส่วนตัว</a></li>
						<li><a href="#">แผนผังเว็บไซต์</a></li> 
					</ul>
				</div><!--col-md-3-->

				<div class="col-md-3 col-6">
					<h3 class="footer-title">กฏระเบียบการใช้บริการ</h3>
					<ul class="footer-links">
						<li><a href="<?= BASE_URL ?>guide.html">คู่มือการสร้างแคมเปญ</a></li>
						<li><a href="<?= BASE_URL ?>trust.html">Trust & Safety</a></li>
						<li><a href="<?= BASE_URL ?>policy.html">Cookie & Policy</a></li>
						<li><a href="<?= BASE_URL ?>prohibited.html">Prohibited item สิ่งของต้องห้าม</a></li> 
					</ul>
				</div><!--col-md-3-->

				<div class="col-md-3">
					<div class="footer-followus">
						<div class="inner">
							<div class="followus">
								<a class="icons icon-facebook" href="#" target="_blank"></a>
								<a class="icons icon-instagram"  href="#" target="_blank"></a>
								<a class="icons icon-twitter"  href="#" target="_blank"></a>
								<a class="icons icon-youtube"  href="#" target="_blank"></a>
							</div>

							<div class="dropdown currency">
								<a data-toggle="dropdown" href="#">Currency Exchange</a>
								<div class="dropdown-menu">
									<p class="text-center mb-0">DATA</p>
								</div><!--dropdown-menu-->
							</div><!--dropdown-->
						</div><!--inner-->
					</div><!--footer-followus-->
				</div><!--col-md-3-->
			</div><!--row-->
		</div><!--container-->
	</div><!--footer-primary-->

	<div class="footer-secondary">
		© 2020 All rights reserved. <span class="nowrap">Powerde by <a href="https://gramickhouse.com/" target="_blank">Gramick House</a></span>
	</div><!--footer-secondary-->
</div><!--footer-->

<div class="modal modal-search fade" id="searchModal">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
    		<button class="btn btn-close" data-dismiss="modal"></button>
    		<form action="<?= BASE_URL ?>search" method="get">
    		<div class="row">
    			<div class="col-md-12">
    				<div class="input-block search">
    					<input type="text" name="text_search" class="form-control" placeholder="ค้นหาโปรเจคที่คุณสนใจ..." value="<?= (isset($_GET) && !empty($_GET['text_search'])) ? $_GET['text_search'] : '' ?>">
    					<span class="icons icon-search"></span>
    				</div>
    			</div><!--col-md-12-->

    			<div class="col-md-6">
    				<div class="input-block">
						<span class="input-text">หมวดหมู่</span>
						
						<?php $menu_category = menu_category(); ?>

    					<select class="form-control" name="category">
						<option value="">กรุณาเลือก</option>
						<?php foreach ($menu_category as $vm) { ?>
							<option value="<?= $vm->project_category_id ?>" <?= ($_GET['category'] == $vm->project_category_id) ? 'selected' : '' ?>><?= $vm->project_category_description_name ?></option>
							<?php } ?>
						</select>
						
    				</div>
    			</div><!--col-md-6-->

    			<div class="col-md-6">
    				<div class="input-block">
						<span class="input-text">พื้นที่</span>
						<?php $province = province(); ?>
    					<select class="form-control" name="location">
							<option value="">กรุณาเลือก</option>
							<?php foreach ($province as $vm) { ?>
							<option value="<?= $vm->province_id ?>" <?= ($_GET['location'] == $vm->province_id) ? 'selected' : '' ?>><?= $vm->province_name_th ?></option>
							<?php } ?>
    					</select>
    				</div>
    			</div><!--col-md-6-->
    			<div class="col-md-12 text-right">
    				<button class="btn btn-light btn-search" type="submit">ค้นหา</button>
    			</div><!--col-md-6-->

    		</div><!--row-->
    		</form>
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->