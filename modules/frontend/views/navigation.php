<header class="header">
	<div class="navbar navbar-topbar header-slideout">
		<div class="container">
			<ul class="nav nav-general">
				<li><button class="btn btn-light btn-search" data-toggle="modal" data-target="#searchModal"></button></li>
				<?php 
				if (!$this->session->userdata("laun_logged")) { ?>
		            <li class="nav-login"><a class="btn btn-light" href="<?= BASE_URL ?>auth"><span class="icons icon-key"></span> LOGIN</a></li>
					<li class="nav-register"><a class="btn btn-light" href="<?= BASE_URL ?>auth"><span class="icons icon-lock"></span> REGISTER</a></li>
		        <?php }else{ ?>
		        	<li><a class="btn" href="<?= BASE_URL ?>project-create">Start a Project</a></li>
		        	<li class="dropdown nav-user">
						<a data-toggle="dropdown" href="#">
							<span class="avatar" style="background-image: url(<?= $this->session->userdata("laun_avatar") ?>);"></span>
							<span class="username"><?= $this->session->userdata("laun_username") ?></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?= BASE_URL ?>account.html"><span class="icons icon-pencil"></span> จัดการบัญชี</a></li>
							<li><a href="<?= BASE_URL ?>notifications.html"><span class="icons icon-notification"></span> จัดการแจ้งเตือน</a></li>
							<li><a href="<?= BASE_URL ?>account/payment-methods.html"><span class="icons icon-card"></span> ช่องทางการชำระเงิน</a></li>
							<li><a href="<?= BASE_URL ?>my-project"><span class="icons icon-award"></span> โครงการของฉัน</a></li>
							<li><a href="<?= BASE_URL ?>favorite"><span class="icons icon-favorite"></span> ที่ชื่นชอบ</a></li>
							<li class="logout"><a href="<?= BASE_URL ?>logout"><span class="icons icon-logout"></span> ออกจากระบบ</a></li>
						</ul>
					</li> 
		        <?php } ?>
				

				

				<li class="nav-lang dropdown">
					<a data-toggle="dropdown" href="#"><span class="icons icon-global"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<? BASE_URL ?>LanguageSwitcher/switchLang/thai">TH</a></li>
						<li><a href="<? BASE_URL ?>LanguageSwitcher/switchLang/english">EN</a></li>
					</ul>
				</li>
			</ul><!--nav-general-->
		</div><!--container-->
	</div><!--navbar-topbar-->

	<div class="navbar navbar-device dark d-block d-lg-none header-slideout"> <!--show on screen < 991px (Tablet & Mobile)-->
		<div class="container">
			<div class="row">
				<div class="col-6">
					<div class="navbar-brand"><a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>asset/launcher/img/logo.png" alt=""></a></div>
				</div>
				<div class="col-6">
					<button class="btn btn-icon navbar-toggle" type="button"> 
			            <span class="group">
					      <span></span>
					      <span></span>
					      <span></span>
					    </span>
			        </button>
				</div>
			</div><!--row-->
		</div><!--container-->
	</div><!--navbar-device-->

	<div class="navbar navbar-main dark">
		<div class="container">
			<div class="navbar-brand d-none d-lg-inline-block"><a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>asset/launcher/img/logo.png" alt=""></a></div>

			<ul class="nav nav-main">
				<li class="nav-search d-block d-lg-none">
					<div class="search-general in-navbar">
						<button class="btn btn-light btn-search"></button>
						<input type="text" class="form-control" name=""> 
					</div>
				</li>
				<?php $menu_category = menu_category(); ?>
	        	<?php foreach ($menu_category as $vm) { 
	        		$menu_subcategory = menu_subcategory($vm->project_category_id);
	        	?>
	        		<?php if (isset($menu_subcategory) && !empty($menu_subcategory)) { ?>
	        			<li class="nav-item dropdown">
							<a data-toggle="dropdown" href="#"><?= $vm->project_category_description_name ?></a>
							<ul class="dropdown-menu">
								<li class="nav-close"><a><span class="arrow"></span> <?= $vm->project_category_description_name ?></a></li>
								<?php foreach ($menu_subcategory as $vs) { ?>
									<li><a href="<?= BASE_URL ?>project-list?category=<?= $vm->project_category_id ?>"><?= $vs->project_subcategory_description_name ?></a></li>
								<?php } ?>
							</ul><!--dropdown-menu-->
						</li>
	        		<?php }else{ ?>
						<li class="nav-item"><a href="<?= BASE_URL ?>project-list?category=<?= $vm->project_category_id ?>"><?= $vm->project_category_description_name ?></a></li>	
	        		<?php } ?>
	        		
	        	<?php } ?>
			</ul> 
		</div><!--container-->
	</div><!--navbar-general-->
</header>