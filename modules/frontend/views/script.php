<script src="<?= base_url('asset/launcher/js/jquery-1.12.4.min.js') ?>"></script>
<script src="<?= base_url('asset/launcher/js/popper.min.js') ?>"></script>
<script src="<?= base_url('asset/launcher/js/bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sharer.js@latest/sharer.min.js"></script>
<script src="<?= base_url('asset/launcher/js/wow.js') ?>"></script>   
<script src="<?= base_url('asset/launcher/js/swiper.js') ?>"></script>     
<script src="<?= base_url('asset/launcher/js/jquery.fancybox.js') ?>"></script>   
<script src="<?= base_url('asset/launcher/js/ie11CustomProperties.js') ?>"></script>
<!-- <script src="<?= base_url('asset/launcher/js/jquery.customSelect.js') ?>"></script>   -->
<script src='<?= base_url('asset/launcher/js/circle-progress.js') ?>'></script> 
<script src='<?= base_url('asset/launcher/js/jquery.appear.js') ?>'></script>   
<script src='<?= base_url('asset/launcher/js/bootstrap-datepicker.js') ?>'></script>  
<script src='<?= base_url('asset/launcher/js/jquery.validate.js') ?>'></script>   
<script src="<?= base_url('asset/launcher/js/custom.js') ?>"></script>  
<div id="var-data-url" style="display:none" site="<?= BASE_URL ?>">
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
<script>
    const BASE_URL = $("#var-data-url").attr("site");
</script>
<script type="text/javascript">
  
	var swiperBanner = new Swiper('.swiper-banner', {
      speed: 1000,
      loop: true,
      observer: true,
      effect: 'slide',
      observeParents: true,
      autoplay: {
        delay: 7000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination.banner',
        clickable: true,
      },
      navigation: {
          nextEl: '.swiper-button-next.banner',
          prevEl: '.swiper-button-prev.banner',
      },
      
    });

    var swiperProject = new Swiper('.swiper-project', {
      speed: 1000,
      loop: false,
      observer: true,
      spaceBetween: 20,
      effect: 'slide',
      observeParents: true,
      pagination: {
        el: '.swiper-pagination.project',
        clickable: true,
      },
      navigation: {
          nextEl: '.swiper-button-next.project',
          prevEl: '.swiper-button-prev.project',
      },
      
    });

    //---

    var swiperNews = new Swiper('.swiper-news', {
    	slidesPerView: 4,
	    spaceBetween:30,
	    slidesPerGroup: 4,
	    speed: 800,
	    loop: false,
	    observer: true,
	    effect: 'slide',
	    observeParents: true,
	    pagination: {
	        el: '.swiper-pagination.news',
	        clickable: true,
	    },
	    navigation: {
	        nextEl: '.swiper-button-next.news',
	        prevEl: '.swiper-button-prev.news',
	    }, 
	    breakpoints: {
		    1440: {
		        slidesPerView: 4,  
		        slidesPerGroup: 4,
		        spaceBetween: 20,
		        speed: 700,
		    },
		    992: { 
	          	slidesPerView: 3,  
		        slidesPerGroup: 3,
		        spaceBetween: 20,
		        speed: 700,
		    },
		    767: {
		        slidesPerView: 1, 
		        slidesPerGroup: 1,
	          	spaceBetween: 10, 
		    }  
	    }
    });

    function _upload(){
      document.getElementById('file_upload_avatar').click();
  }
    function readURLHeader(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imageHeader').css('background-image', 'url('+e.target.result +')');
                    $('#imageHeader').hide();
                    $('#imageHeader').fadeIn(650);
                    $('.file-upload-content').css('display', 'block');
                }

                var formData = new FormData();
                formData.append('profile_image', input.files[0]);

                $.ajax({
                    type: 'post',
                    url: BASE_URL+'account/profile_upload',
                    data: formData,
                    async:false,
                    enctype: 'multipart/form-data',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        let json = JSON.parse(data);
                        if (json.status) {
                            location.reload();
                        }
                    },
                    error: function(data) {
                        console.log("error : " + data);
                    }
                });

                reader.readAsDataURL(input.files[0]);
            }
        }

    $('.progress-bar').each(function(){
      	$(this).appear(function() {
          $(this).css('width',  function(){ return ($(this).attr('data-percentage')+'%')});
    	}); 
  	}); 

    $(window).on('load resize', function(){
    	var window_size = $(window).width();
    	var p_size = 120;
    	var p_thickness = 15;

	  	if (window_size > 1281) {
	     	var p_size = 200;
    		var p_thickness = 30;
  
	  	}else if (window_size > 1025) {
	    	var p_size = 160;
    		var p_thickness = 20;	
	 	} 

	 	$('.progress-circle').each(function(){
	    	$(this).appear(function() {
		      $(this).find(".circle").circleProgress({
			    //value: .84,
			    fill: {color: '#21D695'},
			    emptyFill: 'rgba(244, 244, 244, .4)',
			    startAngle: -Math.PI / 4 * 2,
			    thickness:p_thickness,
			    size:p_size,
			  }).on('circle-animation-progress', function(event, progress, stepValue) {
			    $(this).find('strong').text(stepValue.toFixed(2).substr(2));
			  });
			}); 
		});
	});

    $(document).on('click','.in_favourite', function() {
	 	let project = $(this)
      	let id = project.data('id')

      	$.ajax({
          type: 'get',
          url: BASE_URL + 'project/in_favourite/'+id,
          async:false,
          cache: false,
          contentType: false,
          processData: false,
          success: function(data) {
            let json = JSON.parse(data);
            project.removeClass('in_favourite')
            project.addClass('active un_favourite')
          },
          error: function(data) {
              console.log("error : " + data);
          }
      	});
	});

    $(document).on('click','.un_favourite', function() {
      	let project = $(this)
      	let id = project.data('id')
      	$.ajax({
          type: 'get',
          url: BASE_URL + 'project/un_favourite/'+id,
          async:false,
          cache: false,
          contentType: false,
          processData: false,
          success: function(data) {
            let json = JSON.parse(data);
            project.removeClass('active un_favourite')
            project.addClass('in_favourite')
          },
          error: function(data) {
              console.log("error : " + data);
          }
      	});
	});

	$(document).on('click','.in_like', function() {
	 	let project = $(this)
      	let id = project.data('id')

      	$.ajax({
          type: 'get',
          url: BASE_URL + 'project/in_like/'+id,
          async:false,
          cache: false,
          contentType: false,
          processData: false,
          success: function(data) {
            let json = JSON.parse(data);
            project.removeClass('in_like')
            project.addClass('active un_like')
          },
          error: function(data) {
              console.log("error : " + data);
          }
      	});
	});

    $(document).on('click','.un_like', function() {
      	let project = $(this)
      	let id = project.data('id')
      	$.ajax({
          type: 'get',
          url: BASE_URL + 'project/un_like/'+id,
          async:false,
          cache: false,
          contentType: false,
          processData: false,
          success: function(data) {
            let json = JSON.parse(data);
            project.removeClass('active un_like')
            project.addClass('in_like')
          },
          error: function(data) {
              console.log("error : " + data);
          }
      	});
	});

	$(document).on('click','.goto_login', function() {
      	window.location.href = BASE_URL + 'auth';
	});

   
  
</script>