<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 <style>
	 .upload_image_file{
		/*  opacity:0; */
		display:none;
	 }
</style>
<body>
<div class="preload"></div>
<div class="page "> 
<?= $this->template->build('frontend/navigation'); ?>
<div class="page-slideout"> 
	<div class="section has-space-top"> 
 		<div class="container">
 			<div class="project-donate-total">
 				<h3>
 					ยอดรวมสนุบสนุนโครงการ
 				</h3>

 				<div class="project-donate-box">
 					<table class="table xs-block">
 						<tr>
 							<td>โครงการ</td>
 							<th><?= $project[0]->project_name ?></th>
 						</tr>
 					</table>
 					<table class="table secondary"> 
 						<tr>
 							<td>ยอดสนับสนุน</td>
 							<th>฿<?= number_format($donate[0]->donate_amount) ?></th>
 						</tr>

 						<tr>
 							<td>ค่าธรรมเนียม 3%</td>
 							<th>฿3.00</th>
 						</tr>

 						<tr class="total">
 							<td>รวมทั้งหมด</td>
 							<th class="f20">฿<?= number_format($donate[0]->donate_total) ?></th>
 						</tr>
 					</table>
 				</div><!--project-donate-box-->

 				<form class="form mt-5" action="project-detail.html" method="post">
 					<div class="row">
 						<div class="col-12">
 							<div class="card-logo text-right">
 								<img src="<?= BASE_URL ?>asset/launcher/img/thumb/card-logo.png" alt="">
 							</div>
 						</div>
 						<div class="col-12">
 							<div class="input-block">
								<input type="text" class="form-control" name="card_id" placeholder="หมายเลขบัตร" required="">
							</div>
 						</div><!--col-12-->

 						<div class="col-12">
 							<div class="input-block">
								<input type="text" class="form-control" name="card_name" placeholder="ชื่อผู้ถือบัตร" required="">
							</div>
 						</div><!--col-12-->

 						<div class="col-12">
 							<p>วันหมดอายุบัตร</p>
 						</div><!--col-12-->

 						<div class="col-md-4">
 							<div class="input-block">
								<input type="text" class="form-control" name="card_m" placeholder="MM" required="">
							</div>
 						</div><!--col-md-4-->

 						<div class="col-md-4">
 							<div class="input-block">
								<input type="text" class="form-control" name="card_y" placeholder="YYYY" required="">
							</div>
 						</div><!--col-md-4-->

 						<div class="col-md-4">
 							<div class="input-block">
								<input type="text" class="form-control" name="card_cvc" placeholder="CVC" required="">
							</div>
 						</div><!--col-md-4-->
 					</div><!--row-->

 					<div class="buttons">
	 					<button class="btn f16">สนับสนุน</button>
	 				</div>
 				</form>
 				
 			</div><!--project-donate-total-->
		</div><!--container-->
	</div><!--section-->
 
	<div class="footer">
		<div class="footer-primary">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<h3 class="footer-title d-block d-md-none">หมวดหมู่</h3>
						<ul class="footer-links category">
							<li><a href="project.html">Comics & Illustration</a></li>
							<li><a href="project.html">Design & Tech</a></li>
							<li><a href="project.html">Food & Craft</a></li>
							<li><a href="project.html">Games</a></li>
							<li><a href="project.html">Music</a></li>
							<li><a href="project.html">Mobile App</a></li>
							<li><a href="project.html">Donation Project </a></li>
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3 col-6">
						<h3 class="footer-title">เกี่ยวกับเรา</h3>
						<ul class="footer-links">
							<li><a href="help.html">ศูนย์ความช่วยเหลือ</a></li>
							<li><a href="#">เงื่อนไขและความเป็นส่วนตัว</a></li>
							<li><a href="#">แผนผังเว็บไซต์</a></li> 
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3 col-6">
						<h3 class="footer-title">กฏระเบียบการใช้บริการ</h3>
						<ul class="footer-links">
							<li><a href="#">คู่มือการสร้างแคมเปญ</a></li>
							<li><a href="#">Trust & Safety</a></li>
							<li><a href="#">Cookie & Policy</a></li>
							<li><a href="#">Prohibited item สิ่งของต้องห้าม</a></li> 
						</ul>
					</div><!--col-md-3-->

					<div class="col-md-3">
						<div class="footer-followus">
							<div class="inner">
								<div class="followus">
									<a class="icons icon-facebook" href="#" target="_blank"></a>
									<a class="icons icon-instagram"  href="#" target="_blank"></a>
									<a class="icons icon-twitter"  href="#" target="_blank"></a>
									<a class="icons icon-youtube"  href="#" target="_blank"></a>
								</div>

								<div class="dropdown currency">
									<a data-toggle="dropdown" href="#">Currency Exchange</a>
									<div class="dropdown-menu">
										<p class="text-center mb-0">DATA</p>
									</div><!--dropdown-menu-->
								</div><!--dropdown-->
							</div><!--inner-->
						</div><!--footer-followus-->
					</div><!--col-md-3-->
				</div><!--row-->
			</div><!--container-->
		</div><!--footer-primary-->

		<div class="footer-secondary">
			© 2020 All rights reserved. <span class="nowrap">Powerde by <a href="https://gramickhouse.com/" target="_blank">Gramick House</a></span>
		</div><!--footer-secondary-->
	</div><!--footer-->
</div><!--page-slideout-->

<?= $this->template->build('frontend/project/search_project'); ?>
<?= $this->template->build('frontend/script'); ?>

<script>
    /*------------[Start] bootstrap-datepicker.js ------------*/

	$('input.date').datepicker({
	    format: "dd/mm/yyyy",
	    todayHighlight: true
	});

	/*------------[Start] circle-progress.js ------------*/
	 
    $(window).on('load resize', function(){
    	var window_size = $(window).width();
    	var p_size = 40;
    	var p_thickness = 3;

	  	if (window_size > 1281) {
	     	var p_size = 70;
    		var p_thickness = 5;
  
	  	}else if (window_size > 768) {
	     	var p_size = 50;
    		var p_thickness = 3;
  
	  	} 

	 	$('.nav .progress-circle').each(function(){
	    	$(this).appear(function() {
		      $(this).find(".circle").circleProgress({
			    //value: .84,
			    fill: {color: '#21D695'},
			    emptyFill: '#E0E0E0',
			    startAngle: -Math.PI / 4 * 2,
			    thickness:p_thickness,
			    size:p_size,
			  }).on('circle-animation-progress', function(event, progress, stepValue) {
			   // $(this).find('strong').text(stepValue.toFixed(2).substr(2));
			  });
			}); 
		});
	});
    //$(function() { 
        $(".btn-save").click(function(e){

            //console.log('prj submit',$('input[name="project_date_start"]').val())
            e.preventDefault();

            var formData = new FormData($('#form-payment-project')[0]);
            console.log('formData',formData)

            $.ajax({
                type: 'post',
                url: BASE_URL + 'frontend/project/project/add_payment_project',
                data: formData,
                async:false,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {

                    let json = JSON.parse(data);

                    if(json.status === true){

                        $.confirm({
                                        title: 'บันทึก',
                                        content: 'เชื่อมโยงบัญชีธนาคารสำเร็จ',
                                        autoClose: 'Close|2000',
                                        type: 'green',
                                        buttons: {
                                            Close: {}
                                        }
                                });
                        //window.location.href = BASE_URL + 'story-'+'<?= $project_id; ?>';

                    }
                    console.log(json)
                },
                error: function(data) {
                    console.log("error : " + data);
                }
            });
        });

   // });

</script>
</body>
</html>