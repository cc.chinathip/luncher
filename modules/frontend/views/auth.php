<!DOCTYPE html>
<html lang="th">

<head> 
	<?= $this->template->build('frontend/header'); ?>
</head>
 
<body>
<div class="preload"></div>
 
<div class="page"> 
	<header class="header">
		<div class="navbar navbar-topbar header-slideout">
			<div class="container">
				<ul class="nav nav-general"> 
					<li class="nav-lang dropdown">
						<a data-toggle="dropdown" href="#"><span class="icons icon-global"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">TH</a></li>
							<li><a href="#">EN</a></li>
						</ul>
					</li>
				</ul><!--nav-general-->
			</div><!--container-->
		</div><!--navbar-topbar-->
	</header>

	<div class="page-slideout">
		<div class="section section-login-register full-height">
			<div class="background" style="background-image: url(img/thumb/login-bg.jpg);"></div>
			<div class="container">
				<div class="form form-login-regsiter wow fadeIn">

					<div class="logo">
						<a href="index.html"><img src="img/logo.png" alt=""></a>
					</div>

					<ul class="nav nav-tabs nav-login-tabs">
						<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_login"><span>LOGIN</span></a></li>
						<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_register"><span>REGESTER</span></a></li>
					</ul>

					<div class="tab-content tab-login-content">
						<div class="tab-pane fade show active" id="tab_login">
							 <form id="form-login">
                        		<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
								<div class="input-block">
									<input type="text" class="form-control" id="member_username" name="member_username" placeholder="Username">
								</div>

								<div class="input-block">
									<input type="password" class="form-control" id="member_password" name="member_password" placeholder="Password">
								</div>

								<div class="input-block forgotpass">
									<a href="#">Forgot your password?</a>
								</div>

								<div class="buttons">
									<button type="submit" class="btn btn-full">LOGIN</button>
								</div>
							</form>
						</div><!--tab-pane-->
						<div class="tab-pane fade" id="tab_register">
							<form id="form-register">
								<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
								<div class="input-block">
									<input type="text" class="form-control" id="member_username" name="member_username" placeholder="Username" required>
								</div>

								<div class="input-block">
									<input type="email" class="form-control" id="member_email" name="member_email" placeholder="Email" required>
								</div>

								<div class="input-block">
									<input type="password" class="form-control" minlength="8" id="member_password_register" name="member_password_register" placeholder="Password" required>
								</div>

								<div class="input-block">
									<input type="password" class="form-control" minlength="8" id="member_confirm_password" name="member_confirm_password" placeholder="Re-Password" required>
								</div> 

								<div class="buttons">
									<button type="submit" class="btn btn-full">CREATE ACCOUNT</button>
								</div>
							</form>
						</div><!--tab-pane-->
				    </div><!--tab-login-content-->
				</div><!--form-->
			</div><!--container-->
		</div><!--section-->
		<div class="footer"> 
			<div class="footer-secondary">
				© 2020 All rights reserved. <span class="nowrap">Powerde by <a href="https://gramickhouse.com/" target="_blank">Gramick House</a></span>
			</div><!--footer-secondary-->
		</div><!--footer-->
	</div><!--page-slideout-->

</div><!--page-->

<?= $this->template->build('frontend/script'); ?>
<link rel="stylesheet" href="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.css') ?>">
<script src="<?= base_url('asset/launcher/lib/jquery-confirm/dist/jquery-confirm.min.js') ?>"></script> 
 	<script type="text/javascript">
      	$('#form-login').on('submit', function(e) {
          	e.preventDefault();
          	let fm = this
         
	        var formData = new FormData(fm);
	       
	        $.ajax({
	            type: 'post',
	            url: BASE_URL + 'auth/login_confirm',
	            data: formData,
	            async:false,
	            enctype: 'multipart/form-data',
	            cache: false,
	            contentType: false,
	            processData: false,
	            success: function(data) {
	              let json = JSON.parse(data);
	              if (json.status == 'successful_access') {
	               	window.location.href = BASE_URL;
	              }else{
	                $.alert({
	                    title: '',
	                    content: json.message,
	                    type: 'red',
	                });
	              }
	              // console.log(json)
	            },
	            error: function(data) {
	                console.log("error : " + data);
	            }
	        });
	               

	      });

      	$('#form-register').on('submit', function(e) {
          e.preventDefault();
          let fm = this
          console.log($('#member_password_register').val(),$('#member_confirm_password').val())
          if ($('#member_password_register').val() != $('#member_confirm_password').val()) {
          	$.alert({
                title: '',
                content: 'Passwords do not match. Please password please match.',
                type: 'red',
            });
          }else{

	        $.confirm({
	            title: 'ยืนยัน',
	            content: 'คุณต้องการสมัครสมาชิก ?',
	            buttons: {
	                confirm: function () {
	                    var formData = new FormData(fm);
	      
	                    $.ajax({
	                        type: 'post',
	                        url: BASE_URL + 'auth/register_confirm',
	                        data: formData,
	                        async:false,
	                        enctype: 'multipart/form-data',
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        success: function(data) {
	                          let json = JSON.parse(data);
	                          
	                          if (json.status) {
	                            window.location.href = BASE_URL + 'auth';
	                          }else{
	                            $.alert({
	                                title: '',
	                                content: json.message,
	                                type: 'red',
	                            });
	                          }
	                        },
	                        error: function(data) {
	                            console.log("error : " + data);
	                        }
	                    });
	                },
	                cancel: function () {
	                    // $.alert('Canceled!');
	                }
	            }
	        });
          }
          

      	});
    </script>
</body>
</html>