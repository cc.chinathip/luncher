<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_project_news extends MY_Model {

    private $primary_key    = 'project_news_id';
    private $table_name     = 'project_news';
    private $field_search   = ['project_news_project_id', 'project_news_title', 'project_news_image', 'project_news_detail', 'project_news_created_at', 'project_news_created_by', 'project_news_updated_at', 'project_news_updated_by', 'project_news_cover'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "project_news.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "project_news.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "project_news.".$field . " LIKE '%" . $q . "%' )";
        }

        if($where != NULL){
            $where .= ' AND project_news.project_news_deleted = 0';
        }else{
            $where = ' project_news.project_news_deleted = 0';
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "project_news.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "project_news.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "project_news.".$field . " LIKE '%" . $q . "%' )";
        }

        if($where != NULL){
            $where .= ' AND project_news.project_news_deleted = 0';
        }else{
            $where = ' project_news.project_news_deleted = 0';
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('project_news.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function get_where($where=[])
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->result();
    }
    
    public function logs_insert($data){
        $this->db->insert('logs_system', $data);
        $this->db->insert_id();
    }

    public function db_update($data,$where=[]){
        $this->db->where($where);
        $this->db->update($this->table_name, $data);
    }

    public function join_avaiable() {
        
        $this->db->select('project_news.*');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            $this->db->where($this->table_name.'.project_news_created_by', get_user_data('id'));
        $this->db->where($this->table_name.'.project_news_updated_by', get_user_data('id'));
        }

        return $this;
    }

}

/* End of file Model_project_news.php */
/* Location: ./application/models/Model_project_news.php */