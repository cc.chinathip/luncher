<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['project_news'] = 'Project News';
$lang['project_news_id'] = 'Project News Id';
$lang['project_news_project_id'] = 'Project News Project Id';
$lang['project_news_title'] = 'Project News Title';
$lang['project_news_image'] = 'Project News Image';
$lang['project_news_detail'] = 'Project News Detail';
$lang['project_news_created_at'] = 'Project News Created At';
$lang['project_news_created_by'] = 'Project News Created By';
$lang['project_news_updated_at'] = 'Project News Updated At';
$lang['project_news_updated_by'] = 'Project News Updated By';
$lang['project_news_cover'] = 'Project News Cover';
