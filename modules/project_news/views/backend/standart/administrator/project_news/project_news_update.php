

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Project News        <small>Edit Project News</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/project_news'); ?>">Project News</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Project News</h3>
                            <h5 class="widget-user-desc">Edit Project News</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/project_news/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_project_news', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_project_news', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="project_news_project_id" class="col-sm-2 control-label">Project News Project Id 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="project_news_project_id" id="project_news_project_id" placeholder="Project News Project Id" value="<?= set_value('project_news_project_id', $project_news->project_news_project_id); ?>">
                                <small class="info help-block">
                                <b>Input Project News Project Id</b> Max Length : 11.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="project_news_title" class="col-sm-2 control-label">Project News Title 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="project_news_title" id="project_news_title" placeholder="Project News Title" value="<?= set_value('project_news_title', $project_news->project_news_title); ?>">
                                <small class="info help-block">
                                <b>Input Project News Title</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="project_news_image" class="col-sm-2 control-label">Project News Image 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="project_news_project_news_image_galery"></div>
                                <input class="data_file data_file_uuid" name="project_news_project_news_image_uuid" id="project_news_project_news_image_uuid" type="hidden" value="<?= set_value('project_news_project_news_image_uuid'); ?>">
                                <input class="data_file" name="project_news_project_news_image_name" id="project_news_project_news_image_name" type="hidden" value="<?= set_value('project_news_project_news_image_name', $project_news->project_news_image); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                  
                                                <div class="form-group ">
                            <label for="project_news_detail" class="col-sm-2 control-label">Project News Detail 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <textarea id="project_news_detail" name="project_news_detail" rows="10" cols="80"> <?= set_value('project_news_detail', $project_news->project_news_detail); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                         
                         
                         
                         
                                                <div class="form-group ">
                            <label for="project_news_cover" class="col-sm-2 control-label">Project News Cover 
                            </label>
                            <div class="col-sm-8">
                                <div id="project_news_project_news_cover_galery"></div>
                                <input class="data_file data_file_uuid" name="project_news_project_news_cover_uuid" id="project_news_project_news_cover_uuid" type="hidden" value="<?= set_value('project_news_project_news_cover_uuid'); ?>">
                                <input class="data_file" name="project_news_project_news_cover_name" id="project_news_project_news_cover_name" type="hidden" value="<?= set_value('project_news_project_news_cover_name', $project_news->project_news_cover); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<!-- Page script -->
<script>
    $(document).ready(function(){
       
      
      CKEDITOR.replace('project_news_detail'); 
      var project_news_detail = CKEDITOR.instances.project_news_detail;
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/project_news';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
        $('#project_news_detail').val(project_news_detail.getData());
                    
        var form_project_news = $('#form_project_news');
        var data_post = form_project_news.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_project_news.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#project_news_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
                     var params = {};
       params[csrf] = token;

       $('#project_news_project_news_image_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/project_news/upload_project_news_image_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/project_news/delete_project_news_image_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/project_news/get_project_news_image_file/<?= $project_news->project_news_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#project_news_project_news_image_galery').fineUploader('getUuid', id);
                   $('#project_news_project_news_image_uuid').val(uuid);
                   $('#project_news_project_news_image_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#project_news_project_news_image_uuid').val();
                  $.get(BASE_URL + '/administrator/project_news/delete_project_news_image_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#project_news_project_news_image_uuid').val('');
                  $('#project_news_project_news_image_name').val('');
                }
              }
          }
      }); /*end project_news_image galey*/
                            var params = {};
       params[csrf] = token;

       $('#project_news_project_news_cover_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/project_news/upload_project_news_cover_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/project_news/delete_project_news_cover_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/project_news/get_project_news_cover_file/<?= $project_news->project_news_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#project_news_project_news_cover_galery').fineUploader('getUuid', id);
                   $('#project_news_project_news_cover_uuid').val(uuid);
                   $('#project_news_project_news_cover_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#project_news_project_news_cover_uuid').val();
                  $.get(BASE_URL + '/administrator/project_news/delete_project_news_cover_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#project_news_project_news_cover_uuid').val('');
                  $('#project_news_project_news_cover_name').val('');
                }
              }
          }
      }); /*end project_news_cover galey*/
              
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>