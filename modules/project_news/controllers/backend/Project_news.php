<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Project News Controller
*| --------------------------------------------------------------------------
*| Project News site
*|
*/
class Project_news extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_project_news');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Project Newss
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('project_news_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['project_newss'] = $this->model_project_news->get($filter, $field, $this->limit_page, $offset);
		$this->data['project_news_counts'] = $this->model_project_news->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/project_news/index/',
			'total_rows'   => $this->model_project_news->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Project News List');
		$this->render('backend/standart/administrator/project_news/project_news_list', $this->data);
	}
	
	/**
	* Add new project_newss
	*
	*/
	public function add()
	{
		$this->is_allowed('project_news_add');

		$this->template->title('Project News New');
		$this->render('backend/standart/administrator/project_news/project_news_add', $this->data);
	}

	/**
	* Add New Project Newss
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('project_news_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('project_news_project_id', 'Project News Project Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('project_news_title', 'Project News Title', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('project_news_project_news_image_name', 'Project News Image', 'trim|required');
		$this->form_validation->set_rules('project_news_detail', 'Project News Detail', 'trim|required');
		

		if ($this->form_validation->run()) {
			$project_news_project_news_image_uuid = $this->input->post('project_news_project_news_image_uuid');
			$project_news_project_news_image_name = $this->input->post('project_news_project_news_image_name');
			$project_news_project_news_cover_uuid = $this->input->post('project_news_project_news_cover_uuid');
			$project_news_project_news_cover_name = $this->input->post('project_news_project_news_cover_name');
		
			$save_data = [
				'project_news_project_id' => $this->input->post('project_news_project_id'),
				'project_news_title' => $this->input->post('project_news_title'),
				'project_news_detail' => $this->input->post('project_news_detail'),
				'project_news_created_at' => date('Y-m-d H:i:s'),
				'project_news_created_by' => get_user_data('id'),				'project_news_updated_at' => date('Y-m-d H:i:s'),
				'project_news_updated_by' => get_user_data('id'),						'project_news_created_by' => get_user_data('id'),
			'project_news_created_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/project_news/')) {
				mkdir(FCPATH . '/uploads/project_news/');
			}

			if (!empty($project_news_project_news_image_name)) {
				$project_news_project_news_image_name_copy = date('YmdHis') . '-' . $project_news_project_news_image_name;

				rename(FCPATH . 'uploads/tmp/' . $project_news_project_news_image_uuid . '/' . $project_news_project_news_image_name, 
						FCPATH . 'uploads/project_news/' . $project_news_project_news_image_name_copy);

				if (!is_file(FCPATH . '/uploads/project_news/' . $project_news_project_news_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['project_news_image'] = $project_news_project_news_image_name_copy;
			}
		
			if (!empty($project_news_project_news_cover_name)) {
				$project_news_project_news_cover_name_copy = date('YmdHis') . '-' . $project_news_project_news_cover_name;

				rename(FCPATH . 'uploads/tmp/' . $project_news_project_news_cover_uuid . '/' . $project_news_project_news_cover_name, 
						FCPATH . 'uploads/project_news/' . $project_news_project_news_cover_name_copy);

				if (!is_file(FCPATH . '/uploads/project_news/' . $project_news_project_news_cover_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['project_news_cover'] = $project_news_project_news_cover_name_copy;
			}
		
			
			$save_project_news = $this->model_project_news->store($save_data);
            
            $save_data_logs_system = [
				'logs_system_table_name' => 'project_news',
				'logs_system_data_id' => $save_project_news,
				'logs_system_title' => 'add data',
				'logs_system_desc' => 'user add data to project_news',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_project_news->logs_insert($save_data_logs_system);

			if ($save_project_news) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_project_news;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/project_news/edit/' . $save_project_news, 'Edit Project News'),
						anchor('administrator/project_news', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/project_news/edit/' . $save_project_news, 'Edit Project News')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/project_news');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/project_news');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Project Newss
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('project_news_update');

		$this->data['project_news'] = $this->model_project_news->find($id);

		$this->template->title('Project News Update');
		$this->render('backend/standart/administrator/project_news/project_news_update', $this->data);
	}

	/**
	* Update Project Newss
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('project_news_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('project_news_project_id', 'Project News Project Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('project_news_title', 'Project News Title', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('project_news_project_news_image_name', 'Project News Image', 'trim|required');
		$this->form_validation->set_rules('project_news_detail', 'Project News Detail', 'trim|required');
		
		if ($this->form_validation->run()) {
			$project_news_project_news_image_uuid = $this->input->post('project_news_project_news_image_uuid');
			$project_news_project_news_image_name = $this->input->post('project_news_project_news_image_name');
			$project_news_project_news_cover_uuid = $this->input->post('project_news_project_news_cover_uuid');
			$project_news_project_news_cover_name = $this->input->post('project_news_project_news_cover_name');
		
			$save_data = [
				'project_news_project_id' => $this->input->post('project_news_project_id'),
				'project_news_title' => $this->input->post('project_news_title'),
				'project_news_detail' => $this->input->post('project_news_detail'),
				'project_news_created_at' => date('Y-m-d H:i:s'),
				'project_news_created_by' => get_user_data('id'),				'project_news_updated_at' => date('Y-m-d H:i:s'),
				'project_news_updated_by' => get_user_data('id'),						'project_news_updated_by' => get_user_data('id'),
			'project_news_updated_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/project_news/')) {
				mkdir(FCPATH . '/uploads/project_news/');
			}

			if (!empty($project_news_project_news_image_uuid)) {
				$project_news_project_news_image_name_copy = date('YmdHis') . '-' . $project_news_project_news_image_name;

				rename(FCPATH . 'uploads/tmp/' . $project_news_project_news_image_uuid . '/' . $project_news_project_news_image_name, 
						FCPATH . 'uploads/project_news/' . $project_news_project_news_image_name_copy);

				if (!is_file(FCPATH . '/uploads/project_news/' . $project_news_project_news_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['project_news_image'] = $project_news_project_news_image_name_copy;
			}
		
			if (!empty($project_news_project_news_cover_uuid)) {
				$project_news_project_news_cover_name_copy = date('YmdHis') . '-' . $project_news_project_news_cover_name;

				rename(FCPATH . 'uploads/tmp/' . $project_news_project_news_cover_uuid . '/' . $project_news_project_news_cover_name, 
						FCPATH . 'uploads/project_news/' . $project_news_project_news_cover_name_copy);

				if (!is_file(FCPATH . '/uploads/project_news/' . $project_news_project_news_cover_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['project_news_cover'] = $project_news_project_news_cover_name_copy;
			}
		
			
			$save_project_news = $this->model_project_news->change($id, $save_data);

			$save_data_logs_system = [
				'logs_system_table_name' => 'project_news',
				'logs_system_data_id' => $id,
				'logs_system_title' => 'edit data',
				'logs_system_desc' => 'user edit data to project_news',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_project_news->logs_insert($save_data_logs_system);

			if ($save_project_news) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/project_news', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/project_news');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/project_news');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Project Newss
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('project_news_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_deleted($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_deleted($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'project_news'), 'success');
        } else {
            set_message(cclang('error_delete', 'project_news'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Project Newss
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('project_news_view');

		$this->data['project_news'] = $this->model_project_news->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Project News Detail');
		$this->render('backend/standart/administrator/project_news/project_news_view', $this->data);
	}
	
	/**
	* delete Project Newss
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$project_news = $this->model_project_news->find($id);

		if (!empty($project_news->project_news_image)) {
			$path = FCPATH . '/uploads/project_news/' . $project_news->project_news_image;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($project_news->project_news_cover)) {
			$path = FCPATH . '/uploads/project_news/' . $project_news->project_news_cover;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_project_news->remove($id);
	}

	private function _deleted($id)
	{
		$project_news = $this->model_project_news->find($id);

		$save_data = [
			'project_news_deleted' => 1,
			'project_news_updated_by' => get_user_data('id'),
			'project_news_updated_at' => date('Y-m-d H:i:s')
		];
		
		$save_data_delete = $this->model_project_news->change($id, $save_data);

		$save_data_logs_system = [
			'logs_system_table_name' => 'project_news',
			'logs_system_data_id' => $id,
			'logs_system_title' => 'delete data',
			'logs_system_desc' => 'user delete data',
			'logs_system_created_by' => get_user_data('id'),
		];

		$save_logs_system = $this->model_project_news->logs_insert($save_data_logs_system);

		return $save_data_delete;
	}

	
	/**
	* Upload Image Project News	* 
	* @return JSON
	*/
	public function upload_project_news_image_file()
	{
		if (!$this->is_allowed('project_news_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'project_news',
		]);
	}

	/**
	* Delete Image Project News	* 
	* @return JSON
	*/
	public function delete_project_news_image_file($uuid)
	{
		if (!$this->is_allowed('project_news_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'project_news_image', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'project_news',
            'primary_key'       => 'project_news_id',
            'upload_path'       => 'uploads/project_news/'
        ]);
	}

	/**
	* Get Image Project News	* 
	* @return JSON
	*/
	public function get_project_news_image_file($id)
	{
		if (!$this->is_allowed('project_news_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$project_news = $this->model_project_news->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'project_news_image', 
            'table_name'        => 'project_news',
            'primary_key'       => 'project_news_id',
            'upload_path'       => 'uploads/project_news/',
            'delete_endpoint'   => 'administrator/project_news/delete_project_news_image_file'
        ]);
	}
	
	/**
	* Upload Image Project News	* 
	* @return JSON
	*/
	public function upload_project_news_cover_file()
	{
		if (!$this->is_allowed('project_news_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'project_news',
		]);
	}

	/**
	* Delete Image Project News	* 
	* @return JSON
	*/
	public function delete_project_news_cover_file($uuid)
	{
		if (!$this->is_allowed('project_news_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'project_news_cover', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'project_news',
            'primary_key'       => 'project_news_id',
            'upload_path'       => 'uploads/project_news/'
        ]);
	}

	/**
	* Get Image Project News	* 
	* @return JSON
	*/
	public function get_project_news_cover_file($id)
	{
		if (!$this->is_allowed('project_news_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$project_news = $this->model_project_news->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'project_news_cover', 
            'table_name'        => 'project_news',
            'primary_key'       => 'project_news_id',
            'upload_path'       => 'uploads/project_news/',
            'delete_endpoint'   => 'administrator/project_news/delete_project_news_cover_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('project_news_export');

		$this->model_project_news->export('project_news', 'project_news');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('project_news_export');

		$this->model_project_news->pdf('project_news', 'project_news');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('project_news_export');

		$table = $title = 'project_news';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_project_news->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file project_news.php */
/* Location: ./application/controllers/administrator/Project News.php */