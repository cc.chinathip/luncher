<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Member Controller
*| --------------------------------------------------------------------------
*| Member site
*|
*/
class Member extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_member');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Members
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('member_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['members'] = $this->model_member->get($filter, $field, $this->limit_page, $offset);
		$this->data['member_counts'] = $this->model_member->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/member/index/',
			'total_rows'   => $this->model_member->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Member List');
		$this->render('backend/standart/administrator/member/member_list', $this->data);
	}
	
	/**
	* Add new members
	*
	*/
	public function add()
	{
		$this->is_allowed('member_add');

		$this->template->title('Member New');
		$this->render('backend/standart/administrator/member/member_add', $this->data);
	}

	/**
	* Add New Members
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('member_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		

		if ($this->form_validation->run()) {
		
			$save_data = [
						'member_created_by' => get_user_data('id'),
			'member_created_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/member/')) {
				mkdir(FCPATH . '/uploads/member/');
			}

			
			$save_member = $this->model_member->store($save_data);
            
            $save_data_logs_system = [
				'logs_system_table_name' => 'member',
				'logs_system_data_id' => $save_member,
				'logs_system_title' => 'add data',
				'logs_system_desc' => 'user add data to member',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_member->logs_insert($save_data_logs_system);

			if ($save_member) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_member;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/member/edit/' . $save_member, 'Edit Member'),
						anchor('administrator/member', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/member/edit/' . $save_member, 'Edit Member')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/member');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/member');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Members
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('member_update');

		$this->data['member'] = $this->model_member->find($id);

		$this->template->title('Member Update');
		$this->render('backend/standart/administrator/member/member_update', $this->data);
	}

	/**
	* Update Members
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('member_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		
		if ($this->form_validation->run()) {
		
			$save_data = [
						'member_updated_by' => get_user_data('id'),
			'member_updated_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/member/')) {
				mkdir(FCPATH . '/uploads/member/');
			}

			
			$save_member = $this->model_member->change($id, $save_data);

			$save_data_logs_system = [
				'logs_system_table_name' => 'member',
				'logs_system_data_id' => $id,
				'logs_system_title' => 'edit data',
				'logs_system_desc' => 'user edit data to member',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_member->logs_insert($save_data_logs_system);

			if ($save_member) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/member', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/member');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/member');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Members
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('member_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_deleted($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_deleted($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'member'), 'success');
        } else {
            set_message(cclang('error_delete', 'member'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Members
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('member_view');

		$this->data['member'] = $this->model_member->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Member Detail');
		$this->render('backend/standart/administrator/member/member_view', $this->data);
	}
	
	/**
	* delete Members
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$member = $this->model_member->find($id);

		if (!empty($member->member_avatar)) {
			$path = FCPATH . '/uploads/member/' . $member->member_avatar;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_member->remove($id);
	}

	private function _deleted($id)
	{
		$member = $this->model_member->find($id);

		$save_data = [
			'member_deleted' => 1,
			'member_updated_by' => get_user_data('id'),
			'member_updated_at' => date('Y-m-d H:i:s')
		];
		
		$save_data_delete = $this->model_member->change($id, $save_data);

		$save_data_logs_system = [
			'logs_system_table_name' => 'member',
			'logs_system_data_id' => $id,
			'logs_system_title' => 'delete data',
			'logs_system_desc' => 'user delete data',
			'logs_system_created_by' => get_user_data('id'),
		];

		$save_logs_system = $this->model_member->logs_insert($save_data_logs_system);

		return $save_data_delete;
	}

	
	/**
	* Upload Image Member	* 
	* @return JSON
	*/
	public function upload_member_avatar_file()
	{
		if (!$this->is_allowed('member_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'member',
		]);
	}

	/**
	* Delete Image Member	* 
	* @return JSON
	*/
	public function delete_member_avatar_file($uuid)
	{
		if (!$this->is_allowed('member_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'member_avatar', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'member',
            'primary_key'       => 'member_id',
            'upload_path'       => 'uploads/member/'
        ]);
	}

	/**
	* Get Image Member	* 
	* @return JSON
	*/
	public function get_member_avatar_file($id)
	{
		if (!$this->is_allowed('member_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$member = $this->model_member->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'member_avatar', 
            'table_name'        => 'member',
            'primary_key'       => 'member_id',
            'upload_path'       => 'uploads/member/',
            'delete_endpoint'   => 'administrator/member/delete_member_avatar_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('member_export');

		$this->model_member->export('member', 'member');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('member_export');

		$this->model_member->pdf('member', 'member');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('member_export');

		$table = $title = 'member';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_member->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file member.php */
/* Location: ./application/controllers/administrator/Member.php */