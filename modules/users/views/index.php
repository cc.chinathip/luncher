<?php $this->load->view('../../section/header.php'); ?>
<div class="page"> 


<div class="page-slideout">
	<div class="section section-banner">
		<div class="swiper-container swiper-banner">
			<div class="swiper-wrapper">
	        	<div class="swiper-slide"> 
	        		<div class="background" style="background-image: url(img/thumb/photo-1920x725--1.jpg);"></div>
				</div>
				<div class="swiper-slide"> 
	        		<div class="background" style="background-image: url(img/thumb/photo-1920x725--1.jpg);"></div>
	            </div>
				<div class="swiper-slide"> 
	        		<div class="background" style="background-image: url(img/thumb/photo-1920x725--1.jpg);"></div>
				</div>
			</div>
		</div><!--swiper-banner-->

		<div class="swiper-pagination banner"></div> 

		<div class="search-general-container align-items-center">
			<div class="inner">
				<h2 class="title-xl wow fadeInUp">Lorem ipsum dolor sit amet</h2>
				<p class="wow fadeInUp" data-wow-delay="0.2s">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eros, nibh nibh nisl ridiculus non praesent cursus viverra. Molestie eros, id nam in in eros, a. Urna, cras urna, sit cras tempor. Faucibus egestas pulvinar senectus ut arcu.</p>

				<div class="search-general wow fadeInUp" data-wow-delay="0.4s">
					<button class="btn btn-light btn-search"></button>
					<input type="text" class="form-control" name="" placeholder="ค้นหาโปรเจคที่คุณสนใจ..."> 
				</div>
			</div><!--inner-->
		</div><!--search-general-container-->
	</div><!--section-banner-->

	<!--============[End] section 'Banner' ==============-->

	<div class="section section-pj-recommend pt-0 gray-bg">
		<div class="container">
			<div class="section-title">
				<h2 class="title-lg wow fadeIn">โครงการแนะนำ</h2>
			</div>
			<div class="row space-10">
				<div class="col-lg-6">
					<div class="highlight-box">
						<div class="card card-project lg no-bg wow fadeIn">
							<div class="card-photo">
								<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-1280x960--1.jpg);">
									<img src="img/thumb/photo-100x75--blank.png" alt="">
								</a>

								<div class="card-share">
									<div class="item favorite active" data-toggle="tooltip" title="Favorite"></div>
									<div class="item like" data-toggle="tooltip" title="Like"></div>
									<div class="item share" data-toggle="tooltip" title="Share"></div> 
								</div><!--card-share-->
							</div><!--card-photo-->

							<div class="card-body d-flex">
								<div class="col-left">
									<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
								</div>
								<div class="col-right">
									<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
		 							<div class="post-info">
										<div class="author">by <a href="project-homepage.html">Radkey</a></div>
										<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
									</div><!--post-info-->
								</div><!--col-right-->
							</div><!--card-body-->

							<div class="card-footer">
								<div class="progress-box dark">
									<div class="progress">
			                        	<div class="progress-bar" data-percentage="80.30"></div>  
									</div>

									<div class="progress-label">
										<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
										<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
									</div>
								</div><!--progress-box-->
							</div><!--card-footer-->
						</div><!--card-project-->
					</div><!--highlight-box-->
				</div><!--col-lg-6-->
				<div class="col-lg-6">
					<div class="swiper-container swiper-project wow fadeIn">
						<div class="swiper-wrapper">
				        	<div class="swiper-slide"> 

				        		<div class="card-group">
					        		<div class="card card-project lg no-bg list">
										<div class="card-photo">
											<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-1280x960--1.jpg);">
												<img src="img/thumb/photo-100x75--blank.png" alt="">
											</a>

											<div class="card-share">
												<div class="item favorite active" data-toggle="tooltip" title="Favorite"></div>
												<div class="item like" data-toggle="tooltip" title="Like"></div>
												<div class="item share" data-toggle="tooltip" title="Share"></div> 
											</div><!--card-share-->
										</div><!--card-photo-->

										<div class="card-body d-flex">
											<div class="col-left">
												<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
											</div>
											<div class="col-right">
												<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
					 							<div class="post-info">
													<div class="author">by <a href="project-homepage.html">Radkey</a></div>
													<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
												</div><!--post-info-->
											</div><!--col-right-->
										</div><!--card-body-->

										<div class="card-footer">
											<div class="progress-box dark">
												<div class="progress">
						                        	<div class="progress-bar" data-percentage="100"></div>  
												</div>

												<div class="progress-label">
													<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
													<div class="percentage wow fadeIn" data-wow-delay="0.3s">100.00%</div>
												</div>
											</div><!--progress-box-->
										</div><!--card-footer-->
									</div><!--card-project-->

									<div class="card card-project lg no-bg list">
										<div class="card-photo">
											<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-1280x960--1.jpg);">
												<img src="img/thumb/photo-100x75--blank.png" alt="">
											</a>

											<div class="card-share">
												<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
												<div class="item like" data-toggle="tooltip" title="Like"></div>
												<div class="item share" data-toggle="tooltip" title="Share"></div> 
											</div><!--card-share-->
										</div><!--card-photo-->

										<div class="card-body d-flex">
											<div class="col-left">
												<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
											</div>
											<div class="col-right">
												<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
					 							<div class="post-info">
													<div class="author">by <a href="project-homepage.html">Radkey</a></div>
													<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
												</div><!--post-info-->
											</div><!--col-right-->
										</div><!--card-body-->

										<div class="card-footer">
											<div class="progress-box dark">
												<div class="progress">
						                        	<div class="progress-bar" data-percentage="45"></div>  
												</div>

												<div class="progress-label">
													<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
													<div class="percentage wow fadeIn" data-wow-delay="0.3s">45.00%</div>
												</div>
											</div><!--progress-box-->
										</div><!--card-footer-->
									</div><!--card-project-->

									<div class="card card-project lg no-bg list">
										<div class="card-photo">
											<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-1280x960--1.jpg);">
												<img src="img/thumb/photo-100x75--blank.png" alt="">
											</a>

											<div class="card-share">
												<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
												<div class="item like" data-toggle="tooltip" title="Like"></div>
												<div class="item share" data-toggle="tooltip" title="Share"></div> 
											</div><!--card-share-->
										</div><!--card-photo-->

										<div class="card-body d-flex">
											<div class="col-left">
												<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
											</div>
											<div class="col-right">
												<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
					 							<div class="post-info">
													<div class="author">by <a href="project-homepage.html">Radkey</a></div>
													<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
												</div><!--post-info-->
											</div><!--col-right-->
										</div><!--card-body-->

										<div class="card-footer">
											<div class="progress-box dark">
												<div class="progress">
						                        	<div class="progress-bar" data-percentage="45"></div>  
												</div>

												<div class="progress-label">
													<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
													<div class="percentage wow fadeIn" data-wow-delay="0.3s">45.00%</div>
												</div>
											</div><!--progress-box-->
										</div><!--card-footer-->
									</div><!--card-project-->
								</div><!--card-group-->
							</div><!--swiper-slide-->

							<div class="swiper-slide"> 
								<div class="card-group">
					        		<div class="card card-project lg no-bg list">
										<div class="card-photo">
											<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-1280x960--1.jpg);">
												<img src="img/thumb/photo-100x75--blank.png" alt="">
											</a>

											<div class="card-share">
												<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
												<div class="item like" data-toggle="tooltip" title="Like"></div>
												<div class="item share" data-toggle="tooltip" title="Share"></div> 
											</div><!--card-share-->
										</div><!--card-photo-->

										<div class="card-body d-flex">
											<div class="col-left">
												<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
											</div>
											<div class="col-right">
												<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
					 							<div class="post-info">
													<div class="author">by <a href="project-homepage.html">Radkey</a></div>
													<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
												</div><!--post-info-->
											</div><!--col-right-->
										</div><!--card-body-->

										<div class="card-footer">
											<div class="progress-box dark">
												<div class="progress">
						                        	<div class="progress-bar" data-percentage="30.30"></div>  
												</div>

												<div class="progress-label">
													<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
													<div class="percentage wow fadeIn" data-wow-delay="0.3s">30.30%</div>
												</div>
											</div><!--progress-box-->
										</div><!--card-footer-->
									</div><!--card-project-->

									<div class="card card-project lg no-bg list">
										<div class="card-photo">
											<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-1280x960--1.jpg);">
												<img src="img/thumb/photo-100x75--blank.png" alt="">
											</a>

											<div class="card-share">
												<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
												<div class="item like" data-toggle="tooltip" title="Like"></div>
												<div class="item share" data-toggle="tooltip" title="Share"></div> 
											</div><!--card-share-->
										</div><!--card-photo-->

										<div class="card-body d-flex">
											<div class="col-left">
												<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
											</div>
											<div class="col-right">
												<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
					 							<div class="post-info">
													<div class="author">by <a href="project-homepage.html">Radkey</a></div>
													<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
												</div><!--post-info-->
											</div><!--col-right-->
										</div><!--card-body-->

										<div class="card-footer">
											<div class="progress-box dark">
												<div class="progress">
						                        	<div class="progress-bar" data-percentage="12.13"></div>  
												</div>

												<div class="progress-label">
													<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
													<div class="percentage wow fadeIn" data-wow-delay="0.3s">12.13%</div>
												</div>
											</div><!--progress-box-->
										</div><!--card-footer-->
									</div><!--card-project-->

									<div class="card card-project lg no-bg list">
										<div class="card-photo">
											<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-1280x960--1.jpg);">
												<img src="img/thumb/photo-100x75--blank.png" alt="">
											</a>

											<div class="card-share">
												<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
												<div class="item like" data-toggle="tooltip" title="Like"></div>
												<div class="item share" data-toggle="tooltip" title="Share"></div> 
											</div><!--card-share-->
										</div><!--card-photo-->

										<div class="card-body d-flex">
											<div class="col-left">
												<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
											</div>
											<div class="col-right">
												<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
					 							<div class="post-info">
													<div class="author">by <a href="project-homepage.html">Radkey</a></div>
													<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
												</div><!--post-info-->
											</div><!--col-right-->
										</div><!--card-body-->

										<div class="card-footer">
											<div class="progress-box dark">
												<div class="progress">
						                        	<div class="progress-bar" data-percentage="40.30"></div>  
												</div>

												<div class="progress-label">
													<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
													<div class="percentage wow fadeIn" data-wow-delay="0.3s">40.30%</div>
												</div>
											</div><!--progress-box-->
										</div><!--card-footer-->
									</div><!--card-project-->
								</div><!--card-group-->
							</div><!--swiper-slide-->
						</div><!--swiper-wrapper-->
					</div><!--swiper-project-->

					<div class="swiper-pagination project static"></div> 
				</div><!--col-lg-6-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-pj-recommend-->

	<!--============[End] section 'โครงการแนะนำ' ==============-->
	 
	<div class="navbar-category">
		<div class="container">
			<div class="section-title wow fadeIn"><h2 class="title-lg">หมวดหมู่ยอดนิยม</h2></div>
			<div class="row space-10">
				<div class="col-6 col-md-3 wow fadeIn">
					<a class="card card-category" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-commics.svg" alt=""></div>
							<h4>Comics & <br>Illustration</h4>
						</div>
					</a><!--card-category-->
				</div><!--col-md-3-->

				<div class="col-6 col-md-3 wow fadeIn">
					<a class="card card-category" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js"  src="img/icons/icon-design.svg" alt=""></div>
							<h4>Design & <br>Tech </h4>
						</div>
					</a><!--card-category-->
				</div><!--col-md-3-->

				<div class="col-6 col-md-3 wow fadeIn">
					<a class="card card-category" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js"  src="img/icons/icon-game.svg" alt=""></div>
							<h4>Games</h4>
						</div>
					</a><!--card-category-->
				</div><!--col-md-3-->

				<div class="col-6 col-md-3 wow fadeIn">
					<a class="card card-category" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js"  src="img/icons/icon-apps.svg" alt=""></div>
							<h4>Mobile <br>App</h4>
						</div>
					</a><!--card-category-->
				</div><!--col-md-3-->
			</div><!--row-->
		</div><!--container-->
	</div><!--navbar-category-->

	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-commics.svg" alt=""></div>
							<h4><span>Comics & <br>Illustration</span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-4 wow fadeInUp">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--1.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--1.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--1.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-4--> 

						<div class="col-md-12 wow fadeIn">
							<div class="viewall">
								<a href="#">ดูทั้งหมด <span></span></a>
							</div>
						</div>
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 

	<!--================[End] Section 'Comics & Illustration' ====================-->

	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-design.svg" alt=""></div>
							<h4><span>Design & <br>Tech</span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-4 wow fadeInUp">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--2.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite active" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--2.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--2.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-9--> 

						<div class="col-md-12">
							<div class="viewall">
								<a href="#">ดูทั้งหมด <span></span></a>
							</div>
						</div>
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 

	<!--================[End] Section 'Design & Tech' ====================-->

	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-chef.svg" alt=""></div>
							<h4><span>Food & <br>Craft</span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-4 wow fadeInUp">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--7.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite active" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--7.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--7.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-9--> 

						<div class="col-md-12">
							<div class="viewall">
								<a href="#">ดูทั้งหมด <span></span></a>
							</div>
						</div>
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 

	<!--================[End] Section 'Food & Craft' ====================-->

	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-game.svg" alt=""></div>
							<h4><span>Games</span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-4 wow fadeInUp">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--8.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--8.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--8.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-9--> 

						<div class="col-md-12">
							<div class="viewall">
								<a href="#">ดูทั้งหมด <span></span></a>
							</div>
						</div>
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 

	<!--================[End] Section 'Games' ====================-->

	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-piano.svg" alt=""></div>
							<h4><span>Music</span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-4 wow fadeInUp">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--4.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--4.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--4.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-9--> 

						<div class="col-md-12">
							<div class="viewall">
								<a href="#">ดูทั้งหมด <span></span></a>
							</div>
						</div>
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 

	<!--================[End] Section 'Music' ====================-->

	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-apps.svg" alt=""></div>
							<h4><span>Mobile <br>App</span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-4 wow fadeInUp">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--9.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--9.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--9.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-9--> 

						<div class="col-md-12">
							<div class="viewall">
								<a href="#">ดูทั้งหมด <span></span></a>
							</div>
						</div>
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 

	<!--================[End] Section 'Mobile App' ====================-->

	<div class="section-category">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 wow fadeIn">
					<a class="card card-category lg" href="project.html">
						<div class="inner">
							<div class="card-icon"><img class="svg-js" src="img/icons/icon-donation.svg" alt=""></div>
							<h4><span>Donation <br>Project</span></h4>
						</div>
					</a><!--card-category-->
				</div><!--col-lg-3--> 

				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-4 wow fadeInUp">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--10.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="80.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">80.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--10.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-md-4--> 

						<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
							<div class="card card-project normal wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--10.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body d-flex">
									<div class="col-left">
										<a class="avatar" href="project-homepage.html" style="background-image: url(img/thumb/avatar--1.png);"></a>
									</div>
									<div class="col-right">
										<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
											<div class="post-info">
											<div class="author">by <a href="project-homepage.html">Radkey</a></div>
											<div class="date"><span class="icons icon-clock"></span> 29 days left</div>
										</div><!--post-info-->
									</div><!--col-right-->
								</div><!--card-body-->

								<div class="card-footer">
									<div class="progress-box">
										<div class="progress">
				                        	<div class="progress-bar" data-percentage="60.30"></div>  
										</div>

										<div class="progress-label">
											<div class="price wow fadeIn" data-wow-delay="0.1s">$186,576</div>
											<div class="percentage wow fadeIn" data-wow-delay="0.3s">60.30%</div>
										</div>
									</div><!--progress-box-->
								</div><!--card-footer-->
							</div><!--card-project-->
						</div><!--col-lg-9--> 

						<div class="col-md-12">
							<div class="viewall">
								<a href="#">ดูทั้งหมด <span></span></a>
							</div>
						</div>
					</div><!--row-->

				</div><!--col-md-9-->
			</div><!--row-->
		</div><!--container-->
	</div><!--section-category--> 

	<!--================[End] Section 'Donation Project' ====================-->

	<div class="section section-counter">
		<div class="background" style="background-image: url(img/thumb/photo-1920x1000--1.jpg);"></div>
		<div class="container"> 
			<div class="row">
				<div class="col-md-3 col-6">
					<div class="progress-circle">
						<div class="circle" data-value="0.84">
				      		<div class="number"><strong></strong></div>
				      	</div>
				        <h3>Projects are <span>Completed</span></h3>
				    </div>
				</div><!--col-md-3-->

				<div class="col-md-3 col-6">
					<div class="progress-circle">
						<div class="circle" data-value="0.22">
				      		<div class="number"><strong></strong></div>
				      	</div>
				        <h3>Ideas <span>Raised Funds</span></h3>
				    </div>
				</div><!--col-md-3-->

				<div class="col-md-3 col-6">
					<div class="progress-circle">
						<div class="circle" data-value="0.17">
				      		<div class="number"><strong></strong></div>
				      	</div>
				        <h3>Categories <span>Served</span></h3>
				    </div>
				</div><!--col-md-3-->

				<div class="col-md-3 col-6">
					<div class="progress-circle">
						<div class="circle" data-value="0.88">
				      		<div class="number"><strong></strong></div>
				      	</div>
				        <h3>Happy <span>Crstomers</span></h3>
				    </div>
				</div><!--col-md-3-->
			</div><!--row-->
			
		</div><!--container-->
	</div><!--section-counter-->
	<div class="footer-space b-bottom"></div>

	<!--================[End] Section 'Latest News' ====================-->

	<div class="section section-news pt-0">
		<div class="container">
			<div class="section-title"><h2 class="title-lg">Latest News</h2></div>

			<div class="swiper-overflow">
				<div class="swiper-container swiper-news">
					<div class="swiper-wrapper">
			        	<div class="swiper-slide">
			        		<div class="card card-news wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--3.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient convallis tortor, scelerisque nunc augue est sed. Ac volutpat viverra</p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ 20.02.2563</div>
									<div class="readmore-link"><a href="#">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
			        	</div><!--swiper-slide-->

			        	<div class="swiper-slide">
			        		<div class="card card-news wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--4.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient convallis tortor, scelerisque nunc augue est sed. Ac volutpat viverra</p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ 20.02.2563</div>
									<div class="readmore-link"><a href="#">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
			        	</div><!--swiper-slide-->

			        	<div class="swiper-slide">
			        		<div class="card card-news wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--5.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient convallis tortor, scelerisque nunc augue est sed. Ac volutpat viverra</p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ 20.02.2563</div>
									<div class="readmore-link"><a href="#">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
			        	</div><!--swiper-slide-->

			        	<div class="swiper-slide">
			        		<div class="card card-news wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--6.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient convallis tortor, scelerisque nunc augue est sed. Ac volutpat viverra</p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ 20.02.2563</div>
									<div class="readmore-link"><a href="#">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
			        	</div><!--swiper-slide-->

			        	<div class="swiper-slide">
			        		<div class="card card-news wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--3.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient convallis tortor, scelerisque nunc augue est sed. Ac volutpat viverra</p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ 20.02.2563</div>
									<div class="readmore-link"><a href="#">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
			        	</div><!--swiper-slide-->

			        	<div class="swiper-slide">
			        		<div class="card card-news wow fadeIn">
								<div class="card-photo">
									<a class="photo" href="project-detail.html" style="background-image: url(img/thumb/photo-900x675--4.jpg);">
										<img src="img/thumb/photo-100x75--blank.png" alt="">
									</a>

									<div class="card-share">
										<div class="item favorite" data-toggle="tooltip" title="Favorite"></div>
										<div class="item like" data-toggle="tooltip" title="Like"></div>
										<div class="item share" data-toggle="tooltip" title="Share"></div> 
									</div><!--card-share-->
								</div><!--card-photo-->

								<div class="card-body">
									<h3><a href="project-detail.html">Lorem ipsum dolor sit amet</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parturient convallis tortor, scelerisque nunc augue est sed. Ac volutpat viverra</p>
								</div><!--card-body-->

								<div class="card-footer">
									<div class="date">โพสต์เมื่อ 20.02.2563</div>
									<div class="readmore-link"><a href="#">อ่านต่อ</a></div>
								</div><!--card-footer-->
							</div><!--card-project-->
			        	</div><!--swiper-slide--> 
					</div><!--swiper-wrapper-->
				</div><!--swiper-news-->

				<div class="swiper-pagination static news"></div> 
			</div><!--swiper-overflow-->
		</div><!--container-->
	</div><!--section-news-->

	<!--================[End] Section 'Latest News' ====================-->

	<div class="footer-space"></div>
	
</div><!--page-slideout-->

</div><!--page-->
<?php $this->load->view('../../section/footer'); ?>
	