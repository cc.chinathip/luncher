<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['project_category'] = 'Project Category';
$lang['project_category_id'] = 'Id';
$lang['project_category_name'] = 'Name';
$lang['project_category_image'] = 'Image';
$lang['project_category_status'] = 'Status';
$lang['project_category_created_at'] = 'Created At';
$lang['project_category_created_by'] = 'Created By';
