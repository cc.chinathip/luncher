<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['project_category'] = 'Project Category';
$lang['project_category_id'] = 'Project Category Id';
$lang['project_category_name'] = 'Project Category Name';
$lang['project_category_image'] = 'Project Category Image';
$lang['project_category_status'] = 'Project Category Status';
$lang['project_category_created_at'] = 'Project Category Created At';
$lang['project_category_created_by'] = 'Project Category Created By';
$lang['project_category_updated_at'] = 'Project Category Updated At';
$lang['project_category_updated_by'] = 'Project Category Updated By';
