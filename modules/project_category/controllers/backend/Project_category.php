<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Project Category Controller
*| --------------------------------------------------------------------------
*| Project Category site
*|
*/
class Project_category extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_project_category');
		$this->load->model('project_category_description/model_project_category_description');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Project Categorys
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('project_category_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['project_categorys'] = $this->model_project_category->get($filter, $field, $this->limit_page, $offset);
		$this->data['project_category_counts'] = $this->model_project_category->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/project_category/index/',
			'total_rows'   => $this->model_project_category->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Project Category List');
		$this->render('backend/standart/administrator/project_category/project_category_list', $this->data);
	}
	
	/**
	* Add new project_categorys
	*
	*/
	public function add()
	{
		$this->is_allowed('project_category_add');

		$this->template->title('Project Category New');
		$this->render('backend/standart/administrator/project_category/project_category_add', $this->data);
	}

	/**
	* Add New Project Categorys
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('project_category_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		// $this->form_validation->set_rules('project_category_name', 'Name', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('project_category_status', 'Status', 'trim|required');
		

		if ($this->form_validation->run()) {
			$project_category_project_category_image_uuid = $this->input->post('project_category_project_category_image_uuid');
			$project_category_project_category_image_name = $this->input->post('project_category_project_category_image_name');
			
			$language = db_get_all_data('language');

			$save_data = [
				'project_category_name' => $this->input->post('project_category_description_name')[$language[0]->language_id],
				'project_category_status' => $this->input->post('project_category_status'),
				'project_category_created_by' => get_user_data('id'),
				'project_category_created_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/project_category/')) {
				mkdir(FCPATH . '/uploads/project_category/');
			}

			if (!empty($project_category_project_category_image_name)) {
				$project_category_project_category_image_name_copy = date('YmdHis') . '-' . $project_category_project_category_image_name;

				rename(FCPATH . 'uploads/tmp/' . $project_category_project_category_image_uuid . '/' . $project_category_project_category_image_name, 
						FCPATH . 'uploads/project_category/' . $project_category_project_category_image_name_copy);

				if (!is_file(FCPATH . '/uploads/project_category/' . $project_category_project_category_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['project_category_image'] = $project_category_project_category_image_name_copy;
			}
		
			
			$save_project_category = $this->model_project_category->store($save_data);

			foreach ($language as $row){
				$save_data = [
					'project_category_description_project_category_id' => $save_project_category,
					'project_category_description_language_id' => $row->language_id,
					'project_category_description_name' => $this->input->post('project_category_description_name')[$row->language_id],
					'project_category_description_name_nl' => $this->input->post('project_category_description_name_nl')[$row->language_id],
					'project_category_description_status' => $this->input->post('project_category_status'),
					'project_category_description_created_by' => get_user_data('id'),
					'project_category_description_created_at' => date('Y-m-d H:i:s'),
				];

				
				$save_project_category_description = $this->model_project_category_description->store($save_data);
			}
			
            
            $save_data_logs_system = [
				'logs_system_table_name' => 'project_category',
				'logs_system_data_id' => $save_project_category,
				'logs_system_title' => 'add data',
				'logs_system_desc' => 'user add data to project_category',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_project_category->logs_insert($save_data_logs_system);

			if ($save_project_category) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_project_category;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/project_category/edit/' . $save_project_category, 'Edit Project Category'),
						anchor('administrator/project_category', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/project_category/edit/' . $save_project_category, 'Edit Project Category')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/project_category');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/project_category');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Project Categorys
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('project_category_update');

		$this->data['project_category'] = $this->model_project_category->find($id);
		$this->data['project_category_description'] = $this->model_project_category_description->get_where(['project_category_description_project_category_id'=>$id]);

		$this->template->title('Project Category Update');
		$this->render('backend/standart/administrator/project_category/project_category_update', $this->data);
	}

	/**
	* Update Project Categorys
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('project_category_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		// $this->form_validation->set_rules('project_category_name', 'Name', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('project_category_status', 'Status', 'trim|required');
		
		if ($this->form_validation->run()) {
			$project_category_project_category_image_uuid = $this->input->post('project_category_project_category_image_uuid');
			$project_category_project_category_image_name = $this->input->post('project_category_project_category_image_name');
			
			$language = db_get_all_data('language');

			$save_data = [
				'project_category_name' => $this->input->post('project_category_description_name')[$language[0]->language_id],
				'project_category_status' => $this->input->post('project_category_status'),
				'project_category_updated_by' => get_user_data('id'),
				'project_category_updated_at' => date('Y-m-d H:i:s'),
			];

			if (!is_dir(FCPATH . '/uploads/project_category/')) {
				mkdir(FCPATH . '/uploads/project_category/');
			}

			if (!empty($project_category_project_category_image_uuid)) {
				$project_category_project_category_image_name_copy = date('YmdHis') . '-' . $project_category_project_category_image_name;

				rename(FCPATH . 'uploads/tmp/' . $project_category_project_category_image_uuid . '/' . $project_category_project_category_image_name, 
						FCPATH . 'uploads/project_category/' . $project_category_project_category_image_name_copy);

				if (!is_file(FCPATH . '/uploads/project_category/' . $project_category_project_category_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['project_category_image'] = $project_category_project_category_image_name_copy;
			}
		
			
			$save_project_category = $this->model_project_category->change($id, $save_data);

			foreach ($language as $row){
				$save_data = [
					'project_category_description_name' => $this->input->post('project_category_description_name')[$row->language_id],
					'project_category_description_name_nl' => $this->input->post('project_category_description_name_nl')[$row->language_id],
					'project_category_description_status' => $this->input->post('project_category_status'),
					'project_category_description_updated_by' => get_user_data('id'),
					'project_category_description_updated_at' => date('Y-m-d H:i:s')
				];

				$save_project_category_description = $this->model_project_category_description->db_update(
					$save_data,
					['project_category_description_project_category_id'=>$id,'project_category_description_language_id'=>$row->language_id]
				);
			}

			$save_data_logs_system = [
				'logs_system_table_name' => 'project_category',
				'logs_system_data_id' => $id,
				'logs_system_title' => 'edit data',
				'logs_system_desc' => 'user edit data to project_category',
				'logs_system_created_by' => get_user_data('id'),
			];

			$save_logs_system = $this->model_project_category->logs_insert($save_data_logs_system);

			if ($save_project_category) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/project_category', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/project_category');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/project_category');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Project Categorys
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('project_category_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_deleted($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_deleted($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'project_category'), 'success');
        } else {
            set_message(cclang('error_delete', 'project_category'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Project Categorys
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('project_category_view');

		$this->data['project_category'] = $this->model_project_category->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Project Category Detail');
		$this->render('backend/standart/administrator/project_category/project_category_view', $this->data);
	}
	
	/**
	* delete Project Categorys
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$project_category = $this->model_project_category->find($id);

		if (!empty($project_category->project_category_image)) {
			$path = FCPATH . '/uploads/project_category/' . $project_category->project_category_image;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_project_category->remove($id);
	}

	private function _deleted($id)
	{
		$project_category = $this->model_project_category->find($id);

		$save_data = [
			'project_category_deleted' => 1,
			'project_category_updated_by' => get_user_data('id'),
			'project_category_updated_at' => date('Y-m-d H:i:s')
		];
		
		$save_data_delete = $this->model_project_category->change($id, $save_data);

		$save_data_logs_system = [
			'logs_system_table_name' => 'project_category',
			'logs_system_data_id' => $id,
			'logs_system_title' => 'delete data',
			'logs_system_desc' => 'user delete data',
			'logs_system_created_by' => get_user_data('id'),
		];

		$save_logs_system = $this->model_project_category->logs_insert($save_data_logs_system);

		return $save_data_delete;
	}

	
	/**
	* Upload Image Project Category	* 
	* @return JSON
	*/
	public function upload_project_category_image_file()
	{
		if (!$this->is_allowed('project_category_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'project_category',
		]);
	}

	/**
	* Delete Image Project Category	* 
	* @return JSON
	*/
	public function delete_project_category_image_file($uuid)
	{
		if (!$this->is_allowed('project_category_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'project_category_image', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'project_category',
            'primary_key'       => 'project_category_id',
            'upload_path'       => 'uploads/project_category/'
        ]);
	}

	/**
	* Get Image Project Category	* 
	* @return JSON
	*/
	public function get_project_category_image_file($id)
	{
		if (!$this->is_allowed('project_category_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$project_category = $this->model_project_category->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'project_category_image', 
            'table_name'        => 'project_category',
            'primary_key'       => 'project_category_id',
            'upload_path'       => 'uploads/project_category/',
            'delete_endpoint'   => 'administrator/project_category/delete_project_category_image_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('project_category_export');

		$this->model_project_category->export('project_category', 'project_category');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('project_category_export');

		$this->model_project_category->pdf('project_category', 'project_category');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('project_category_export');

		$table = $title = 'project_category';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_project_category->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file project_category.php */
/* Location: ./application/controllers/administrator/Project Category.php */